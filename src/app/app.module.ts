import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DashboardModule } from './dashboard/dashboard.module';
import { SharedModule } from './shared/shared.module';
import { AppComponent } from './app.component';
import { appRoutes } from './routes';
import { HttpClientModule } from '@angular/common/http';

import { CookieService } from 'ngx-cookie-service';
import { OAuthService } from './shared/services/oauth.service';
import { NotificationService } from './shared/services/notification.service';
import { CatalogueService } from './shared/services/catalogue.service';
import { ExcelService } from './shared/services/excel.service';
import {NgSelectizeModule} from 'ng-selectize';
import { Angular2CsvModule } from 'angular2-csv';
import { NgDatepickerModule } from 'ng2-datepicker';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    DashboardModule,
    SharedModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    NgSelectizeModule,
    NgDatepickerModule,
    Angular2CsvModule

  ],

  exports: [ AppComponent, Angular2CsvModule ],

  providers: [
    CookieService,
    OAuthService,
    NotificationService,
    CatalogueService,
    ExcelService,
    {provide: LOCALE_ID, useValue: "es-AR"} ],
  bootstrap: [AppComponent]
})
export class AppModule { }
