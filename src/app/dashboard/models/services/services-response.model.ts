import { Client } from '../sales/sales.model';
import { Producto } from '../products/producto.model';
import {SubLineByLine, Brand, Province, Catalog, Gender, CivilStatus, Line, ProductBySubLine, SubLinea} from './services.model';

class Status {
  code: number;
  description: string;
}

export class CatalogResponse {
  code: Status;
  body: Catalog[];
}

export class ProvinceCatalogResponse extends CatalogResponse {
  body: Province[];
}

export class SubLineCatalogResponse {
  code: Status;
  body: SubLinea[];
}

export class BrandCatalogResponse {
  code: Status;
  body: {
    marcas: Brand[]
  };
}

export class LineCatalogResponse {
  code: Status;
  body: {
    lineas: Line[]
  };
}

export class ProductCatalogResponse {
  code: Status;
  body: ProductBySubLine[];
}

export class GetClientResponse {
  status: Status;
  // id: multi-cliente
  cliente?: Client;
  clientes?: Client[];
}

export class GetProductsResponse {
  code: Status;
  body: {
       productos: Array<Producto>
   };
}
