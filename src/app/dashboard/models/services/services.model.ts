import {Sucursal} from "../sales/sales.model";

export class Catalog {
  id: string;
  descripcion: string;
}

export class Province {
  id: string;
  code: string;
  descripcion: string;
}

export class SubLineByLine {
  idLinea: string;
  sublineas: SubLine[];
}
export class SubLinea {
  id: string;
  descripcion: string;
}

export class ProductBySubLine {
  idLinea: string;
  idSubLinea: string;
  productos: Product[];
}

export class SubLine {
  id: string;
  descripcion: string;
}

export class Brand {
  id: string;
  descripcion: string;
}

export class Line {
  id: string;
  descripcion: string;
}

export class Gender {
  id: string;
  descripcion: string;
}

export class CivilStatus {
  id: string;
  descripcion: string;
}

export class Product {
  marca: string;
  hasImei: boolean;
  idProducto: string;
  descripcion: string;
  hasNroSerie: boolean;
}

export class Range {
  id: number;
  precioDesde: number;
  precioHasta: number;
}

export class Rango {
  codigoProducto: string;
  descripcion: string;
  constructor(codigo: string, descripcion: string ){
    this.descripcion = descripcion;
    this.codigoProducto = codigo;

  }
}

export class Code {
  code: number;
  descripcion: string;
  constructor(code: number, descripcion: string){
    this.code = code;
    this.descripcion = descripcion;
  }
}

export class Rangos {
  rangos: Array<Rango>;
  constructor(rangos: Array<Rango> ){
    this.rangos = rangos;

  }
}

export class RespuestaRango {
  code: Code;
  body: Rangos;
}

export class WarrantyProductPrice {
  Plazo: string;
  Importe: number;
}

export class WarrantyProduct {
  Id: number;
  Descripcion: string;
  Sublinea: string;
  PrecioDesde: string;
  PrecioHasta: string;
  TieneRobo: boolean;
  tieneDano?: boolean;
  precio?: string;
  colPrecios: WarrantyProductPrice[];
}

export class WarrantyResponse {
  code: Code;
  body: WarrantyProduct[];
}

export class JsonParaFiltrar {
  dni?: number;
  fechaDesde?: string;
  fechaHasta?: string;
  productos?: string[];
  estados?: string[];
  sucursales?: string[];
  nombre: string;
  apellido: string;
  vendedores: string[];
  supervisores: string[];
}

/*export const SINGLE_SELECT_PRESET_VALUE_CONFIG = Object.assign({}, DEFAULT_DROPDOWN_CONFIG, {
export const valueConfig = Object.assign() {
  labelField: 'label',
  valueField: 'value',
  searchField: ['label']
});*/



export const Example_Placeholder = 'Placeholder...';
export const Example_Placeholder_HasOptions = 'Click to select options';
export const Example_Placeholder_NoOptions = 'No options available...';

export const DEFAULT_DROPDOWN_CONFIG: any = {
  highlight: false,
  create: false,
  persist: true,
  plugins: ['dropdown_direction', 'remove_button'],
  dropdownDirection: 'down'
};


export const SingleSelectConfig: any = Object.assign({}, DEFAULT_DROPDOWN_CONFIG, {
  labelField: 'label',
  valueField: 'value',
  plugins: ['remove_button'],
  maxItems: 1
});


export interface InterfazFiltro {
  label: string;
  value: string;
  code: number;
}

export class ObjetoFiltro {
  label: string;
  value: string;
  code: number;

  constructor(label?: string, value?: string, code?: number){
    this.label = label;
    this.value = value;
    this.code = code;
  }
}


export const CURRENT_OPTIONS_CONFIG = Object.assign({}, DEFAULT_DROPDOWN_CONFIG, {
  labelField: 'label',
  valueField: 'value',
  searchField: ['label', 'value'],
  maxItems: 10
});

export const CURRENT_OPTIONS_CONFIG_SUCURSALES = Object.assign({}, DEFAULT_DROPDOWN_CONFIG, {
  labelField: 'label',
  valueField: 'value',
  searchField: ['label', 'value'],
  maxItems: 1
});

export const REMOVE_OPTIONS_CONFIG = Object.assign({}, DEFAULT_DROPDOWN_CONFIG, {
  labelField: 'label',
  valueField: 'value',
  searchField: ['label', 'value'],
  maxItems: 10
});

export const ADD_OPTIONS_CONFIG = Object.assign({}, DEFAULT_DROPDOWN_CONFIG, {
  labelField: 'label',
  valueField: 'value',
  searchField: ['label', 'value'],
  maxItems: 10
});

export class RespuestaSucursales{
  status: string;
  sucursales: Sucursal[];
}


