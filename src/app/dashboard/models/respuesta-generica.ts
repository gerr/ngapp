export class RespuestaGenerica {
  code: Code = new Code();
  body: any;
  respuesta: string;
  mensaje: string;
}

class Code {
  code: number;
  description: string;
}
