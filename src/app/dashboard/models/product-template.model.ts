import { Type } from '@angular/core';

export class ProductTemplate {
  constructor(public component: Type<any>, public name: string) {}
}
