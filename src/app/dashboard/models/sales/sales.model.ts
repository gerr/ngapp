import {InterfazFiltro, ObjetoFiltro} from '../services/services.model';

export class Client {
  id?: number;
  apellido?: string;
  calle?: string;
  celular1?: string;
  celular2?: string;
  celular3?: string;
  codAreaCelular1: string;
  codAreaCelular2?: string;
  codAreaCelular3?: string;
  codAreaFijo1: string;
  codAreaFijo2?: string;
  codAreaFijo3?: string;
  codigoPostal?: string;
  departamento?: string;
  digitoVerificador?: number; // last digit of cuil value
  email1?: string;
  email2?: string;
  fechaNacimiento?: string;  // dd-mm-yyyy
  idActividad?: string;
  idEstadoCivil?: string;
  idGenero?: string;
  idLocalidad?: string;
  idNacionalidad?: string;
  idRelacionLaboral?: string;
  idProvincia?: string;
  inicioCuil?: number;
  integradoEnCRM?: string;
  nombre?: string;
  nroDocumento?: string;
  numero?: string;
  piso?: string;
  telefonoFijo1: string;
  telefonoFijo2?: string;
  telefonoFijo3?: string;
  tipoDocumento?: string;
  torre?: string;
  domicilio?: {
    domicilio_codigo_postal?: string,
    id_localidad?: string,
    numero?: string,
    calle?: string,
    piso?: string,
    departamento?: string,
    id_provincia?: number,
    torre?: 7
  };
  email?: Array<string>;
  localidad?: any;
  provincia?: any;
}

export class Adicional {
  persona: Client;
  relacionConTitular: string;
}

export class Beneficiario {
  persona: Client;
  porcentaje: string;
  relacionConTitular: string;
}

export class EncuestaSale {
  idArquetipo: number;
  idContacto: number;
}

export class BaseSale {
  idNegocio: number;
  idVendedor: string;
  numeroTarjeta: string;
  medioDePago: string;
  nombreCompletoTitular: string;
  fechaVencimientoTarjeta: string;
  idSucursal: string;
  idEstado: number;
  motivoCancelacion: string;
  aceptaTerminosYCondiciones: boolean;
  aceptaRecibirNovedades: boolean;
  cliente: Client;
  clientePagador: Client;
  adicionales: Array<Adicional>;
  beneficiarios: Array<any>;
  detalleSolicitudVenta: DetalleSolicitudVenta;
  cuit: string;
  dni: string;
  isWarranty?: boolean;
  respuestaApi?: RespuestaTokenDecidir;
  encuesta: EncuestaSale = new EncuestaSale();
}

export interface DetalleSolicitudVenta {
  idProductoElegido?: any;
  descripcionProductoElegido?: string;
  destino_alta?: string;
  idPlanElegido?: string;
  plan?: string;
  precioPrdElegido?: string;
  codigo_plan_vt?: number;
  beneficiarioOAdicionales?: Array<any>;
  esTitular?: boolean;

}

export class SaleRequest {
  venta: BaseSale;
  image: number[];
}

export class SaleReport {
  idVenta?: string;
  dni?: string;
  nombreCliente?: string;
  descripcionProducto?: string;
  idPlanElegido?: string;
  fechaActualizacion?: string;
  estado?: string;
  numeroCertificado?: string;
  nombreVendedor?: string;
  dniVendedor?: string;
  nombreSupervisor?: string;
  dniSupervisor?: string;
  nombreSucursal?: string;

  /*  constructor(){
      this.idVenta = "-";
      this.dni = "-";
      this.nombreCliente = "-";
      this.descripcionProducto = "-";
      this.fechaActualizacion = "-";
      this.estado = "-";
      this.numeroCertificado = "-";
      this.nombreVendedor = "-";
      this.dniVendedor = "-";
      this.nombreSupervisor = "-";
      this.nombreSucursal = "-";
    }*/

}


export class DatosTarjeta {
  card_number: string;
  card_expiration_month: string;
  card_expiration_year: string;
  security_code: string;
  card_holder_name: string;
  card_holder_identification: CardHolderIdentification = new CardHolderIdentification();
}

class CardHolderIdentification {
  type: string;
  number: string;
}

/*export class RespuestaTokenDecidir {
  token: string;
}*/

export class RespuestaTokenDecidir {
  id?: string;
  status?: string;
  card_number_length?: number;
  date_created?: string;
  bin?: string;
  last_four_digits?: string;
  security_code_length?: number;
  expiration_month?: number;
  expiration_year?: number;
  date_due?: string;
  cardholder?: Cardholder;
  type?: string;
  number?: string;
  name?: string;
}

class Cardholder {
  identification: Identification;
  name: string;
}

class Identification {
  type: string;
  number: string;
}


export const listaDeEstados: ObjetoFiltro[] = [
  {
    code: 1,
    label: 'Pendientes',
    value: '1',
  }, {
    code: 2,
    label: 'Exitosas',
    value: '2'
  }, {
    code: 3,
    label: 'Canceladas',
    value: '3'
  }, {
    code: 4,
    label: 'Rechazadas',
    value: '4'
  }, {
    code: 5,
    label: 'Excluidas',
    value: '5'
  }, {
    code: 6,
    label: 'Anuladas',
    value: '6'
  }
];

export class MotivoCancelacion {
  id: string;
  descripcion: string;
}

export class Cancelacion {
  idSolicitud: number;
  idMotivoCancelacion: number;
  dni: string;
}

export class RespuestaCancelacion {
  idContrato: string;
  idVenta: number;
  estado: number;
  detalle: string;
}

export class Sucursal {
  id: number;
  cuitSocio: string;
  numeroPuntoVenta: number;
  descripcion: string;
  idUsuario: number;
  numeroDocumento: number;
  nombreApellido: string;
  estado: boolean;
  activo: boolean;
  codRespuesta: string;
  mensaje: string;
  idSupervisor: number;
  cuit: string;

  constructor(id?: number, cuitSocio?: string, numeroPuntoVenta?: number, descripcion?: string, idUsuario?: number, numeroDocumento?: number, nombreApellido?: string, estado?: boolean, activo?: boolean, codRespuesta?: string, mensaje?: string, idSupervisor?: number, cuit?: string) {
    this.id = id;
    this.cuitSocio = cuitSocio;
    this.numeroPuntoVenta = numeroPuntoVenta;
    this.descripcion = descripcion;
    this.idUsuario = idUsuario;
    this.numeroDocumento = numeroDocumento;
    this.nombreApellido = nombreApellido;
    this.estado = estado;
    this.activo = activo;
    this.codRespuesta = codRespuesta;
    this.mensaje = mensaje;
    this.idSupervisor = idSupervisor;
    this.cuit = cuit;
  }
}

export class SucursalGuardar {
  id: number;
  descripcion: string;
  idSupervisor: number;
  cuit: string;
  activo: boolean;
  codRespuesta: string;
  mensaje: string;

  constructor(id?: number, descripcion?: string, idSupervisor?: number, cuit?: string, activo?: boolean, codRespuesta?: string, mensaje?: string) {
    this.id = id;
    this.descripcion = descripcion;
    this.idSupervisor = idSupervisor;
    this.cuit = cuit;
    this.activo = activo;
    this.codRespuesta = codRespuesta;
    this.mensaje = mensaje;
  }
}

export class ExportCsv {
  id: string;
  descripcion: string;
  dniSupervisor: string;
  estado: string;
  detalle: string;

  constructor(id?: string, descripcion?: string, dniSupervisor?: string, estado?: string, detalle?: string) {
    this.id = id;
    this.descripcion = descripcion;
    this.dniSupervisor = dniSupervisor;
    this.estado = estado;
    this.detalle = detalle;
  }
}

export class ErrorSPS {
  isValid: boolean;
  error: Error;
  param: string;
}

export class Error {
  type: string;
  message: string;
  param: string;
}

/*
{
  "error": [{
  "isValid": false,
  "error": {
    "type": "invalid_expiry_date",
    "message": "Expiry date is invalid",
    "param": "expiry_date"
  },
  "param": "expiry_date"
},
  {
    "isValid": false,
    "error": {
      "type": "empty_card_holder_name",
      "message": "Card Holder Name is empty",
      "param": "card_holder_name"
    },
    "param": "card_holder_name"
  }]
}*/
