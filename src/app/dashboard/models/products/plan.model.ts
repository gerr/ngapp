//todo id: integracion vt
export class Cobertura {
  id_cobertura: number;
  descripcion_cobertura: string;
}

export class Riesgo {
  codigo_riesgo: string;
  desc_riesgo: string;
  suma_riesgo: string;
}

export class Rango {
  codigo_etario: string;
  desc_etario: string;
  lstRiesgos: Array<Riesgo>;
  precio: string;
}

export class Plan {
  codigo_plan: string;
  codigo_plan_vt: number;

  lstRangos: Array<Rango>;
}

//product.planes.plan is undefined
export class Planes {
  plan: Array<Plan>;
}
