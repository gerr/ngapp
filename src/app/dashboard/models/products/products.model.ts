export class WarrantyForm {
  marca: string;
  linea: string;
  subLinea: string;
  producto: string;
  imei?: string;
  nroFactura: string;
  nroCaja: string;
}

export class TechnologyForm {
  marca: string;
  linea: string;
  subLinea: string;
  producto: string;
  imei?: string;
  nroSerie?: string;
  nroFactura: string;
}

export class  PurchaseForm {
  montoCompra: number;
  tiempoAsegurar: string;
  descripcionBienAsegurado: string;
}

export class HomeForm {
  piso: string;
  calle: string;
  numero: number;
  torre: string;
  idLocalidad: string;
  idProvincia: string;
  codigoPostal: string;
  departamento: string;
}
