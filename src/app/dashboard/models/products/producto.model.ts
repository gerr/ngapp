//todo id: integracion vt
import {Planes, Plan} from './plan.model';

export class Producto {
  codigo_producto: string;
  conjunto_producto: ConjuntoProducto = new ConjuntoProducto();
  descripcion_mkt: string;
  descripcion_producto: string;
  destino_alta: string;
  id_negocio: number;
  id_producto: number;
  id_producto_mkt: number;
  //id_conjunto_producto: number; todo: borrar si funca todo bien.
  medios_pago: Array<MedioPago>;
  metapoliza: number;
  planes: Array<Plan>;
  ramo: number;
  subpoliza: number;
}

class ConjuntoProducto {
  id_conjunto_producto: number;
  titular: number;
  beneficiario: number;
  adicional: number;
  complementario: string;
}

export class MedioPago {
  id: number;
  descripcion: string;
  numero_comercio?: string;
}

export const MEDIOS_PAGO: MedioPago[] = [
  {
    id: 1,
    descripcion: 'Visa',
    numero_comercio: '14411102'
  },
  {
    id: 2,
    descripcion: 'American',
    numero_comercio: '9900864422'
  },
  {
    id: 4,
    descripcion: 'Dinner',
    numero_comercio: '18089330'
  }
];

export class Vendedor {
  id: number;
  nombreApellido: string;
  nroDocumento: number;
}

export class Supervisor {
  id: number;
  nombreApellido?: string;
  nroDocumento?: number;
  cuit?: string;
  email?: string;
  idSucursales?: string[];
}
