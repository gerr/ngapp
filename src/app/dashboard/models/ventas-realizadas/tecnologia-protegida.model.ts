export class TecnologiaProtegida {
  imei: string;
  nroFactura: string;
  nroSerie: string;
  linea: string;
  subLinea: string;
  producto: string;
  marca: string;
}
