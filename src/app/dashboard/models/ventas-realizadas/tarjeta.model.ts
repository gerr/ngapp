export class CreditCard {
  numeroTarjeta: string;
  nombreTitular: string;
  fechaVencimiento: string;
  codigoSeguridad: string;
}
