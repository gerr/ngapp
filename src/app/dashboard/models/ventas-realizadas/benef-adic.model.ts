export class BenefAdic {
  tipoDocumento: string;
  nroDocumentoBeneficiario: string;
  tipo: string;
  apellido: string;
  nombre: string;
  fechaNacimiento: Date;
  cuil: string;
  idGenero: string;
  descripcionGenero: string;
  idNacionalidad: string;
  descripcionNacionalidad: string;
  idEstadoCivil: string;
  descripcionEstadoCivil: string;
  idActividad: number;
  descripcionActividad: string;
  idRelacionLaboral: string;
  descripcionRelacionLaboral: string;
  calle: string;
  numero: string;
  piso: string;
  departamento: string;
  torre: string;
  codigoProvincia: string;
  descripcionProvincia: string;
  localidad: string;
  codigoPostal: string;
  email: string;
  telefonos: Array<any>;
  relacionConElTitular: string;
  porcentajeAsignado: number;
}
