
import { BenefAdic } from './benef-adic.model';

export class SaludOAccidentes {
  nroDocumentoTitular: string;
  beneficiariosOAdicionales: Array<BenefAdic>;
}
