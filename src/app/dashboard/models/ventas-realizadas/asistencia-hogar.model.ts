export class AsistenciaHogar {
    tipoVivienda: string;
    calle: string;
    numero: string;
    piso: string;
    departamento: string;
    torre: string;
    codigoProvincia: string;
    descripcionProvincia: string;
    localidad: string;
    codigoPostal: string;
}
