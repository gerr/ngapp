import { CreditCard } from './tarjeta.model';

export class VentaBase {
  idCliente: string;
  nombreCliente: string;
  idVendedor: string;
  sucursal: string;
  terminosYCondiciones: boolean;
  novedades: boolean;
  estado: string;
  motivoCancelacion: string;
  codigoConfirmacion: string;
  imagenConfirmarVenta: string;
  lineaNegocio: string;
  codigoProducto: string;
  descripcionProducto: string;
  codigoPlan: string;
  idRiesgo: string;
  tarjetaCredito: CreditCard;
}
