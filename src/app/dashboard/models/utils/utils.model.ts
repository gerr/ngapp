import { Client} from '../sales/sales.model';

export class Step {
  name: string;
  state: string;
}

export class Risk {
  monto: string;
  detalle: string;
  idRiesgo: string;
  descripcion: string;
}

export class Reason {
  id: number;
  description: string;
}

export class HeaderOption {
  key: string;
  value: string;
}

export class Additional {
  id?: number;
  type?: string;
  name?: string;

  id_relacion_titular?: string;
  persona?: Client;

  constructor(id: number, type: string, name: string, relacion?: string, persona?: Client) {
    this.id = id;
    this.type = type;
    this.name = name;

    this.id_relacion_titular = relacion;
    this.persona = persona;
  }


}

export class Beneficiary {
  id?: number;
  name?: string;
  percentage?: number;

  orden?: number;
  porcentaje_participacion?: number;
  persona?: Client;

  constructor(id: number, name: string, percentage: number, orden: number, persona: Client) {
    this.orden = orden;
    this.porcentaje_participacion = percentage;
    this.persona = persona;
  }
}

/* export class Additional {
  id?: number;
  type: string;
  name: string;

  constructor(id: number, type: string, name: string) {
    this.id = id;
    this.type = type;
    this.name = name;
  }
}

export class Beneficiary {
  id?: number;
  name: string;
  percentage: number;
} */

export class Encuesta {
  id: number;
  nombre: string;
  descripcion: string;
  descripcion_fem?: string;
  estado: boolean;
}

export const ARQUETIPOS: Encuesta[] = [
  {
    id: 1,
    nombre: 'novato',
    descripcion: 'Novato',
    descripcion_fem: 'Novata',
    estado: false,
  },
  {
    id: 2,
    nombre: 'profesional',
    descripcion: 'Profesional',
    descripcion_fem: 'Profesional',
    estado: false,
  },
  {
    id: 3,
    nombre: 'hobbista',
    descripcion: 'Hobbista',
    descripcion_fem: 'Hobbista',
    estado: false,
  },
  {
    id: 4,
    nombre: 'descreido',
    descripcion: 'Descreído',
    descripcion_fem: 'Descreída',
    estado: false,
  },
  {
    id: 5,
    nombre: 'jubilado',
    descripcion: 'Jubilado',
    descripcion_fem: 'Jubilada',
    estado: false,
  },
  {
    id: 6,
    nombre: 'antojadizo',
    descripcion: 'Antojadizo',
    descripcion_fem: 'Antojadiza',
    estado: false,
  },
  {
    id: 7,
    nombre: 'oficinista',
    descripcion: 'Oficinista',
    descripcion_fem: 'Oficinista',
    estado: false,
  },
  {
    id: 8,
    nombre: 'impulsivo',
    descripcion: 'Impulsivo',
    descripcion_fem: 'Impulsiva',
    estado: false,
  },
  {
    id: 9,
    nombre: 'emergente',
    descripcion: 'Emergente',
    descripcion_fem: 'Emergente',
    estado: false,
  }
];

export const COMUNICACION = [
  {
    id: 1,
    nombre: 'correo',
    descripcion: 'Correo',
    estado: false,
  },
  {
    id: 2,
    nombre: 'telefonico',
    descripcion: 'Telefonico',
    estado: false,
  },
  {
    id: 3,
    nombre: 'chat',
    descripcion: 'Chat',
    estado: false,
  },
  {
    id: 4,
    nombre: 'social',
    descripcion: 'Redes',
    estado: false,
  },
  {
    id: 5,
    nombre: 'presencial',
    descripcion: 'Presencial',
    estado: false,
  },
  {
    id: 6,
    nombre: 'apps',
    descripcion: 'Apps',
    estado: false,
  }
];
