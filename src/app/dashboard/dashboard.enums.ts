export enum TemplateKeys {
  empty       = '',
  health      = 'HealthTemplate',
  home        = 'HomeTemplate',
  technology  = 'TechnologyTemplate',
  purchase    = 'PurchaseTemplate',
  warranty    = 'WarrantyTemplate'
}

export enum State {
  empty       = '',
  pending     = 'PENDING',
  active      = 'ACTIVE',
  completed   = 'COMPLETED',
  valid       = 'VALID',
  invalid     = 'INVALID'
}

export enum StateSale {
  ACCEPTED  = 1,
  PENDING,
  CANCELED,
  REJECTED
}

export enum cardIssuer {
  visa    = 'VISA',
  master  = 'MASTERCARD',
  amex    = 'AMERICAN EXPRESS'
}

export enum Role {
  ADDITIONAL = 1,
  BENEFICIARY,
  TITULAR
}
