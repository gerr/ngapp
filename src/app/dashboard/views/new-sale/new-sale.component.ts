import {Component, OnInit, ViewChild, ElementRef, AfterViewInit, Input} from '@angular/core';
import { Subscription } from 'rxjs';

import { State } from '../../dashboard.enums';
import { Step } from '../../models/utils/utils.model';
import { UserModel } from '../../../shared/models/user.model';
import { NewSaleService } from '../../services/new-sale.service';
import { DashboardService } from '../../services/dashboard.service';
import { OAuthService } from '../../../shared/services/oauth.service';
import { LatestSalesComponent } from '../../components/latest-sales/latest-sales.component';

@Component({
  selector: 'app-new-sale',
  templateUrl: './new-sale.component.html',
  styleUrls: ['./new-sale.component.scss']
})
export class NewSaleComponent implements OnInit,AfterViewInit  {
  readonly SEARCH: string = 'search';

  state: any;
  step: Step;
  steps: Step[];
  user: UserModel;
  stepNumber: number;
  isProcessing: boolean;
  isModalActive: boolean;
  isAdministrador: boolean = false;
  isSupervisor: boolean = false;
  saleSuccessSubscription: Subscription;
  @ViewChild('newSale') newSaleRef: ElementRef;
  @ViewChild(LatestSalesComponent) latestSaleComponent: LatestSalesComponent;
  urlReportes: string = '';



  ngAfterViewInit() {
    if ( this.oAuthService.isLoginAuto ) return;
    
    this.latestSaleComponent.enviarFiltro();
  }

  constructor(private newSaleService: NewSaleService,
              private dashboardService: DashboardService,
              private oAuthService: OAuthService) {}

  ngOnInit() {
    this.setSteps();
    // Verificacion de login automatico.
    if ( this.oAuthService.isLoginAuto ) {
      this.stepNumber = 0;
      this.nextStep({ index: 1, beforeStepState: State.completed });
    } else {
      this.stepNumber = 0;
    }
    this.state = State;
    this.isModalActive = false;
    this.isProcessing = false;
    this.step = this.steps[this.stepNumber];

    this.oAuthService.getUserData().subscribe((user: UserModel) => {
      this.user = user;
      const { cuit, administrador, supervisor } = this.user;
      this.newSaleService.userId = cuit;
      if(administrador){
        this.isAdministrador = true;
        this.isSupervisor = true;
        //carga de url para iframe de reportes. url.dashboar urlDashboard url_dashboard
        this.urlReportes = this.user.url_dashboard;
        //this.urlReportes =
         //'https://portalsocios-test.cardif.com.ar/dashboard/dashboard.php?q=eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJjdWl0IjoiMzA2ODczMTA0MzQiLCJpYXQiOjE1MTYyMzkwMjJ9.MbACJ6QdGTX0SQsK5O0nXWZFqaIJehDSgEPBdSr9NvYAoUdxC_lBeUoMU6o9mabOSGJK8oHWAoYWUTRoFY0FdQ/token';
      }
      if(supervisor){
        this.isAdministrador = false;
        this.isSupervisor = true;
      }

    });

    this.saleSuccessSubscription = this.dashboardService.isSaleSuccess.subscribe(
      (isSuccess) => {
        if (isSuccess) {
          this.dashboardService.setSaleSuccess(false);
          this.resetProcess(true);
        }
      }
    );
  }
  mostrarUltimas(){
    var mostrar= this.step.name == this.SEARCH && !this.isProcessing;
    // if(mostrar && this.latestSaleComponent)
    //   this.latestSaleComponent.cargarDatos();
    return mostrar;
  }
  cancelOperation() {
    this.isModalActive = true;
  }

  closeModal(event) {
    this.isModalActive = event;
  }

/*   nextStep() {
    this.steps[this.stepNumber].state = State.completed;
    this.stepNumber += 1;
    this.showCurrentStep();
  } */

  nextStep(event) {
    if(event!=undefined && event.index==-1){
      this.steps[3].state = State.pending;
      this.stepNumber -= 1;
      this.showCurrentStep();
    }else{
      this.steps[this.stepNumber].state = State.completed;
      this.stepNumber += 1;
      if(this.stepNumber == 5){
        this.stepNumber = 4;
        this.steps[this.stepNumber].state = State.active;
      }
      this.showCurrentStep();
    }

  }

  backStep(index: number) {

    /**
     * gerrDev: logica para saltear paso 3 en caso de ser un producto de warranty
     */
    if(this.stepNumber == 4 && index == 2 || this.stepNumber == 4 && index == 3){
      this.steps[4].state = State.pending;
      this.steps[3].state = State.pending;
      this.stepNumber = 2;
      this.newSaleService.selectedWarrantyPlan = null;
      this.newSaleService.warranty = null;
      this.newSaleService.saleRequest.venta.isWarranty = false;

      this.showCurrentStep();
    }
    const previous = this.stepNumber - 1;

    if (previous === index) {
      this.steps[this.stepNumber].state = State.pending;
      this.stepNumber -= 1;
      this.showCurrentStep();
    }
  }

  toggleSpinner(status: boolean) {
    this.isProcessing = status;
  }

  private setSteps() {
    this.steps = [
      { name: 'search', state: State.pending },
      { name: 'personalData', state: State.pending },
      { name: 'availableProducts', state: State.pending },
      { name: 'saleData', state: State.pending },
      { name: 'pay&consent', state: State.pending }
    ];
  }

  resetProcess($event: boolean) {
    if ($event) {
      this.dashboardService.hiddenActivado = false;
      this.dashboardService.reset().subscribe(() => {
        this.setSteps();
        this.stepNumber = 0;
        this.step = this.steps[this.stepNumber];
      });
    }
  }

  private showCurrentStep() {
    this.steps[this.stepNumber].state = State.active;
    this.step = this.steps[this.stepNumber];
    this.newSaleRef.nativeElement.scrollIntoView();
  }

}
