import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

import { PasswordValidation } from './password-check-validator';
import { Router } from '@angular/router';
import { NotificationService } from './../../../shared/services/notification.service';
import { DashboardService } from '../../services/dashboard.service';
import { Subscription } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { OAuthService } from './../../../shared/services/oauth.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  changePassForm: FormGroup;
  submitted: boolean;
  isInfoVisible: boolean;
  showSuccess: boolean;
  isLoading: boolean;
  errorMsg: string;
  minPassLength = 8;
  ChangePassSubscription: Subscription;

  constructor(private router: Router,
              private oauthService: OAuthService,
              private dashboardService: DashboardService,
              private notificationService: NotificationService) { }

  ngOnInit() {
    this.submitted = false;
    this.isLoading = false;
    this.showSuccess = false;
    this.isInfoVisible = false;
    this.errorMsg = 'Ingrese y repita la contraseña!';
    this.changePassForm = new FormGroup({
      password: new FormControl('', {
        validators: [ Validators.required ]
      }),
      newpassword: new FormControl('', {
        updateOn: 'change',
        validators: [
          Validators.required,
          Validators.minLength(this.minPassLength),
          Validators.pattern(/^(?=.*[A-Za-z]{2,})(?=.*\d)(?=.*[$@$!<>,;°_=.\-:+%*#?&])[A-Za-z\d$@$!<>,;°_=.\-:+%*#?&]{2,}$/)
        ]
      }),
      newpassword2: new FormControl('', {
        updateOn: 'change',
        validators: [ Validators.required ]
      })
    }, {
      validators: [PasswordValidation.CheckPasswords]
    });

    this.changePassForm.valueChanges.pipe(debounceTime(500))
      .subscribe(() => this.checkPassErrors());
  }

  showInfo() {
    this.isInfoVisible = !this.isInfoVisible;
  }

  checkPassErrors() {
    // require -> minlength -> pattern -> sameAsOldPass -> differentPass
    if (this.changePassForm.invalid) {
      if (this.changePassForm.get('newpassword').getError('required')) {
        this.errorMsg = 'Ingrese y repita la contraseña!';
      } else {
        if (this.changePassForm.get('newpassword').getError('minlength') || this.changePassForm.get('newpassword').getError('pattern')) {
          this.errorMsg = 'La nueva contraseña no tiene el formato correcto!';
        } else {
          if (this.changePassForm.get('newpassword').getError('sameAsOldPass')) {
            this.errorMsg = 'La nueva contraseña debe ser distinta a la anterior!';
          } else {
            if (this.changePassForm.get('newpassword2').getError('differentPass')) {
              this.errorMsg = 'Las contraseñas no coinciden!';
            }
          }
        }
      }
    }
  }

  back()
  {
    this.router.navigate(['']);
  }

  onSubmit() {
    this.submitted = true;
    this.isLoading = true;

    if (!this.changePassForm.invalid) {
      const oldpass = this.changePassForm.get('password').value;
      const newpass = this.changePassForm.get('newpassword').value;
      this.ChangePassSubscription = this.dashboardService.SetNewPassword(oldpass, newpass, this.oauthService.tokenObject.mail,
         this.oauthService.userData.cuit).subscribe(
        (response: HttpResponse<any>) => {
          if (response.status === 200) {
            this.notificationService.showChangePasswordSucces();
            this.isLoading = false;
            this.router.navigate(['']);
          }
        }, error => {
          this.notificationService.showChangePasswordError();
          this.isLoading = false;
        });
    }
  }

  ngOnDestroy() {
    if (this.ChangePassSubscription) {
      this.ChangePassSubscription.unsubscribe();
    }
  }
}
