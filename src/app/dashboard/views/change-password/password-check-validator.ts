import { AbstractControl } from '@angular/forms';

export class PasswordValidation {

  static CheckPasswords(control: AbstractControl) {
    const originalPasword = control.get('password').value;
    const password = control.get('newpassword').value;
    const password2 = control.get('newpassword2').value;

    if (!password.length)  {
      return null;
    }

    if (originalPasword !== password) {
      if (password2.length > 0) {
        if (password !== password2) {
          control.get('newpassword2').setErrors({ differentPass: true });
        } else {
          control.get('newpassword2').setErrors(null);
          return null;
        }
      } else {
        return null;
      }
    } else {
      control.get('newpassword').setErrors({ sameAsOldPass: true });
    }
  }
}
