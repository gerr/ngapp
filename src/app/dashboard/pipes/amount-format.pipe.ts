import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'amountFormat'
})
export class AmountFormatPipe implements PipeTransform {
  transform(value: string, separator: string = '.'): string {
    if(value===undefined || value===null || value==='')
      return '-';
    const newValue = value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator);
    var str = newValue.split("." );
    if(str.length==1)
      return `$ ${newValue}`;
    if(str[1].length == 1){
      return `$ ${newValue}0`
    }
    return `$ ${newValue}`;
  }
}
