import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'arrayvalue'
})
export class ArrayValuePipe implements PipeTransform {
  transform(value: Array<any>, key: any ): any {
    return value[key];
  }
}
