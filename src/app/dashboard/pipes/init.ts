import { DecodePipe } from './decode.pipe';
import { AmountFormatPipe } from './amount-format.pipe';
import { DashboardSelectPipe } from './dashboard-select.pipe';
import { ArrayValuePipe } from './arrayvalue';


export const PIPES = [
  DecodePipe,
  AmountFormatPipe,
  DashboardSelectPipe,
  ArrayValuePipe
];
