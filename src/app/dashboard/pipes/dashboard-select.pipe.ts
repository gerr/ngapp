import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'dashboardSelect'})
export class DashboardSelectPipe implements PipeTransform {

  transform(value: string): string {
    if (value.length >= 5) {
      value = value.toLocaleLowerCase();
      value = value.substr(0, 1).toLocaleUpperCase() + value.substr(1, value.length - 1);
    } else {
      value = value.toLocaleUpperCase();
    }

    return value;
  }
}
