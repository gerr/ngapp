import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

import * as moment from 'moment';

@Directive({
  selector: '[appBorn][ngModel]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: BornValidatorDirective,
    multi: true
  }]
})
export class BornValidatorDirective implements Validator {
  @Input() pattern: RegExp;
  @Input() appBorn: boolean;

  validate({ value }: AbstractControl): { [key: string]: any } | null {
    let result = null;

   if (this.pattern.test(value)) {
    let date = value.split('/').reverse().join('-');
    date = moment(date);

    if (!date.isValid) {
      result = null;
    } else {
      const days = moment().diff(date, 'days');
      result = days < 1 ? { pastDate: 'Fecha de nacimiento no válida' } : null;
    }
   }

  return result;
  }
}
