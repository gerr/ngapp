import { ProductTemplateDirective } from './product.directive';
import { BornValidatorDirective } from './born-validator.directive';
import { AdultValidatorDirective } from './adult-validator.directive';

export const DIRECTIVES = [
  AdultValidatorDirective,
  BornValidatorDirective,
  ProductTemplateDirective
];
