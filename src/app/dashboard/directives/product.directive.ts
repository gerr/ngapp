import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appProductTemplateHost]'
})
export class ProductTemplateDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}
