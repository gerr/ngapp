import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

import * as moment from 'moment';

@Directive({
  selector: '[appAdult][ngModel]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: AdultValidatorDirective,
    multi: true
  }]
})
export class AdultValidatorDirective implements Validator {
  @Input() pattern: RegExp;
  @Input() appAdult: boolean;

  validate({ value }: AbstractControl): { [key: string]: any } | null {
    let result = null;

    if (!this.appAdult) {
      return result;
    }

    if (this.pattern.test(value)) {
      let date = value.split('/').reverse().join('-');
      date = moment(date);

      if (!date.isValid) {
        result = null;
      } else {
        const years = moment().diff(date, 'years');
        result = years < 18 ? { adult: 'Debe ser mayor de edad' } : null;
      }
    }

    return result;
  }
}
