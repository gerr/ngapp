import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

import {
  BrandCatalogResponse,
  LineCatalogResponse,
  SubLineCatalogResponse,
  ProductCatalogResponse
} from '../../models/services/services-response.model';
import { State } from '../../dashboard.enums';
import { CatalogService } from '../../services/catalog.service';
import { TechnologyForm } from '../../models/products/products.model';
import { Brand, Line, SubLine, Product, ProductBySubLine } from '../../models/services/services.model';

@Component({
  selector: 'app-technology-form',
  templateUrl: './technology-form.component.html',
  styleUrls: ['./technology-form.component.scss']
})
export class TechnologyFormComponent implements OnInit {
  msgError: string;
  hasIMEI: boolean;
  lines: Line[];
  brands: Brand[];
  subLines: SubLine[];
  products: Product[];
  hasSerialNumber: boolean;
  subLineaList: SubLineCatalogResponse;

  @Input() model: TechnologyForm;

  @Output() handleSubmit: EventEmitter<TechnologyForm> = new EventEmitter<TechnologyForm>();

  @ViewChild('f') form: any;

  constructor(private catalogService: CatalogService) {}

  ngOnInit() {
    this.lines = [];
    this.brands = [];
    this.subLines = [];
    this.products = [];
    this.model.marca = State.empty;
    this.model.linea = State.empty;
    this.model.subLinea = State.empty;
    this.model.producto = State.empty;
    this.msgError = 'Seleccione una opción válida';
    this.hasIMEI = true;
    this.hasSerialNumber = true;
    this.fullSelects();
  }

  onChangeProduct() {
    const { producto: id } = this.form.value;
    const { hasImei, hasNroSerie } = this.products.find(p => p.idProducto.trim() === id);
    this.hasIMEI = hasImei;
    this.hasSerialNumber = hasNroSerie;
    this.model.imei = State.empty;
    this.model.nroSerie = State.empty;
  }

  onChangeLine() {
/*    const { linea: id } = this.form.value;

    if (!id) {
      this.subLines = [];
    } else {
      const found = this.subLineaList.body.find(item => {
        return item.idLinea === id;
      });

      if (found) {
        //this.subLines = found.sublineas;
      } else {
        this.subLines = [];
      }
    }

    this.model.subLinea = State.empty;*/
  }

  nextAction() {
    if (this.form.valid) {
      this.handleSubmit.emit(this.form.value);
    }
  }

  private fullSelects() {
    this.catalogService.getBrandCatalog()
      .subscribe((response: BrandCatalogResponse) => {
        this.brands = response.body.marcas;
      });

    this.catalogService.getLineCatalog()
      .subscribe((response: LineCatalogResponse) => {
        this.lines = response.body.lineas;
      });

    this.catalogService.getSubLineCatalog()
      .subscribe((response: SubLineCatalogResponse) => {
        this.subLineaList = response;
      });

    this.catalogService.getProductCatalog()
      .subscribe(({ body }: ProductCatalogResponse) => {
        this.products = body.reduce((base: Product[], item: ProductBySubLine) => {
          return base.concat(item.productos);
        }, []);
      });
  }
}
