import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { State } from '../../dashboard.enums';

@Component({
  selector: 'app-items-form',
  templateUrl: './items-form.component.html',
  styleUrls: ['./items-form.component.scss']
})
export class ItemsFormComponent implements OnInit {
  item: string;
  isEmail: boolean;
  exist: boolean;
  pattern:string="";

  @Input() title: string;
  @Input() items: Object[];
  @Input() attribute: string;
  @Input() maxItems: number = 3;
  @Input() type: string = State.empty;
  @Input() maxlength: number = 99999999999;


  @Output() addItem: EventEmitter<Object> = new EventEmitter<Object>();
  @Output() removeItem: EventEmitter<Object> = new EventEmitter<Object>();

  constructor() {}

  ngOnInit() {
    this.item = State.empty;
    this.isEmail = this.type === 'email';
    this.exist = false;
    if(this.isEmail)
      this.pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,63}$";
  }

  onSubmit() {
    this.addItem.emit({attribute: this.attribute, value: this.item });
    this.item = State.empty;
  }

  onKeyUp() {
    this.exist = this.items.includes(this.item);
  }

  onClick(item: string) {
    this.removeItem.emit({ attribute: this.attribute, value: item });
  }
}
