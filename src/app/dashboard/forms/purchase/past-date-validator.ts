import { AbstractControl, ValidatorFn } from '@angular/forms';

import * as moment from 'moment';

export function pastDateValidator(pattern: RegExp): ValidatorFn {
 return ({ value }: AbstractControl): {[key: string]: any} | null => {
   let result = null;

   if (pattern.test(value)) {
    let date = value.split('/').reverse().join('-');
    date = moment(date);

    if (!date.isValid) {
      result = null;
    } else {
      const days = moment().diff(date, 'days');
      result = days > -1 ? { pastDate: 'Ingrese una fecha válida ' } : null;
    }
   }

  return result;
 };
}
