import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { State } from '../../dashboard.enums';
import { pastDateValidator } from './past-date-validator';
import { PurchaseForm } from '../../models/products/products.model';

@Component({
  selector: 'app-purchase-form',
  templateUrl: './purchase-form.component.html',
  styleUrls: ['./purchase-form.component.scss']
})
export class PurchaseFormComponent implements OnInit {
  pattern: RegExp;
  cleaveDate: Object;
  cleaveAmount: Object;
  purchaseForm: FormGroup;

  @Input() model: PurchaseForm;

  @Output() handleSubmit: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {
    this.pattern = /^(0?[1-9]|[12][0-9]|3[01])[\/](0?[1-9]|1[012])[\/]\d{4}$/;
    this.purchaseForm = new FormGroup({
      montoCompra: new FormControl(State.empty, [
        Validators.required,
        Validators.minLength(3),
        Validators.pattern(/^[\$] [1-9][0-9,.]*$/)
      ]),
      tiempoAsegurar: new FormControl(State.empty, [
        Validators.required,
        pastDateValidator(this.pattern),
        Validators.pattern(this.pattern)
      ]),
      descripcionBienAsegurado: new FormControl(State.empty, [
        Validators.maxLength(300)
      ])
    });
    this.cleaveDate = { date: true };
    this.cleaveAmount = {
      prefix: '$ ',
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
    };
  }

  nextAction() {
    if (this.purchaseForm.valid) {
      const model = this.purchaseForm.value;
      const regex = /[$| |,]/g;
      model.montoCompra = model.montoCompra.replace(regex, State.empty);
      model.montoCompra = parseFloat(model.montoCompra);
      this.handleSubmit.emit(model);
    }
  }
}
