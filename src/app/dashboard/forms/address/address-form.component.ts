import {Component, OnInit, Input, Output, EventEmitter, ViewChild} from '@angular/core';
import {debounceTime} from 'rxjs/operators';

import {CatalogueService} from '../../../shared/services/catalogue.service';
import {State} from '../../dashboard.enums';
import {CatalogService} from '../../services/catalog.service';
import {ProvinceCatalogResponse} from '../../models/services/services-response.model';
import { Localidad, LocalidadesResponse } from '../../../shared/models/catalogue-models/localidades-response.model';
import {DashboardService} from '../../services/dashboard.service';
import {NewSaleService} from '../../services/new-sale.service';

@Component({
  selector: 'app-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.scss']
})
export class AddressFormComponent implements OnInit {
  provinceList = [];
  localityList = [];
  localityListFiltered: Array<Localidad> = [];
  typeCpa = 'CPA';
  typeAlt = 'ALT';
  CPALink: string;

  @Input() model: any;

  @Output() handleSubmit: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('f') form: any;

  constructor(private catalogService: CatalogService,
              private catalogueService: CatalogueService,
              private newSaleService: NewSaleService) {
  }

  ngOnInit() {
    this.fullSelects();

    this.CPALink = 'https://www.correoargentino.com.ar/formularios/cpa';
    // this.form.statusChanges.pipe(debounceTime(5000)).subscribe((status: string)  => {
    //   const event = status === State.valid ? this.model : null;
    //   this.handleSubmit.emit(event);
    // });
  }

  cargar() {
    // se pisan datos de la direccion original porque el back no tiene el modelo para recibir el domicilio sumado.
    this.newSaleService.saleRequest.venta.cliente.calle = this.model.calle;
    this.newSaleService.saleRequest.venta.cliente.numero = this.model.numero;
    this.newSaleService.saleRequest.venta.cliente.codigoPostal = this.model.codigoPostal;
    this.newSaleService.saleRequest.venta.cliente.departamento = this.model.departamento;
    this.newSaleService.saleRequest.venta.cliente.piso = this.model.piso;
    this.newSaleService.saleRequest.venta.cliente.torre = this.model.torre;
    this.newSaleService.saleRequest.venta.cliente.idLocalidad = this.model.idLocalidad;
    this.newSaleService.saleRequest.venta.cliente.localidad = this.localityListFiltered.find(( { idLocalidad }: any ) => idLocalidad === this.model.idLocalidad );
    this.newSaleService.saleRequest.venta.cliente.idProvincia = this.model.idProvincia == "" ? this.model.id_provincia : this.model.idProvincia;
    this.handleSubmit.emit(this.form);
  }

  handleChange() {
    let id;
    this.model.idProvincia == "" ? id = this.model.id_provincia : id = this.model.idProvincia;
    this.catalogueService.getLocalidades(id)
      .subscribe((body: any) => {
        var filtered = body.body;
        if (filtered) {
          this.localityListFiltered = filtered;
          this.model.idLocalidad = '';
        } else {
          this.localityListFiltered = [];
        }
      });
  }

  private fullSelects() {
    this.catalogService.getProvinceCatalog()
      .subscribe(({body}: ProvinceCatalogResponse) => {
        this.provinceList = body;
        this.model.idProvincia = this.model.idProvincia || State.empty;
        if (this.model.idProvincia == State.empty) {
          this.localityListFiltered = [];
        }
      });
/*
    this.catalogueService.getLocalidades(this.model.idProvincia).subscribe(({body}: any) => {
      this.localityList = body;
      if (this.model.idProvincia) {
        this.handleChange();
      }
      this.model.idLocalidad = this.model.idLocalidad || State.empty;
    });*/
  }
}
