import { ItemsFormComponent } from './items/items-form.component';
import { ClientFormComponent } from './client/client-form.component';
import { AddressFormComponent } from './address/address-form.component';
import { PurchaseFormComponent } from './purchase/purchase-form.component';
import { TechnologyFormComponent } from './technology/technology-form.component';
import { WarrantyFormComponent } from './warranty/warranty-form.component';

export const FORMS = [
  ItemsFormComponent,
  ClientFormComponent,
  AddressFormComponent,
  PurchaseFormComponent,
  TechnologyFormComponent,
  WarrantyFormComponent
];

