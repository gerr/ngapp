import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

import {
  BrandCatalogResponse,
  LineCatalogResponse,
  SubLineCatalogResponse,
  ProductCatalogResponse
} from '../../models/services/services-response.model';
import { State } from '../../dashboard.enums';
import { CatalogService } from '../../services/catalog.service';
import { WarrantyForm } from '../../models/products/products.model';
import { Brand, Line, SubLine, Product, SubLineByLine, ProductBySubLine } from '../../models/services/services.model';

@Component({
  selector: 'app-warranty-form',
  templateUrl: './warranty-form.component.html',
  styleUrls: ['./warranty-form.component.scss']
})
export class WarrantyFormComponent implements OnInit {
  msgError: string;
  hasIMEI: boolean;
  lines: Line[];
  brands: Brand[];
  subLines: SubLine[];
  products: Product[];

  @Input() model: WarrantyForm;

  @Output() handleSubmit: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('f') form: any;

  constructor(private catalogService: CatalogService) {}

  ngOnInit() {
    this.lines = [];
    this.brands = [];
    this.subLines = [];
    this.products = [];
    this.hasIMEI = true;
    this.fullSelects();
    this.model.marca = State.empty;
    this.model.producto = State.empty;
    this.msgError = 'Seleccione una opción válida';
  }

  onChangeProduct() {
    const { producto: id } = this.form.value;
    const { hasImei } = this.products.find(p => p.idProducto.trim() === id);
    this.hasIMEI = hasImei;
    this.model.imei = State.empty;
  }

  nextAction() {
    if (this.form.valid) {
      this.handleSubmit.emit(this.form.value);
    }
  }

  private fullSelects() {
    this.catalogService.getBrandCatalog()
      .subscribe((response: BrandCatalogResponse) => {
        this.brands = response.body.marcas;
      });

    this.catalogService.getLineCatalog()
      .subscribe((response: LineCatalogResponse) => {
        this.lines = response.body.lineas;
      });

    /*this.catalogService.getSubLineCatalog()
      .subscribe((response: SubLineCatalogResponse) => {
        this.subLines = response.body.reduce((base, item) => {
          return base.concat(item.sublineas);
        }, []);

        const found = response.body.find((line: any) => {
          return line.sublineas.find(sub => {
            return sub.id.trim() === this.model.subLinea.trim();
          });
        });

        this.model.linea = found.idLinea;
      });*/

    this.catalogService.getProductCatalog()
      .subscribe(({ body }: ProductCatalogResponse) => {
        this.products = body.reduce((base: Product[], item: ProductBySubLine) => {
          return base.concat(item.productos);
        }, []);
      });
  }
}
