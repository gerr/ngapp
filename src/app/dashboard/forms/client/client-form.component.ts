import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';

import { State } from '../../dashboard.enums';
import { CatalogueService } from '../../../shared/services/catalogue.service';
import { CatalogResponse } from '../../models/services/services-response.model';
import { CatalogService } from '../../services/catalog.service';
import { Gender, CivilStatus } from '../../models/services/services.model';
import { LocalidadesResponse } from '../../../shared/models/catalogue-models/localidades-response.model';
import { NewSaleService } from '../../services/new-sale.service';
import { NgForm } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { SortPipe } from '../../../shared/pipes/sort.pipe';

@Component ( {
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: [ './client-form.component.scss' ]
} )
export class ClientFormComponent implements OnInit, OnDestroy {
  CPALink: string;
  documentTypeList: Array<any> = [];
  genderList: Gender[] = [];
  nationalityList: Array<any> = [];
  civilStatusList: CivilStatus[] = [];
  activityList: Array<any> = [];
  workRelationList: Array<any> = [];
  provinceList: Array<any> = [];
  localityList: Array<any> = [];
  localityListFiltered: Array<any> = [];
  personalRelationList: Array<any> = [];
  fecha: string;
  localidad: string;
  porcentajeAceptado: boolean = false;

  email: String[];
  celular: String[];
  telefonoFijo: String[];
  datePattern: RegExp;
  isEmailInvalid: boolean = false;
  isPhoneInvalid: boolean = false;
  typeCpa = 'CPA';
  typeAlt = 'ALT';
  disabled = {
    tipoDocumento: false,
    nroDocumento: false,
    apellido: false,
    nombre: false,
    fechaNacimiento: false,
    cuil: false,
    idGenero: false
  };

  @Input () model: any;
  @Input () title: string;
  @Input () type: String = 'owner';
  @Input () bornRequired: boolean = false;
  @Input () adultRequired: boolean = false;
  @Input () isLoading: boolean = false;
  @Input () showCancelButton: Boolean = false;
  @Input () principal: boolean = false;
  @Input () porcentajeAcumulado: number = 0;

  @Output () handleSubmit: EventEmitter<any> = new EventEmitter<any> ();
  @Output () handleCancel: EventEmitter<any> = new EventEmitter<any> ();
  isPhoneFijoInvalid: boolean = false;
  errorCelLenght: boolean = false;
  private telefonoFijo1 = '';
  telefonoFijo3: '';
  telefonoFijo2: '';
  celular1: '';
  celular2: '';
  celular3: '';
  msjErrorCelular: string = 'Es obligatorio un telefono';

  constructor ( private catalogService: CatalogService,
                private catalogueService: CatalogueService,
                private newSaleService: NewSaleService,
                private sortPipe: SortPipe) {}

  ngOnInit () {
    //correccion en el modelo para usar snake_case, cuando ya estaba camelCase en front y back.
    if(this.model.domicilio ) this.correcionCargarDatosDelCliente ();// si existe cliente, hace las correcciones.
    this.email = [];
    this.celular = [];
    this.telefonoFijo = [];
    this.disableAttributes ();
    this.fullSelects ();
    this.CPALink = 'https://www.correoargentino.com.ar/formularios/cpa';
    this.datePattern = /^(0?[1-9]|[12][0-9]|3[01])[\/](0?[1-9]|1[012])[\/](1?[89]|2[01])\d{2}$/;
    //this.datePattern=/.*/
    // if (this.model.fechaNacimiento) {
    //this.model.fechaNacimiento = this.formatDate(this.model.fechaNacimiento);

    /**
     * correccion para limpiar celulares sucios de la bdd o con mas de 10 digitos.
     */
    this.correcionLimpiarCaracteresDeTelefonos();
    this.fecha = this.newSaleService.client.fechaNacimiento;
    this.loadItems ( 'email' );
    this.loadItems ( 'celular' );
    this.loadItems ( 'telefonoFijo' );

    const { inicioCuil, digitoVerificador, nroDocumento } = this.model;
    if ( inicioCuil && digitoVerificador ) {
      //this.model.cuil = `${inicioCuil}${nroDocumento}${digitoVerificador}`;
    }

    this.localidad = this.newSaleService.client.idLocalidad;
    this.correcionLimpiarCaracteresDeTelefonos();
  }

  calcular ( valor: string ): number {
    return this.porcentajeAcumulado + parseInt ( valor );


  }

  loadItems ( attr: string ) {
    Object.keys ( this.model ).forEach ( key => {
      if ( key.includes ( attr ) && this.model[key] ) {
        console.log(this.model[key]);
        this[attr].push ( this.model[key] );
      }
    } );
  }

  addItem ( { attribute, value }: any ) {
    if ( ( attribute === 'celular' && value.length != 10 ) || ( attribute === 'telefonoFijo' && value.length != 10 ) ) {
      return;
    }
    if ( attribute == 'email' ) {
      this.isEmailInvalid = false;
      if ( this[attribute] ) {
        const len = this[attribute].push ( value );
        this.model[`${ attribute }${ len }`] = value;
      }
    }
    if ( attribute == 'telefonoFijo' || attribute == 'celular' ) {
      value = this.limpiarCelular ( value );
      this.isPhoneInvalid = false;
      if ( this[attribute] ) {
        const len = this[attribute].push ( value );
        this.model[`${ attribute }${ len }`] = value;
      }
    }

    if (!this.model.telefonos) {
      this.model.telefonos = [];
    } else {
      if ( attribute == 'telefonoFijo' ) {
        this.model.telefonos.push ( { 'tipo_telefono': 'FIJO', 'cod_area': value.substring ( 0, 2 ).toString (), 'numero': value.substring ( 2, 10 ).toString () } );
      }
      if ( attribute == 'celular' ) {
        this.model.telefonos.push ( { 'tipo_telefono': 'CEL', 'cod_area': value.substring ( 0, 2 ).toString (), 'numero': value.substring ( 2, 10 ).toString () } );
      }
    }
    if ( attribute == 'email' ) {
      this.model.email.push ( value );
    }

  }

  removeItem ( { attribute, value }: any ) {
    //remueve item del array de string propio del componente
    if ( attribute === 'telefonoFijo' ) {
      this.isPhoneFijoInvalid = false;

    }
    if ( this[attribute] ) {
      const len = this[attribute].length;
      this[attribute] = this[attribute].filter ( item => item !== value );
      if ( attribute == 'email' && len == 1 ) {
        this.model[`${ attribute }`][0] = State.empty;
        this.email = [];
      } else {
        for ( let i = 0; i < len; i++ ) {
          this.model[`${ attribute }${ i + 1 }`] = this[attribute][i] || State.empty;
        }
      }

    }

    //remueve el item del objeto telefonos, de la clase, la que se envia en la venta.

    if ( (attribute == 'telefonoFijo' || attribute == 'celular') && this.model.telefonos) {
      let filtro = this.model.telefonos.filter ( function ( item ) {
        let num = limpiarCelular ( item.cod_area + item.numero );

        if ( num === value ) {
          return false;
        } else {
          return true;
        }

        //se repite funcion limpiarCelular porque no acepta el objeto this dentro del filter
        function limpiarCelular ( numero: string ) {
          let numeroLimpio = numero.replace ( /[^\d.]/g, '' );
          let tam = numeroLimpio.length;
          let num = numeroLimpio.substr ( tam - 8, tam );
          let area = numeroLimpio.substr ( 0, tam - 8 );
          let areaToArray = area.split ( '' );
          if ( areaToArray[0] === '0' ) {
            area = area.substr ( 1, area.length );
          }
          let numeroCompleto = area + num;
          let tamanio = numeroCompleto.length;
          let inicio = tamanio - 10;
          return numeroCompleto.length > 10 ? numeroCompleto.substring(inicio, tamanio) : numeroCompleto;
        }
      } );

      this.model.telefonos = [];
      this.model.telefonos = filtro;
    }

    if ( attribute == 'email' ) {
      let filtrados = this.model.email.filter ( item => item === value );
      this.model.email = [];
      this.model.email = filtrados;
    }

  }

  ngOnDestroy () {}

  handleChange () {
    const id = this.model.idProvincia;
    if ( this.model.idLocalidad == null ) {
      this.model.idLocalidad = '';
    }
    //const filtered = this.localityList.find(loc => loc.idProvincia === id); gerr: 29/04 comentado para cambiar por mi codigo
    this.catalogueService.getLocalidades ( id )
      .subscribe ( ( { body }: LocalidadesResponse ) => {
        let filtered = body;
        if ( filtered ) {
          this.localityListFiltered = filtered;
          this.sortPipe.transform(this.localityListFiltered, 'localidad', true);

        } else {
          this.localityListFiltered = [];
        }
      } );
    
    this.model.provincia = this.provinceList.find( (item)=> {
      return item.id === this.model.idProvincia.toString()? item.descripcion : '';
    });
  
  
  }
  //update
  //boton siguiente
  handleClick ( f: NgForm ) {
    if ( !( f.invalid || this.isLoading || !this.email.length || !this.celular.length ) ) {
      this.submit ();
    } else {
      //Marco todos los controles como tocados para que marque los invalidos
      for ( let i in f.controls ) {
        f.controls[i].markAsTouched ();
      }
      if ( !this.email.length ) {
        this.isEmailInvalid = true;
      }
      if ( !this.celular.length ) {
        this.isPhoneInvalid = true;
      }
    }
  }

  limpiarCelular ( celular: string, detalle = 'Telefono' ) {

    let celularLimpio = celular.replace ( /[^\d.]/g, '' );

    let tam = celularLimpio.length;
    let num = celularLimpio.substr ( tam - 8, tam );
    let area = celularLimpio.substr ( 0, tam - 8 );
    let areaToArray = area.split ( '' );
    if ( areaToArray[0] === '0' ) {
      area = area.substr ( 1, area.length );
    }
/*    if ( (area + num).length > 10 ) {
      //celularLimpio = celularLimpio.substring ( celularLimpio.length - 10, celularLimpio.length );
      // el telefono tiene demasiados caracteres.
      this.msjErrorCelular = 'el ' + detalle + ' tiene demasiado caracteres.';
      this.isPhoneInvalid = true;
    }*/
    let numeroCompleto = area + num;
    let tamanio = numeroCompleto.length;
    let inicio = tamanio - 10;
    return numeroCompleto.length > 10 ? numeroCompleto.substring(inicio, tamanio) : numeroCompleto;
  }

  private submit () {

    const { cuil, codigoPostal, localidad } = this.model;
    //correccion para codigo postal y localidad
    //TODO a mejorar!
    //this.newSaleService.saleRequest.venta.cliente.codigoPostal = codigoPostal;
    this.model.localidad = this.localityListFiltered.find( (item)=> {
      if (item.idLocalidad === this.model.idLocalidad.toString()) {
      
        return item.localidad;
      }
    });
    //this.newSaleService.saleRequest.venta.cliente.localidad = this.model.localidad;
    this.newSaleService.saleRequest.venta.cliente.codigoPostal = this.model.codigoPostal;
    this.newSaleService.saleRequest.venta.cliente.localidad = this.localityListFiltered.find( (item)=> {
      if (item.idLocalidad === this.model.idLocalidad.toString()) {
    
        return item.localidad;
      }
    });
    /**
     * correccion celulares vacios que al volver de paso siguiente se podian borrar y pasaban vacios. tambien cuando vienen sucios de la bdd.
     */
    this.correcionAlVolver();

    if ( cuil ) {
      const len = cuil.length - 1;
      this.model.inicioCuil = cuil.substr ( 0, 2 );
      this.model.digitoVerificador = cuil.substr ( len );
    }

    const clone = Object.assign ( {}, this.model );
    delete clone.cuil;

    this.handleSubmit.emit ( this.model );
  }

  private disableAttributes () {
    Object.keys ( this.disabled ).forEach ( key => {
      this.disabled[key] = !!this.model[key] || this.model[key] === 0;
    } );

    this.disabled.idGenero = ( this.model.nombre && this.model.apellido );
  }

  private formatDate ( date: string ) {
    return date.includes ( '-' ) ? date.split ( '-' ).reverse ().join ( '/' ) : date;
  }

  private fullSelects () {

    this.catalogueService.getDocumentType ().subscribe ( ( response: any ) => {
      this.documentTypeList = response.listadoItem;

      if ( this.model.tipoDocumento ) {
        if ( isNaN ( +this.model.tipoDocumento ) ) {
          this.documentTypeList.map ( doctype => {
            if ( doctype.userDocumentNumber === this.model.tipoDocumento ) {
              this.model.tipoDocumento = doctype.userDocumentType;
            }
          } );
        }
      } else {
        this.model.tipoDocumento = 0;
      }
    } );

    this.catalogService.getGenderCatalog ()
      .subscribe ( ( { body }: CatalogResponse ) => {
        this.genderList = body;
        this.model.idGenero = this.model.idGenero || State.empty;
      } );

    this.catalogueService.getNationalities ().subscribe ( ( { body }: any ) => {
      //this.nationalityList = body.nacionalidades_list;
      this.nationalityList = body; // gerr: 24/04

      this.model.idNacionalidad = this.model.idNacionalidad || State.empty;
    } );

    this.catalogService.getCivilStatusCatalog ()
      .subscribe ( ( { body }: CatalogResponse ) => {
        this.civilStatusList = body;
        this.model.idEstadoCivil = this.model.idEstadoCivil || State.empty;
      } );

    this.catalogueService.getActivities ().subscribe ( ( { body }: any ) => {
      //this.activityList = body.actividadList;
      this.activityList = body; // gerr: 24/04

      this.model.idActividad = this.model.idActividad || State.empty;
    } );

    this.catalogService.getWorkRelationsCatalog ().subscribe ( ( { body }: CatalogResponse ) => {
      this.workRelationList = body;
      this.model.idRelacionLaboral = this.model.idRelacionLaboral || State.empty;
    } );

    this.catalogService.getPersonalRelationCatalog ().subscribe ( ( { body }: CatalogResponse ) => {
      this.personalRelationList = body;
      //console.log("relacionLaboral: ", JSON.stringify(this.personalRelationList));
      this.model.relacionPersonal = State.empty;
    } );

    this.catalogService.getProvinceCatalog ().subscribe ( ( { body }: CatalogResponse ) => {
      this.provinceList = body;
      //console.log("provincias: ", JSON.stringify(this.provinceList));
      this.model.idProvincia = this.model.idProvincia || State.empty;
      this.handleChange ();
    } );

    /* gerr: 29/04 comentado para crear nuevo metodo, para elegir solo las localidades de la prov */
    this.catalogueService.getLocalidades ( this.newSaleService.client.idProvincia ).subscribe ( ( { body }: any ) => {
      this.localityList = body.localidades;
      this.newSaleService.localidades = this.localityList;
      if ( this.model.idProvincia ) {
        this.handleChange ();
      }
      this.model.idLocalidad = this.model.idLocalidad || State.empty;
    } );
  }

  correcionCargarDatosDelCliente () {
    if ( this.model.idLocalidad == null ) {
      this.model.idLocalidad = '';
    }
    if ( this.model.domicilio.idProvincia ) {
      this.model.idProvincia = this.model.domicilio.idProvincia;
    }
    if ( this.model.domicilio.idLocalidad ) {
      this.model.idLocalidad = this.model.domicilio.idLocalidad;
    }
    if ( this.model.domicilio.calle ) {
      this.model.calle = this.model.domicilio.calle;
    }
    if ( this.model.domicilio.numero ) {
      this.model.numero = this.model.domicilio.numero;
    }
    if ( this.model.domicilio.piso ) {
      this.model.piso = this.model.domicilio.piso;
    }
    if ( this.model.domicilio.departamento ) {
      this.model.departamento = this.model.domicilio.departamento;
    }
    if ( this.model.domicilio.torre ) {
      this.model.torre = this.model.domicilio.torre;
    }
    if ( this.model.domicilio.domicilio_codigo_postal ) {
      this.model.codigoPostal = this.model.domicilio.domicilio_codigo_postal;
    }
  }
  //update
  correcionLimpiarCaracteresDeTelefonos () {
    this.model.celular1 && this.model.celular1 ? this.model.celular1 = this.limpiarCelular ( this.model.celular1, 'celular 1' ) : this.model.celular1 = undefined;
    this.model.celular2 && this.model.celular2 ? this.model.celular2 = this.limpiarCelular ( this.model.celular2, 'celular 3' ) : this.model.celular2 = undefined;
    this.model.celular3 && this.model.celular3 ? this.model.celular3 = this.limpiarCelular ( this.model.celular3, 'celular 3' ) : this.model.celular3 = undefined;

    this.model.telefonoFijo1 && this.model.telefonoFijo1 ? this.model.telefonoFijo1 = this.limpiarCelular ( this.model.telefonoFijo1 ) : this.model.telefonoFijo1 = undefined;
    this.model.telefonoFijo1 && this.model.telefonoFijo1.length > 10 ? this.isPhoneFijoInvalid = true : this.isPhoneFijoInvalid = false;

    this.model.telefonoFijo2 && this.model.telefonoFijo2 ? this.model.telefonoFijo2 = this.limpiarCelular ( this.model.telefonoFijo2 ) : this.model.telefonoFijo2 = undefined;
    this.model.telefonoFijo2 && this.model.telefonoFijo2.length > 10 ? this.isPhoneFijoInvalid = true : this.isPhoneFijoInvalid = false;

    this.model.telefonoFijo3 && this.model.telefonoFijo3 ? this.model.telefonoFijo3 = this.limpiarCelular ( this.model.telefonoFijo3 ) : this.model.telefonoFijo3 = undefined;
    this.model.telefonoFijo3 && this.model.telefonoFijo3.length > 10 ? this.isPhoneFijoInvalid = true : this.isPhoneFijoInvalid = false;
  }

  private correcionAlVolver () {
    const {  celular1, celular2, celular3, telefonoFijo1, telefonoFijo2, telefonoFijo3 } = this.model;

    if ( celular1 && celular1.length > 10 ) {
      this.isPhoneInvalid = true;
      return;
    }

    if ( celular2 && celular2.length > 10 ) {
      this.isPhoneInvalid = true;
      return;
    }

    if ( celular3 && celular3.length > 10 ) {
      this.isPhoneInvalid = true;
      return;
    }

    if ( telefonoFijo1 && telefonoFijo1.length > 10 ) {
      this.isPhoneFijoInvalid = true;
      return;
    }

    if ( telefonoFijo2 && telefonoFijo2.length > 10 ) {
      this.isPhoneFijoInvalid = true;
      return;
    }

    if ( telefonoFijo3 && telefonoFijo3.length > 10 ) {
      this.isPhoneFijoInvalid = true;
      return;
    }

  }

  private correcionTelefonos () {
    this.model.celular1 && this.model.celular1.length > 10 ? this.isPhoneInvalid = true : this.isPhoneInvalid = false;
    this.model.celular2 && this.model.celular2.length > 10 ? this.isPhoneInvalid = true : this.isPhoneInvalid = false;
    this.model.celular3 && this.model.celular3.length > 10 ? this.isPhoneInvalid = true : this.isPhoneInvalid = false;
    this.model.telefonoFijo1 && this.model.telefonoFijo1.length > 10 ? this.isPhoneFijoInvalid = true : this.isPhoneFijoInvalid = false;
  }

}
