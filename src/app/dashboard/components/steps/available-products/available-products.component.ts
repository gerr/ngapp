import { Component, OnInit, Output, EventEmitter, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';

import { NewSaleService } from '../../../services/new-sale.service';
import { CatalogService } from '../../../services/catalog.service';
import { DashboardService } from '../../../services/dashboard.service';

import { State } from '../../../dashboard.enums';
import { Producto } from '../../../models/products/producto.model';
import { animate, state, style, transition, trigger } from "@angular/animations";

import { SubLineCatalogResponse } from '../../../models/services/services-response.model';
import {
  SubLine,
  Range,
  WarrantyProduct,
  WarrantyProductPrice,
  Rango,
  RespuestaRango,
  WarrantyResponse
} from '../../../models/services/services.model';
import { OAuthService } from '../../../../shared/services/oauth.service';
import { HttpErrorResponse } from "@angular/common/http";
import { throwError, throwError as observableThrowError } from "rxjs";
import { CatalogueObject } from "../../../../shared/models/catalogue-models/catalogue-object.model";
import { Plan } from "../../../models/products/plan.model";
import { environment } from "../../../../../environments/environment";
import { catchError } from "rxjs/operators";

@Component ( {
  selector: 'app-available-products',
  templateUrl: './available-products.component.html',
  styleUrls: [ './available-products.component.scss' ],
    animations: [
        trigger('animacion', [
          state('inactive', style({
            opacity: '0'
          })),
          state('active', style({
            opacity: '1'
          })),
          transition('inactive => active', animate('1000ms ease-in')),
          transition('active => inactive', animate('1000ms ease-out')),
        ])
      ]
} )
export class AvailableProductsComponent implements OnInit, AfterViewInit {
  id_socio = 30123456781;   // MOCK
  channel = 5;              // MOCK
  categories: Array<any>;
  categoryDesigns = {
    '1': { type: 'icon-icon_seguro', name: 'Seguros' },
    '2': { type: 'icon-icon_asistencia', name: 'Asistencias' },
    '3': { type: 'icon-icon_gextendida', name: 'Garantías Extendidas' }
  };
  state = State;
  noProducts: boolean;
  hasOneSelected: boolean;
  products: Array<Producto>;
  productsSegunNegocio: Array<Producto>;
  listaUno: Array<Producto>;
  plans: Array<any>;
  selectedProduct: Producto;
  selectedCategory: any;
  selectedWarrantyProduct: WarrantyProduct;
  subLineaItems: SubLine[];
  rangeItems: Array<Rango>;
  range: string;
  respuestaRango: RespuestaRango;
  subLine: string;
  precio: string;
  cuitSocio: string;
  pos: number;
  errorDeRango: boolean = false;
  listaPrdsUno: Producto[] = [];
  listaPrdsDos: Producto[] = [];
  listaPrdsTres: Producto[] = [];

  @Output () next: EventEmitter<any> = new EventEmitter<any> ();

  @ViewChild ( 'f' ) form: any;
  mostrarProducto: boolean = false;
  buttonValid: boolean = false;
  listaDos: Producto[];
  listaTres: Producto[];
  isLoading: boolean = false;
  private msjError: string = '';

  constructor ( private catalogService: CatalogService,
                private newSaleService: NewSaleService,
                private dashboardService: DashboardService,
                private userService: OAuthService,
                private cdRef:ChangeDetectorRef) {
  }

  /**
   * Carga los productos disponibles: seguros - asistencias - garantias
   */
  ngOnInit () {

    this.noProducts = false;
    this.hasOneSelected = false;
    this.categories = [];
    this.listaUno = [];
    this.plans = [];
    this.selectedProduct = null;
    this.subLineaItems = [];
    this.rangeItems = null;
    this.cuitSocio = this.userService.userData.cuit;

    this.catalogService.getSubLineCatalog ()
      .subscribe ( ( response: SubLineCatalogResponse ) => {
        /*this.subLineaItems = response.body.reduce((base, item) => {
          return base.concat(item.sublineas);
        }, []);*/
        if ( !response ) {
          this.subLineaItems = undefined;
        } else {
          this.subLineaItems = response.body;
        }
        this.range = State.empty;
        this.subLine = State.empty;
        if ( this.subLineaItems != undefined ) {
          this.categories.push ( this.addCategory ( '3', this.categoryDesigns[3] ) );
        }
      } );

    this.dashboardService.getProductList ().subscribe ( products => {
      //products[1].id_negocio = 2;
      this.products = products;
      const aux = {};

      this.products.map ( ( product: Producto ) => {
        aux[product.id_negocio] = true;
      } );
      Object.keys ( aux ).map ( key => {
        this.categories.push ( this.addCategory ( key, this.categoryDesigns[key] ) );
      } );


      const businessLine = this.newSaleService.category;

      if ( businessLine ) {
        const category = this.categories.find ( cat => cat.id === businessLine );
        const product = this.newSaleService.product;
        this.selectCategory ( category );

        if ( [ '1', '2' ].includes ( businessLine ) && product ) {
          this.selectProduct ( product );
        }
      }
    } );

  }

  selectCategory ( category: any ) {

    this.productsSegunNegocio = this.products.filter ( ( product: Producto ) => {
      return product.id_negocio === parseInt ( category.id );
    } );
    let tam = this.productsSegunNegocio.length;
    let entero = Math.trunc ( this.productsSegunNegocio.length / 3 );
    let decimal = this.productsSegunNegocio.length % 3;

    if ( decimal == 0 ) {
      this.listaPrdsUno = this.productsSegunNegocio.slice ( 0, entero );
      this.listaPrdsDos = this.productsSegunNegocio.slice ( entero, ( entero * 2 ) );
      this.listaPrdsTres = this.productsSegunNegocio.slice ( tam - entero, tam );
    } else if ( decimal < 0.5 ) {
      this.listaPrdsUno = this.productsSegunNegocio.slice ( 0, ( entero + 1 ) );
      this.listaPrdsDos = this.productsSegunNegocio.slice ( ( entero + 1 ), ( entero * 2 + 1 ) );
      this.listaPrdsTres = this.productsSegunNegocio.slice ( tam - entero, tam );
    } else if ( tam == 1 ) {
      this.listaPrdsUno = this.productsSegunNegocio.slice ( 0, 1 );
    } else if ( tam == 2 ) {
      this.listaPrdsUno = this.productsSegunNegocio.slice ( 0, 1 );
      this.listaPrdsDos = this.productsSegunNegocio.slice ( 1 );
    } else if ( tam == 3 ) {
      this.listaPrdsUno = this.productsSegunNegocio.slice ( 0, 1 );
      this.listaPrdsDos = this.productsSegunNegocio.slice ( 1, 2 );
      this.listaPrdsTres = this.productsSegunNegocio.slice ( 2 );
    } else {
      this.listaPrdsUno = this.productsSegunNegocio.slice ( 0, ( entero + 1 ) );
      this.listaPrdsDos = this.productsSegunNegocio.slice ( ( entero + 1 ), ( entero * 2 + 1 ) );
      this.listaPrdsTres = this.productsSegunNegocio.slice ( tam - ( entero ), tam );
    }


    if ( !category.state ) {
      this.listaUno = [];
      this.plans = [];
      if ( this.selectedCategory ) {
        this.selectedCategory.state = false;
      }
      this.selectedCategory = null;
      this.unselectProduct ();
      this.resetForm ();
      this.hasOneSelected = false;
    }

    category.state = category.state ? State.empty : State.active;
    this.hasOneSelected = !!category.state;

    if ( category.state ) {
      this.selectedCategory = category;

      if ( this.selectedCategory.id !== '3' ) {
        this.listaUno = this.listaPrdsUno.filter ( ( product: any ) => {
          return product.id_negocio == category.id;
        } );
        this.listaDos = this.listaPrdsDos.filter ( ( product: any ) => {
          return product.id_negocio == category.id;
        } );
        this.listaTres = this.listaPrdsTres.filter ( ( product: any ) => {
          return product.id_negocio == category.id;
        } );
      }
    }

  }

  selectProduct ( product: Producto ) {
    this.selectedProduct = product;
    this.plans = this.refactorPlans ( this.selectedProduct );
    this.newSaleService.productoSeleccionado = this.selectedProduct;
    this.newSaleService.saleRequest.venta.detalleSolicitudVenta.idProductoElegido = this.selectedProduct.id_producto;
    this.newSaleService.saleRequest.venta.detalleSolicitudVenta.descripcionProductoElegido = this.selectedProduct.descripcion_mkt;
    this.newSaleService.saleRequest.venta.detalleSolicitudVenta.destino_alta = this.selectedProduct.destino_alta;
  }

  unselectProduct () {
    this.selectedProduct = null;
  }

  onChange () {
    const { subLine } = this.form.value;

    this.range = State.empty;
    this.selectedWarrantyProduct = null;

    if ( !subLine ) {
      this.rangeItems = null;
      return false;
    }


  }

  onFormChange () {
    if ( this.form && this.form.valid && this.form.value.range ) {
      const { subLine, range: id } = this.form.value;
      //const range = this.rangeItems.filter(item => item.id === +id)[0];
      const range = this.rangeItems;

      this.dashboardService.getWarrantyProduct ( this.cuitSocio, range[0].codigoProducto )
        .subscribe ( ( response: WarrantyProduct ) => {
          this.selectedWarrantyProduct = response;
        } );
    }
  }

  selectWarrantyPlan ( price: WarrantyProductPrice ) {
    this.newSaleService.warrantyPlan = price;
    this.onNext ( true );
  }

  onNext ( isWarranty?: boolean ) {
    this.newSaleService.category = this.selectedCategory.id;

    if ( isWarranty ) {
      this.newSaleService.warranty = this.selectedWarrantyProduct;
      this.newSaleService.product = new Producto ();
      this.newSaleService.product.conjunto_producto.titular = 1;
    } else {
      this.newSaleService.product = this.selectedProduct;
    }

    this.next.emit ();
  }

  private resetForm () {
    this.errorDeRango = false;
    if ( this.form ) {
      this.form.reset ();
      this.subLine = State.empty;
      this.range = State.empty;
    }
  }

  private refactorPlans ( product: Producto ) {
    let plans: Array<any>, temp: any;

    if ( product.planes.length ) {
      plans = product.planes.sort ( ( a: Plan, b: Plan ) =>
        a.codigo_plan < b.codigo_plan ? -1 : 1 );
    } else {
      temp = product.planes;
      product.planes = [ temp ];
      plans = product.planes;
    }

    /* plans = plans.map(plan => {
      let aux: any;

      if (!plan.riesgos.riesgo.length) {
        aux = plan.riesgos.riesgo;
        plan.riesgos.riesgo = [aux];
      }

      return plan;
    }); */

    return plans;
  }

  private addCategory ( id, options ) {
    console.log("...option", ...options);
    console.log(JSON.stringify({ id, state: State.empty, ...options }));
    return { id, state: State.empty, ...options };
  }


  buscarRango () {
    this.isLoading = true;
    //this.MockBuscarRango ();
    this.catalogService.getRangeBySubLineCatalog ( this.cuitSocio, parseInt ( this.precio ), this.subLine )
      .pipe ( catchError ( error => this.manejarError ( error ) ) )
      .subscribe ( ( response: RespuestaRango ) => {
          this.rangeItems = response.body.rangos;
          let idPrd = this.rangeItems[0].codigoProducto;
          this.dashboardService.getWarrantyProduct ( this.cuitSocio, idPrd )
            .subscribe ( ( response: WarrantyResponse ) => {
              this.selectedWarrantyProduct = response.body[0];
              this.mostrarProducto = true;
              this.buttonValid = false;
            } );
        }
      );
  }

  MockBuscarRango () {

    if ( !environment.production ) {
      console.log ( "************************************************* environment.production = ", environment.production );
      console.log ( "MockBuscarRango: True" );
      console.log ( "************************************************************************** " );
    }

    let precio = new WarrantyProductPrice ();
    precio.Plazo = "2";
    precio.Importe = 123;
    this.selectedWarrantyProduct = new WarrantyProduct ();
    this.selectedWarrantyProduct.Id = 1;
    this.selectedWarrantyProduct.Descripcion = "descrip";
    this.selectedWarrantyProduct.Sublinea = "sublinea";
    this.selectedWarrantyProduct.PrecioDesde = "33333";
    this.selectedWarrantyProduct.PrecioHasta = "44444";
    this.selectedWarrantyProduct.tieneDano = false;
    this.selectedWarrantyProduct.TieneRobo = false;
    this.selectedWarrantyProduct.precio = "123";
    this.selectedWarrantyProduct.colPrecios = [ precio ];
    this.mostrarProducto = true;
    this.buttonValid = false;


    this.resetForm ();
    if ( this.mostrarProducto === true ) {
      this.buttonValid = false;
      this.errorDeRango = false;

    } else {
      this.buttonValid = false;

      setTimeout ( () => {
        this.errorDeRango = true;
        this.isLoading = false;
      }, 1000 );
    }
  }

  unselectProductWarranty () {
    this.mostrarProducto = false;
    this.errorDeRango = false;
    this.isLoading = false;
  }

  prdWarrantySeleccionado ( plazo: any ) {
    this.newSaleService.selectedWarrantyPlan = plazo.substring ( 0, 1 );
    this.resetForm ();
    this.unselectProductWarranty ();
    this.onNext ( true );
  }

  /**
   * solo valida que el monto tenga mas de 3 digitos
   * @param val
   */
  validar ( val: any ) {
    //si se borra monto button false
    if ( this.precio === null ) {
      this.buttonValid = false;
      return;
    }
    //continue
    this.errorDeRango = false;
    let montoArray = val.toString ().split ( "." );
    let montoString;
    let decimalString;
    let decimalInt;
    let montoInt;
    //si val pasado a array es mayor a 1 es porque tiene decimales.
    if ( montoArray.length > 1 ) {
      montoString = montoArray[0].toString ();
      decimalString = montoArray[1].toString ();
      if ( montoString.length > 3 ) {
        decimalInt = parseInt ( decimalString );
        montoInt = parseInt ( montoString );
        if ( decimalString.length > 2 ) {
          decimalInt = parseInt ( decimalString.substring ( 0, 2 ) );
        }
        this.precio = montoInt + "." + decimalInt;
        this.buttonValid = true;
      } else {
        this.buttonValid = false;
      }
    } else {
      if ( val.toString ().length > 3 ) {
        this.precio = val;
        this.buttonValid = true;
      } else {
        this.precio = val;
        this.buttonValid = false;
      }

    }
  }

  private manejarError ( error: HttpErrorResponse ) {
    this.isLoading = false;
    this.msjError = "Error al intentar obtener rango, No hay valores para esta categoria";
    this.errorDeRango = true;
    return throwError ( "Error al intentar obtener rango" );
  }

  //seccion de animacion
  estado = "inactive";
  animar () {
    this.estado = this.estado === 'active'? 'inactive' : 'active';
  }
  ngAfterViewInit(){
    //despues de iniciar por completo el componente se produce el cambio.
    this.animar();
    this.cdRef.detectChanges();
  }
}
