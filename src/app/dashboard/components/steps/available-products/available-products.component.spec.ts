import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvailableProductsComponent } from './available-products.component';

describe('AvailableProductsComponent', () => {
  let component: AvailableProductsComponent;
  let fixture: ComponentFixture<AvailableProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailableProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailableProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    this.cuitSocio = '30681424721';
    this.precio = 33000;
    this.subLine = 'LBHEL';
    //this.rangeItems.CodigoProducto = 4832;
  });

  it('Comprobar respuesta de rangos', () => {
    expect(component.buscarRango()).toBeTruthy();
  });
});
