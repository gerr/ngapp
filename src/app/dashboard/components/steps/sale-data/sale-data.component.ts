import { Component, OnInit, Output, EventEmitter, ComponentFactoryResolver, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';

import { NewSaleService } from '../../../services/new-sale.service';
import { State, TemplateKeys } from '../../../dashboard.enums';
import { Producto } from '../../../models/products/producto.model';
import { ProductTemplate } from '../../../models/product-template.model';
import { ProductTemplateDirective } from '../../../directives/product.directive';
import { ProductComponent } from '../product.component';
import { getProductTemplates } from '../../products/init';
import { WarrantyProduct } from '../../../models/services/services.model';
import { animate, state, style, transition, trigger } from "@angular/animations";

@Component({
  selector: 'app-sale-data',
  template: `
    <div class="sale-data" [@animacion] = "estado">
      <ng-template appProductTemplateHost></ng-template>
    </div>
  `,
    animations: [
        trigger('animacion', [
          state('inactive', style({
            opacity: '0'
          })),
          state('active', style({
            opacity: '1'
          })),
          transition('inactive => active', animate('600ms ease-in')),
          transition('active => inactive', animate('600ms ease-out')),
        ])
      ]
})
export class SaleDataComponent implements OnInit, AfterViewInit {
  selectedCategory: string;
  selectedProduct: Producto;
  selectedWarranty: WarrantyProduct;
  saleDataTemplate: string;
  templatesMatches: any;
  productTemplates: ProductTemplate[];

  @ViewChild(ProductTemplateDirective) productHost: ProductTemplateDirective;

  @Output() next: EventEmitter<any> = new EventEmitter<any>();

  constructor(private newSaleService: NewSaleService,
              private componentFactoryResolver: ComponentFactoryResolver,
              private cdRef:ChangeDetectorRef) {}

  ngOnInit() {
     this.selectedCategory = this.newSaleService.category;
    this.productTemplates = getProductTemplates();
    this.templatesMatches = {
      'bolso': TemplateKeys.health, //bolso - no funca
      'resp': TemplateKeys.health, //resp - no funca
      'salud': TemplateKeys.health, // salud - ¿q p es el titular del seguro? SI/NO .... no funca dni
      'contenido': TemplateKeys.health, //ap - mismo problema que salud
      'vida': TemplateKeys.health, //vida - mismo problema que salud
      'hogar': TemplateKeys.health, // hogar - no funca localidad
      'asist': TemplateKeys.health, //asist - no funca localidad
      'mayor': TemplateKeys.health, //asist - no funca localidad
      'tecno': TemplateKeys.health,//tecno - no funcan marca, linea, producto
      'celular': TemplateKeys.health,//tecno - no funcan marca, linea, producto
      'compra': TemplateKeys.health,//compra - no funca fecha
      'portables': TemplateKeys.health,//compra - no funca fecha
      'warranty': TemplateKeys.health
    };
   
    if (this.selectedCategory !== '3') {
      this.selectedProduct = this.newSaleService.product;
      this.generateSaleDetailProduct(this.selectedProduct);
      this.saleDataTemplate = this.getTemplateByProductTag(this.selectedProduct);
    } else {
      this.selectedWarranty = this.newSaleService.warranty;
      this.generateSaleDetailWarranty(this.selectedWarranty);
      this.saleDataTemplate = `${this.templatesMatches.warranty}Component`;
      this.continue();
    }

    if(this.newSaleService.servicioSinParametros)
      {
        this.newSaleService.servicioSinParametros=false;
        this.next.emit({index:-1});
        return;
      }

    if (!this.saleDataTemplate || this.saleDataTemplate==TemplateKeys.empty) {
      this.newSaleService.servicioSinParametros=true;
      this.continue();
    } else {
     
      const component = this.productTemplates
        .find(templates => templates.name === this.saleDataTemplate);

      if (component && !this.selectedWarranty) {
        this.newSaleService.servicioSinParametros=false;
        this.loadProductTemplate(component, this.selectedProduct || this.selectedWarranty);
      } else {
        this.continue();
      }
    }
  }

  continue() {
    this.next.emit({ index: 4, beforeStepState: State.completed });
  }

  private loadProductTemplate(productTemplate: ProductTemplate, product: any) {
    const componentFactory = this.componentFactoryResolver
      .resolveComponentFactory(productTemplate.component);
    const viewContainerRef = this.productHost.viewContainerRef;
    viewContainerRef.clear();
    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<ProductComponent>componentRef.instance).product = product;
    (<ProductComponent>componentRef.instance).continue.subscribe( () => {
      this.continue();
    });
  }

  private getTemplateByProductTag(product: Producto) {
    let template: string = TemplateKeys.empty;

    if (!product) {
      return template;
    }

    const templateFound = Object.keys(this.templatesMatches)
      .find(key => product.descripcion_mkt.toLowerCase().includes(key));

    if (templateFound) {
      const templateName: string = this.templatesMatches[templateFound];
      template = `${templateName}Component`;
    }

    return template = 'HealthTemplateComponent';
  }

  private generateSaleDetailProduct({ id_producto, descripcion_mkt}: Producto) {
    const current = this.newSaleService.saleDetail;

    this.newSaleService.saleDetail = Object.assign({
      idProductoElegido: id_producto,
      descripcionProductoElegido: descripcion_mkt
    }, current);
  }

  generateSaleDetailWarranty(warranty: WarrantyProduct) {
    const current = this.newSaleService.saleDetail;
    const {
      Id, Descripcion, tieneDano,
      TieneRobo, PrecioDesde, PrecioHasta,precio, colPrecios
    } = warranty;

    this.newSaleService.saleDetail = Object.assign({
      idProductoElegido: Id,
      tieneDano: !!tieneDano,
      tieneRobo: !!TieneRobo,
      descripcionProductoElegido: Descripcion,
      precioDesde: PrecioDesde,
      precioHasta: PrecioHasta,
      precio: precio,
      plazo: this.newSaleService.selectedWarrantyPlan
    }, current);
  }
  
  
  //seccion de animacion
  estado = "inactive";
  animar () {
    this.estado = this.estado === 'active'? 'inactive' : 'active';
  }
  ngAfterViewInit(){
    //despues de iniciar por completo el componente se produce el cambio.
    this.animar();
    this.cdRef.detectChanges();
  }
}
