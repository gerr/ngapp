import {Component, OnInit, OnDestroy, Output, EventEmitter} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';

import {State} from '../../../dashboard.enums';
import {CatalogService} from '../../../services/catalog.service';
import {DashboardService} from '../../../services/dashboard.service';
import {CatalogueService} from '../../../../shared/services/catalogue.service';
import {OAuthService} from '../../../../shared/services/oauth.service';
import {NewSaleService} from '../../../services/new-sale.service';
import {Client} from '../../../models/sales/sales.model';
import {GetClientResponse} from '../../../models/services/services-response.model';

@Component({
  selector: 'app-search-client',
  templateUrl: './search-client.component.html',
  styleUrls: ['./search-client.component.scss']
})
export class SearchClientComponent implements OnInit, OnDestroy {
  searchForm: FormGroup;
  private readonly SUCCESS: number = 200;
  isLoading: boolean;
  dniMinLenght: number;
  dniMaxLenght: number;
  genderList: Array<any>;
  // id: mock dni
  //listadoItem: DocumentType[];
  listadoItem: any[];
  // fin mock
  genderSubs: Subscription;
  dniTypeSubs: Subscription;
  clientDataSubs: Subscription;
  selectedDoc: number;

  @Output() next: EventEmitter<any> = new EventEmitter<any>();
  @Output() loading: EventEmitter<boolean> = new EventEmitter<boolean>();
  clientesRepetidos: Client[];
  hayClientesRepetidos = false;
  clienteObj: Object = {
    id: undefined,
    apellido: undefined,
    nombre: undefined,
    genero: undefined,
    fecha: undefined,
    estado: false
  };
  listaClientesObj: any[] = [];
  haySeleccionado = false;
  idClienteSeleccionado: number;

  constructor(private fb: FormBuilder,
              private catalogService: CatalogService,
              private dashboardService: DashboardService,
              private catalogueService: CatalogueService,
              private newSaleService: NewSaleService,
              private userService: OAuthService) {
  }

  ngOnInit() {
    if (this.userService.isLoginAuto) {
      // id: multi-clientes
      this.dashboardService.clientData.push(this.userService.userAuto.client);
      this.newSaleService.client = this.userService.userAuto.client;
      this.next.emit({index: 1, beforeStepState: State.completed});
    } else {
      this.init();
    }
  }

  createForm() {
    this.searchForm = this.fb.group(
      {
        selectedType: [State.empty, Validators.required],
        idGenero: [State.empty, Validators.required],
        document: [State.empty, [
          Validators.required,
          Validators.min(100000),
          Validators.max(99999999)
        ]]
      });
    //this.selectedDoc=223550000;
  }

  get selectedType() {
    return this.searchForm.get('selectedType');
  }

  get idGenero() {
    return this.searchForm.get('idGenero');
  }

  get document() {
    return this.searchForm.get('document');
  }

  init() {
    this.listaClientesObj = [];
    this.haySeleccionado = false;
    this.dniMinLenght = 7;
    //Cambiar esto si se agregan CUIT, como lso docuemntos se obtienen por fuera
    //los datos de longitud min y max tambien deberian venir en ese servicio
    this.dniMaxLenght = 9;
    this.genderList = [];
    this.isLoading = false;
    this.createForm();
    this.listadoItem = new Array<any>();

    this.dniTypeSubs = this.catalogueService.getDocumentType()
      .subscribe((response: any) => {
        //gerr: quitar
        //if (response.code.code === this.SUCCESS) {
        this.listadoItem = response;
        //}
      });

    // id: mock sexos
    let sexos = JSON.parse('{"code":{"code":200,"description":"OK"},"body":[{"id":"1","descripcion":"Masculino"},{"id":"2","descripcion":"Femenino"}]}');
    this.genderList = sexos.body;
    /*    this.genderSubs = this.catalogService.getGenderCatalog ()
          .subscribe ( ( { body }: CatalogResponse ) => {
            this.genderList = body;
          } );*/
    // fin mock
    this.onChangeSelect();
  }

  ngOnDestroy() {
    Object.keys(this).filter(k => k.includes('Subs')).map(key => {
      if (this[key]) {
        this[key].unsubscribe();
      }
    });
  }

  onSearchClient() {
    const {selectedType, idGenero, document} = this.searchForm.value;
    this.isLoading = true;
    this.loading.emit(true);
    this.clientDataSubs = this.dashboardService
      .searchClient(selectedType, document, idGenero).subscribe((res: GetClientResponse) => {
        if (res.cliente) {
          this.newSaleService.client = res.cliente;
          this.loading.emit(false);
          this.next.emit({index: 1, beforeStepState: State.completed});
        } else {
          this.tratarClientesRepetidos(res.clientes);
        }

      });
  }

  tratarClientesRepetidos(clientes: Client[]) {
    if (clientes.length === 1) {
      this.newSaleService.client = clientes[0];
      this.loading.emit(false);
      this.next.emit({index: 1, beforeStepState: State.completed});
    } else {
      this.clientesRepetidos = clientes;
      clientes.forEach(cli => {
        this.clienteObj = {};
        this.clienteObj['id'] = cli.id;
        this.clienteObj['apellido'] = cli.apellido;
        this.clienteObj['nombre'] = cli.nombre;
        this.clienteObj['genero'] = cli.idGenero === '1' ? 'Masculino' : cli.idGenero === '2' ? 'Femenino' : 'Otro';
        this.clienteObj['fecha'] = cli.fechaNacimiento;
        this.clienteObj['estado'] = false;
        this.listaClientesObj.push(this.clienteObj);
      });
      this.hayClientesRepetidos = true;
    }
  }

  onChangeSelect() {

  }

  cerrar() {
    this.hayClientesRepetidos = !this.hayClientesRepetidos;
    this.init();
  }

  clienteSeleccionado(cliente: any, i: any) {
    this.listaClientesObj.forEach(c => {
      if (c.id === cliente.id) {
        c.estado = !c.estado;
        if (c.estado) {
          this.idClienteSeleccionado = c.id;
          this.haySeleccionado = true;
        } else {
          this.haySeleccionado = false;
          this.idClienteSeleccionado = undefined;
        }

      } else {
        c.estado = false;
      }
    });
    // this.listaClientesObj[i]['estado'] = true;
    /*    this.newSaleService.client = cliente;
        this.dashboardService.clientData = cliente;
        this.loading.emit(false);
        this.next.emit({index: 1, beforeStepState: State.completed});*/
  }

  continuar(nuevo: boolean) {
    if (nuevo) {
      this.newSaleService.client = new Client();
      const {selectedType, idGenero, document} = this.searchForm.value;
      this.newSaleService.client.tipoDocumento = selectedType;
      this.newSaleService.client.idGenero = idGenero;
      this.newSaleService.client.nroDocumento = document;
    } else {
      this.newSaleService.client = this.clientesRepetidos.find(cli => cli.id === this.idClienteSeleccionado);
    }
    this.dashboardService.clientData.push(this.newSaleService.client);
    this.loading.emit(false);
    this.next.emit({index: 1, beforeStepState: State.completed});
  }
}
