import { SearchClientComponent } from './search-client/search-client.component';
import { PersonalDataComponent } from './personal-data/personal-data.component';
import { AvailableProductsComponent } from './available-products/available-products.component';
import { SaleDataComponent } from './sale-data/sale-data.component';
import { PayAndConsentComponent } from './pay-and-consent/pay-and-consent.component';

export const STEPS = [
  SearchClientComponent,
  PersonalDataComponent,
  AvailableProductsComponent,
  SaleDataComponent,
  PayAndConsentComponent
];
