// todo id: encuesta
// todo id: integracion vt
import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Input,
  Directive,
  AfterViewInit,
  ChangeDetectorRef
} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';

import {cardIssuer, State} from '../../../dashboard.enums';
import {DashboardService} from '../../../services/dashboard.service';
import {ConfirmationMethod} from '../../../models/confirmation-method.model';
import {OAuthService} from '../../../../shared/services/oauth.service';
import {NewSaleService} from '../../../services/new-sale.service';
import {NotificationService} from '../../../../shared/services/notification.service';
import {ConfirmationCode} from '../../../models/confirmation-code.model';

import {MedioPago, MEDIOS_PAGO} from '../../../models/products/producto.model';
import {RespuestaVenta} from '../../../models/ventas-realizadas/respuesta-venta.model';
import {WarrantyProductPrice} from '../../../models/services/services.model';
import {RespuestaGenerica} from '../../../models/respuesta-generica';
import {PayData, UserModel} from '../../../../shared/models/user.model';
import {environment} from '../../../../../environments/environment';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {SolicitudAdhesion} from '../../../../shared/models/SolicitudAdhesion.model';
import {now} from 'moment';
import {CatalogService} from '../../../services/catalog.service';
import {Encuesta, ARQUETIPOS, COMUNICACION} from '../../../models/utils/utils.model';
import {EncuestaSale} from '../../../models/sales/sales.model';

@Component({
  selector: 'app-pay-and-consent',
  templateUrl: './pay-and-consent.component.html',
  styleUrls: ['./pay-and-consent.component.scss'],
  animations: [
    trigger('animacion', [
      state('inactive', style({
        opacity: '0'
      })),
      state('active', style({
        opacity: '1'
      })),
      transition('inactive => active', animate('700ms ease-in')),
      transition('active => inactive', animate('700ms ease-out'))
    ])
  ]
})


export class PayAndConsentComponent implements OnInit, AfterViewInit {

  @ViewChild('codigoValidacion') codigoValidacion: ElementRef;
  @ViewChild('cardNum2') cardnum2: ElementRef;
  @ViewChild('cardNum3') cardnum3: ElementRef;
  @ViewChild('cardNum4') cardnum4: ElementRef;
  @ViewChild('realFileInput') fileInput: ElementRef;

  public formSms: FormGroup;
  public isLoading = false;
  public isLoadingSms = false;

  public typeCard = 'CARD';
  public typeValC = 'VALC';
  public typeVenc = 'VENC';
  public typeText = 'L';

  public clientName: string;
  public phones: Array<string>;
  //para el enviar SMS solo a los celulares
  public celulares: Array<string> = [];
  public listaMediosDePago: Array<MedioPago>;
  public idMedioPago: number;

  public payAndConsentForm: FormGroup;
  public uploadForm: FormGroup;
  public isValidCardIssuer = false;

  public isLuhnCheck = false;
  public isLuhnMsjOn = false;

  public filename = '';

  //parametros de mensajes:
  public descripcionPrd: string;
  public planPrd: any;
  public precioPrd: string;
  public nombreClienteAsegurado: string;
  public apellidoClienteAsegurado: string;
  public dniCliente: string;
  public tipoDoc: string;

  public validationMethod = true; // true -> sms / false -> dni pic
  public validationMessage = '';
  public validationCode: number;
  public confirmationCode: RespuestaGenerica;
  public isConsentGiven = false;
  public isConfirmationCodeError = false;
  public confirmationCodeErrorMsg = '';
  public imageUploadErrorMsg = '';
  public isNotPUA: boolean;
  public grayMaster: boolean = false;
  public grayVisa: boolean = false;
  public grayAmex: boolean = false;
  public mostrarMediosDePago: boolean = true;
  public mostrarBoton: boolean = false;
  public medioDePagoSeleccionado = false;
  public sinMedioDePago = false;
  public isProducto: boolean = false;
  public isWarranty: boolean = false;
  public tieneServicioSPS: boolean = false;


  public planElegido: any;
  public precioPlan: any;
  public idMedioPagoSelected = '';
  public cardType: string;
  public isCodeGenerated: boolean = false;
  public conMensajeria: boolean = false;
  public celular: any;
  public codigoErroneo = false;
  public mensajeEnviado = false;
  public errorEnMsjEnviado: boolean = false;
  public tieneSolicitud: boolean;
  //payData: PayData;

  //public planElegido: string;
  public solicitud: SolicitudAdhesion = new SolicitudAdhesion();
  public telefonosClienteAsegurado: string;

  //id: ENCUESTA
  public arquetipos: Encuesta[];
  public comunicacion: Encuesta[];
  public cuit: string = '';
  public msjSolicitud: boolean = false;
  public idArquetipoElegido: number;
  public idContactoElegido: number;
  public tieneEncuesta: boolean = false;
  public idGenero: string = '';
  public msjContactoNoSeleccionado: string = '';
  public mostrarMsjContacto: boolean = false;
  public descripcionPlan: string;

  constructor(private formBuilder: FormBuilder, private dashboardService: DashboardService,
              private oAuthService: OAuthService, public newSaleService: NewSaleService,
              private notificationService: NotificationService, private catalogService: CatalogService,
              private cdRef: ChangeDetectorRef) {

    this.formSms = this.formBuilder.group(
      {
        numCelular: ['', Validators.required],
        numValidacion: ['', Validators.required]
      }
    );
  }

  ngOnInit() {
    this.tieneServicioSPS = this.newSaleService.user.tiene_sps;
    //id: ENCUESTA
    this.arquetipos = ARQUETIPOS;
    this.comunicacion = COMUNICACION;

    //Se obtienen datos del usuario para ver si tiene la opcion de mensajeria.
    const VENTA = this.newSaleService.saleRequest.venta;

    this.oAuthService.getUserData().subscribe((user: UserModel) => {
      const {mensajeria, cuit} = user;
      this.cuit = cuit;

      if (mensajeria) {
        //if(true){
        this.conMensajeria = true;
        this.isConsentGiven = false;
      } else {
        this.conMensajeria = false;
        //si no tiene mensajeria muestra datos de tarjeta para finalizar la venta.
        this.isConsentGiven = true;
      }
    });
    //id: ENCUESTA : si es cuit carre y sucu 001 o sucu 021 se muestra ENCUESTA.
    if (this.cuit == '30687310434' && (this.newSaleService.idBranch == '1' || this.newSaleService.idBranch == '21')) {
      //this.tieneEncuesta = true;
      if (!environment.production) {
        console.log('************************************************* environment.production = ', environment.production);
        console.log('HARDCODEADO ENCUESTA');
        console.log('************************************************************************** ');
      }
      this.tieneEncuesta = true;
    }

    /*    if ( !environment.production ) {
          console.log ( '************************************************* environment.production = ', environment.production );
          console.log ( 'tieneSolicitud', this.newSaleService.user.tieneSolicitud );
          this.tieneSolicitud = this.newSaleService.user.tieneSolicitud;

          console.log ( '************************************************************************** ' );
        }*/
    this.tieneSolicitud = this.newSaleService.user.tieneSolicitud;
    /*    if(this.oAuthService.isLoginAuto)
          this.payData = this.dashboardService.payData;
        */
    //celular indefinido al inicio para activar boton de enviar.

    //id: ENCUESTA : Carga de solicitud.
    this.idGenero = VENTA.cliente.idGenero;
    this.planElegido = this.newSaleService.selectedWarrantyPlan;
    this.solicitud.nombreApellido = `${VENTA.cliente.nombre} ${VENTA.cliente.apellido}`;
    this.solicitud.fechaNacimiento = VENTA.cliente.fechaNacimiento;
    this.solicitud.tipoDocNum = `${VENTA.cliente.tipoDocumento == '5' ? 'DNI' : 'CUIL'} ${VENTA.cliente.nroDocumento}`;
    this.solicitud.telefono = ` (Fijo) ${VENTA.cliente.telefonoFijo1 ? VENTA.cliente.telefonoFijo1 : 'no tiene'} / (Cel) ${VENTA.cliente.celular1}`;
    this.solicitud.domicilio = `${VENTA.cliente.calle} ${VENTA.cliente.numero}`;
    VENTA.cliente.provincia ?  this.solicitud.provincia = VENTA.cliente.provincia.descripcion : this.solicitud.provincia = '';
    this.solicitud.localidad = VENTA.cliente.localidad ? VENTA.cliente.localidad.localidad : '-';
    this.solicitud.codPostal = VENTA.cliente.codigoPostal ? VENTA.cliente.codigoPostal : '-';
    this.solicitud.email = VENTA.cliente.email ? VENTA.cliente.email[0] : VENTA.cliente.email1;
    this.solicitud.fechaSolicitud = '-';
    this.solicitud.productoElegido = VENTA.detalleSolicitudVenta.idProductoElegido;
    this.solicitud.cuit = this.cuit;

    if (!this.newSaleService.warranty) {
      this.descripcionPrd = VENTA.detalleSolicitudVenta.descripcionProductoElegido;
      this.descripcionPlan = `  "${VENTA.detalleSolicitudVenta.idPlanElegido}" - $${this.newSaleService.precioPrdElegido}/mes`;
      this.isProducto = true;
      this.solicitud.plan = `${this.newSaleService.precioPrdElegido}/mes, ${this.descripcionPrd} ,
      Plan - ${VENTA.detalleSolicitudVenta.idPlanElegido}`;
    } else {
      const pos = parseInt(this.newSaleService.selectedWarrantyPlan) - 2;
      this.descripcionPrd = this.newSaleService.warranty.Descripcion;
      this.descripcionPlan = ` "${this.newSaleService.selectedWarrantyPlan}" - $${this.newSaleService.warranty.colPrecios[pos].Importe}/mes`;
      this.isWarranty = true;
      this.solicitud.plan = `$${this.newSaleService.warranty.colPrecios[pos].Importe}/mes
      , ${this.descripcionPrd}, Plan - ${this.newSaleService.selectedWarrantyPlan}`;
    }

    //this.tipoDoc = this.newSaleService.saleRequest.venta.cliente.tipoDocumento;
    this.setTipoDocuemnto();
    //medio de pago
    this.newSaleService.product.medios_pago ? this.listaMediosDePago = this.newSaleService.product.medios_pago : this.listaMediosDePago = MEDIOS_PAGO;
    this.isLoading = false;
    this.isNotPUA = this.newSaleService.category !== '3';
    const {nombre, apellido, celular1, celular2, celular3, telefonoFijo1, telefonoFijo2, telefonoFijo3} = this.newSaleService.client;
    this.clientName = `${nombre} ${apellido}`;
    this.phones = [];

    /**
     * Quitar caracteres no numericos de los numeros telefonicos.
     */
    if (celular1) {
      const limpio = this.limpiarCelular(celular1);
      this.newSaleService.client.celular1 = limpio;
      this.phones.push(limpio);
      this.celulares.push(limpio);
    }

    if (celular2) {
      const limpio = this.limpiarCelular(celular2);
      this.newSaleService.client.celular2 = limpio;
      this.phones.push(limpio);
      this.celulares.push(limpio);
    }

    if (celular3) {
      const limpio = this.limpiarCelular(celular3);
      this.newSaleService.client.celular3 = limpio;
      this.phones.push(limpio);
      this.celulares.push(limpio);
    }

    if (telefonoFijo1) {
      const limpio = this.limpiarCelular(telefonoFijo1);
      this.newSaleService.client.telefonoFijo1 = limpio;
      this.phones.push(telefonoFijo1);
    }

    if (telefonoFijo2) {
      const limpio = this.limpiarCelular(telefonoFijo2);
      this.newSaleService.client.telefonoFijo2 = limpio;
      this.phones.push(telefonoFijo2);
    }

    if (telefonoFijo3) {
      const limpio = this.limpiarCelular(telefonoFijo3);
      this.newSaleService.client.telefonoFijo3 = limpio;
      this.phones.push(telefonoFijo3);
    }

    this.payAndConsentForm = this.formBuilder.group({
      medioPago: [Validators.required],
      cardNum1: [State.empty, [Validators.required, Validators.minLength(4)]],
      cardNum2: [State.empty, [Validators.required, Validators.minLength(4)]],
      cardNum3: [State.empty, [Validators.required, Validators.minLength(4)]],
      cardNum4: [State.empty, [Validators.required, Validators.minLength(3)]]

    });

    if (this.oAuthService.isLoginAuto && this.oAuthService.userAuto.pay_data) {
      this.payAndConsentForm.get('cardNum1').patchValue(this.oAuthService.userAuto.pay_data.card_number.substring(0, 4));
      this.payAndConsentForm.get('cardNum2').patchValue(this.oAuthService.userAuto.pay_data.card_number.substring(4, 8));
      this.payAndConsentForm.get('cardNum3').patchValue(this.oAuthService.userAuto.pay_data.card_number.substring(8, 12));
      this.payAndConsentForm.get('cardNum4').patchValue(this.oAuthService.userAuto.pay_data.card_number.substring(12, 16));
      this.checkLuhn();
    }

    /*    if (!this.sinMedioDePago) {
          this.isLuhnCheck = false;
          this.isValidCardIssuer = false;
          this.payAndConsentForm.get('cardNum1').disable();
          this.payAndConsentForm.get('cardNum1').clearValidators();
          this.payAndConsentForm.get('cardNum2').disable();
          this.payAndConsentForm.get('cardNum2').clearValidators();
          this.payAndConsentForm.get('cardNum3').disable();
          this.payAndConsentForm.get('cardNum3').clearValidators();
          this.payAndConsentForm.get('cardNum4').disable();
          this.payAndConsentForm.get('cardNum4').clearValidators();
        }*/

    this.uploadForm = new FormGroup({
      file: new FormControl(null, [Validators.required])
    });

    if (this.listaMediosDePago.length) {
      this.mostrarMediosDePago = true;
    }
  }

  limpiarCelular(celular: string) {
    const celularLimpio = celular.replace(/[^\d.]/g, '');
    const celularLimpio2 = celularLimpio;
    const tam = celularLimpio.length;
    const num = celularLimpio.substr(tam - 8, tam);
    let area = celularLimpio.substr(0, tam - 8);
    const areaToArray = area.split('');
    if (areaToArray[0] === '0') {
      area = area.substr(1, area.length);
    }
    return area + num;
  }

  onKeyUpInput(inputNumber: number) {
    const num: string = this.payAndConsentForm.get('cardNum' + inputNumber).value;
    if (num.length === 4) {
      switch (inputNumber) {
        case 1: {
          this.cardnum2.nativeElement.focus();
          break;
        }
        case 2: {
          this.cardnum3.nativeElement.focus();
          break;
        }
        case 3: {
          this.cardnum4.nativeElement.focus();
          break;
        }
        default: {
          break;
        }
      }
    }

    if (inputNumber === 1) {
      if (num.length < 4) {
        this.isLuhnCheck = false;
        this.mostrarBoton = false;
        this.isValidCardIssuer = false;
      }
    }

    if (inputNumber === 2) {
      if (num.length < 4) {
        this.isLuhnCheck = false;
        this.mostrarBoton = false;
        this.isValidCardIssuer = false;
      }
    }

    if (inputNumber === 3) {
      if (num.length < 4) {
        this.isLuhnCheck = false;
        this.mostrarBoton = false;
        this.isValidCardIssuer = false;
      }
    }

    if (inputNumber === 4) {

      if (num.length >= 1) {
        this.checkLuhn();
      } else {
        if (num.length < 3) {
          this.isLuhnCheck = false;
          this.mostrarBoton = false;
          this.isValidCardIssuer = false;

        }
      }
    }
  }

  checkLuhn() {
    let ccNumber = this.payAndConsentForm.get('cardNum1').value;
    ccNumber += this.payAndConsentForm.get('cardNum2').value;
    ccNumber += this.payAndConsentForm.get('cardNum3').value;
    ccNumber += this.payAndConsentForm.get('cardNum4').value;
    if (ccNumber.startsWith('0000') || ccNumber.startsWith('1111') || ccNumber.startsWith('2222') || ccNumber.startsWith('3333') || ccNumber.startsWith('4444') || ccNumber.startsWith('5555') || ccNumber.startsWith('6666') || ccNumber.startsWith('7777') || ccNumber.startsWith('8888') || ccNumber.startsWith('9999')) {
      this.isLuhnCheck = false;
      this.mostrarBoton = false;
      this.isValidCardIssuer = false;
      this.isLuhnMsjOn = true;
      return;
    }
    if (this.idMedioPagoSelected === '75' && ccNumber.startsWith('858')) {
      ccNumber = 507 + ccNumber;
    }
    if (ccNumber.length >= 12) {
      const max = ccNumber.length - 1;
      let sum = 0;
      let alternate = false;
      let number;
      for (let i = max; i >= 0; i--) {
        number = ccNumber.substring(i, i + 1);
        if (alternate) {
          number *= 2;
          if (+number > 9) {
            number -= 9;
          }
        }
        sum += +number;
        alternate = !alternate;
      }
      this.isLuhnCheck = (sum % 10 === 0);
      this.isLuhnMsjOn = !this.isLuhnCheck;
      this.mostrarBoton = true;
      if (this.isLuhnMsjOn) {
        this.isValidCardIssuer = false;
        this.isLuhnCheck = false;
        this.mostrarBoton = false;
      } else {
        this.isValidCardIssuer = true;
      }

    } else {
      this.isLuhnCheck = false;
      this.mostrarBoton = false;
      this.isValidCardIssuer = false;

    }
  }


  changeCheck($event) {
    if ($event.target.checked) {
      this.sinMedioDePago = true;
      this.mostrarBoton = true;
      this.mostrarMediosDePago = false;
      this.isLuhnCheck = false;
      this.idMedioPagoSelected = '';
      this.payAndConsentForm.get('cardNum1').disable();
      this.payAndConsentForm.get('cardNum1').clearValidators();
      this.payAndConsentForm.get('cardNum2').disable();
      this.payAndConsentForm.get('cardNum2').clearValidators();
      this.payAndConsentForm.get('cardNum3').disable();
      this.payAndConsentForm.get('cardNum3').clearValidators();
      this.payAndConsentForm.get('cardNum4').disable();
      this.payAndConsentForm.get('cardNum4').clearValidators();
    } else {
      this.mostrarBoton = false;
      this.mostrarMediosDePago = true;
      this.payAndConsentForm.reset();
      this.isValidCardIssuer = false;
    }
  }

  seleccionarMedio(value) {
    if (parseInt(value) > 0) {
      this.sinMedioDePago = true;
      this.payAndConsentForm.get('cardNum1').enable();
      this.payAndConsentForm.get('cardNum2').enable();
      this.payAndConsentForm.get('cardNum3').enable();
      this.payAndConsentForm.get('cardNum4').enable();
    } else {
      this.payAndConsentForm.reset();
      this.mostrarBoton = false;
      this.isValidCardIssuer = false;

      this.payAndConsentForm.get('cardNum1').disable();
      this.payAndConsentForm.get('cardNum2').disable();
      this.payAndConsentForm.get('cardNum3').disable();
      this.payAndConsentForm.get('cardNum4').disable();
    }
    this.idMedioPagoSelected = value;
  }

  setTipoDocuemnto() {
    const tipo = this.newSaleService.client.tipoDocumento;

    if (tipo === '5') {
      this.tipoDoc = 'DNI';
    }
    if (tipo === '2') {
      this.tipoDoc = 'LC';
    }
    if (tipo === '3') {
      this.tipoDoc = 'LE';
    }
    if (tipo === '1') {
      this.tipoDoc = 'PASAPORTE';
    }
  }

  /**
   * confirmacion de la venta completa
   */
  onSubmit() {
    //id: ENCUESTA
    if (this.tieneEncuesta) {
      if (this.idContactoElegido < 1 || this.idContactoElegido > 6 || this.idContactoElegido == undefined) {
        this.mostrarMsjContacto = true;
        this.msjContactoNoSeleccionado = 'Falta seleccionar un medio de contacto';
        return;
      } else {
        this.mostrarMsjContacto = false;
        this.msjContactoNoSeleccionado = '';
      }
    }
    //si tiene encuesta y no selecciono
    this.dashboardService.hiddenActivado = true;
    this.isLoading = true;

    const {
      cardNum1, cardNum2, cardNum3, cardNum4, medioPago
    } = this.payAndConsentForm.value;

    this.newSaleService.saleRequest.venta.idVendedor = '' + this.newSaleService.user.userid;
    this.newSaleService.saleRequest.venta.dni = this.newSaleService.user.dni;
    if (this.newSaleService.saleRequest.venta.clientePagador === null) {
      this.newSaleService.saleRequest.venta.clientePagador = this.newSaleService.saleRequest.venta.cliente;
    }

    if (this.mostrarMediosDePago) {
      const numeroTarjeta = `${cardNum1}${cardNum2}${cardNum3}${cardNum4}`;
      this.newSaleService.updateSaleRequest('numeroTarjeta', numeroTarjeta);
      this.newSaleService.saleRequest.venta.medioDePago = medioPago;
    } else {
      this.newSaleService.saleRequest.venta.numeroTarjeta = '';
      this.newSaleService.saleRequest.venta.medioDePago = '';
    }

    //id: ENCUESTA
    this.newSaleService.saleRequest.venta.encuesta.idArquetipo = this.idArquetipoElegido;
    this.newSaleService.saleRequest.venta.encuesta.idContacto = this.idContactoElegido;
    // reset de icono seleccionado
    this.arquetipos.forEach((item) => {
      if (item.id === this.idArquetipoElegido) {
        item.estado = false;
      }
    });
    this.comunicacion.forEach((item) => {
      if (item.id === this.idContactoElegido) {
        item.estado = false;
      }
    });
    //FIN ENCUESTA

    const newSaleRequest = this.newSaleService.saleRequest;

    this.dashboardService.setConfirmSale(newSaleRequest, true)
      .subscribe((response: RespuestaVenta) => {
          this.newSaleService.resetSale();
          response.producto = this.solicitud.plan;
          // response.plan = this.descripcionPlan;
          this.notificationService.showSaleSuccess(response);
          this.dashboardService.respuestaVenta = response;
          if (!environment.production) {
            console.log('************************************************* environment.production = ', environment.production);
            console.log('Respuesta de la venta realizada: ', response);
            console.log('************************************************************************** ');
          }
          this.dashboardService.reset();
          this.isLoading = false;
          this.dashboardService.setSaleSuccess(true);
        }, (error: any) => {
          this.newSaleService.resetSale();
          //this.notificationService.showChangePasswordError();
          this.notificationService.mostrarErrorServidor();
          this.dashboardService.reset();
          this.isLoading = false;
          this.dashboardService.setSaleSuccess(true);

        }
      );
  }

  /**
   * creacion de codigo para SMS
   */
  onSendMessage() {
    this.isLoadingSms = true;

    this.dashboardService.SendMessage(this.numCelular).subscribe((response: RespuestaGenerica) => {
      if (response.code.code === 200 && response.body === 'OK') {
        this.isCodeGenerated = true;
        this.isLoadingSms = false;
        this.mensajeEnviado = true;
        this.errorEnMsjEnviado = false;
        this.isConfirmationCodeError = false;

      } else {
        this.isCodeGenerated = false;
        this.confirmationCodeErrorMsg = 'Error al enviar mensaje. Verifique por favor si el numero' +
          'es correcto.';
        this.mensajeEnviado = false;
        this.isLoadingSms = false;
        this.errorEnMsjEnviado = true;
      }

      if (!environment.production) {
        console.log('************************************************* environment.production = ', environment.production);
        console.log('respuesta codigo SMS:', response);
        console.log(this.numCelular);
        console.log('************************************************************************** ');
      }

    });
  }


  /**
   * validacion de codigo de confirmacion por SMS
   * @param e
   */
  checkValidationCode() {
    //this.validationCode = e.target.value;
    //console.log("this.validationCode");
    //console.log(this.validationCode);
    if (this.formSms.get('numValidacion').value.toString().length !== 4) {
      this.confirmationCodeErrorMsg = 'El codigo debe tener 4 digitos';
      this.isConfirmationCodeError = true;
      return;
    }

    this.dashboardService.getConfirmationCode(this.formSms.get('numValidacion').value).subscribe((response: RespuestaGenerica) => {
      this.confirmationCode = response;
      if (!environment.production) {
        console.log('************************************************* environment.production = ', environment.production);
        console.log('Codigo confirmacion: ', this.confirmationCode);
        console.log('************************************************************************** ');
      }

      const body = JSON.parse(response.body);
      if ((response.code.code === 200) && (body.estado === 0)) {
        this.isConfirmationCodeError = false;
        this.isConsentGiven = true;
        this.conMensajeria = false;
        this.mensajeEnviado = false;
      } else {
        this.confirmationCodeErrorMsg = 'Error con el código enviado, reingrese el código o reenvíe el mensaje nuevamente.';
        this.isConfirmationCodeError = true;
        this.isConsentGiven = false;
        this.mensajeEnviado = false;
        this.errorEnMsjEnviado = true;
        this.formSms.get('numValidacion').reset();
        this.codigoValidacion.nativeElement.focus();
      }
    });
  }

  get numCelular() {
    return this.formSms.get('numCelular').value;
  }

  //seccion de animacion
  // tslint:disable-next-line:member-ordering
  estado = 'inactive';
  isLoadingDescarga: boolean = false;
  descargaDisponible: boolean = false;

  animar() {
    this.estado = this.estado === 'active' ? 'inactive' : 'active';
  }

  ngAfterViewInit() {
    //despues de iniciar por completo el componente se produce el cambio.
    this.animar();
    this.cdRef.detectChanges();
  }

  descargarSolicitud() {
    this.isLoadingDescarga = true;
    this.descargaDisponible = true;
    this.dashboardService.getSolicitudAdhesion(this.solicitud).subscribe(res => {
      const newblob = new Blob([res], {type: 'application/pdf'});
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(newblob);
        return;
      }
      const data = window.URL.createObjectURL(newblob);
      let link = document.createElement('a');
      link.href = data;
      link.download = 'solicitud.pdf';
      link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

      setTimeout(() => {
        window.URL.revokeObjectURL(data);
        this.isLoadingDescarga = false;
        this.descargaDisponible = false;
      }, 2000);

    }, error => {
      this.isLoadingDescarga = false;
      this.descargaDisponible = true;
      this.msjSolicitud = true;
      console.log(error);
    });
  }

  cerrar() {
    this.tieneSolicitud = false;
  }

  //id: ENCUESTA
  seleccionDeArquetipo(id: number) {
    this.idArquetipoElegido = id;
    this.arquetipos.forEach((item) => {
      if (item.id === id) {
        item.estado = !item.estado;
      } else {
        item.estado = false;
      }
    });
  }

  seleccionDeMedioContacto(id: number) {
    this.mostrarMsjContacto = false;
    this.idContactoElegido = id;
    this.comunicacion.forEach((item) => {
      if (item.id === id) {
        item.estado = !item.estado;
        if (item.estado == false) {
          this.idContactoElegido = undefined;
        }
      } else {
        item.estado = false;
      }
    });
  }
}
