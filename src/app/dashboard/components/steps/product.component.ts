import { Producto } from '../../models/products/producto.model';

export interface ProductComponent {
  product: Producto;
  continue: any;
}
