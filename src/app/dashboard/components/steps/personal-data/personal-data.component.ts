import { Component, OnInit, OnDestroy, Output, EventEmitter, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';

import { State } from '../../../dashboard.enums';
import { Client } from '../../../models/sales/sales.model';
import { NewSaleService } from '../../../services/new-sale.service';
import { DashboardService } from '../../../services/dashboard.service';
import { OAuthService } from '../../../../shared/services/oauth.service';
import { animate, state, style, transition, trigger } from "@angular/animations";

@Component({
  selector: 'app-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['./personal-data.component.scss'],
    animations: [
        trigger('animacion', [
          state('inactive', style({
            opacity: '0'
          })),
          state('active', style({
            opacity: '1'
          })),
          transition('inactive => active', animate('600ms ease-in')),
          transition('active => inactive', animate('600ms ease-out')),
        ])
      ]
})
export class PersonalDataComponent implements OnInit, OnDestroy, AfterViewInit {
  client: Client;
  isLoading: boolean;
  productsSubs: Subscription;

  @Output() next: EventEmitter<any> = new EventEmitter<any>();

  constructor(private newSaleService: NewSaleService,
              private dashboardService: DashboardService,
              private userService: OAuthService,
              private cdRef:ChangeDetectorRef) {}

  ngOnInit() {
    this.isLoading = false;
    this.client = this.newSaleService.client;
    this.newSaleService.category = State.empty;
  }

  ngOnDestroy() {
    Object.keys(this).filter(k => k.endsWith('Subs'))
      .map(k => this[k].unsubscribe());
  }

  onSubmit(client: Client) {
    this.newSaleService.client = client;
    this.isLoading = true;
    const channel = 4;
    const cuit_socio = this.userService.userData.cuit;

   /*  this.productsSubs = this.dashboardService.getProducts(cuit_socio, channel)
      .subscribe(() => {
        this.isLoading = false;
        this.next.emit();
      }); */
    this.productsSubs = this.dashboardService.productsSubs;
    this.isLoading = false;
    this.next.emit();
  }
  
  //seccion de animacion
  estado = "inactive";
  animar () {
    this.estado = this.estado === 'active'? 'inactive' : 'active';
  }
  ngAfterViewInit(){
    //despues de iniciar por completo el componente se produce el cambio.
    this.animar();
    this.cdRef.detectChanges();
  }
}
