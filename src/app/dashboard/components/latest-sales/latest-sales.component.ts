import {Component, OnInit, Input, OnDestroy, ElementRef} from '@angular/core';
import {Observable, Subscription} from 'rxjs';


import {DatePipe} from '../../../shared/pipes/date.pipe';
import {SortPipe} from '../../../shared/pipes/sort.pipe';
import {listaDeEstados, SaleReport} from '../../models/sales/sales.model';
import {UserModel} from '../../../shared/models/user.model';
import {DashboardService} from '../../services/dashboard.service';
import {ExcelService} from '../../../shared/services/excel.service';
import {
  ADD_OPTIONS_CONFIG,
  CURRENT_OPTIONS_CONFIG, InterfazFiltro,
  JsonParaFiltrar, ObjetoFiltro,
  REMOVE_OPTIONS_CONFIG
} from '../../models/services/services.model';
import {CatalogueService} from '../../../shared/services/catalogue.service';
import {PuntoVenta} from '../../../shared/models/catalogue-models/branches-response.model';
import {Producto, Supervisor, Vendedor} from '../../models/products/producto.model';
import {NewSaleService} from '../../services/new-sale.service';

import {DatepickerOptions} from 'ng2-datepicker';
import * as esLocale from 'date-fns/locale/es';


@Component({
  selector: 'app-latest-sales',
  templateUrl: './latest-sales.component.html',
  styleUrls: ['./latest-sales.component.scss']
})
export class LatestSalesComponent implements OnInit, OnDestroy {

  fechaDesde: Date = null;
  fechaHasta: Date = null;
  options: DatepickerOptions = {
    minYear: 2016,
    maxYear: 2020,
    displayFormat: 'DD/MM/YYYY',
    barTitleFormat: 'MMMM YYYY',
    dayNamesFormat: 'dd',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    locale: esLocale,
    //minDate: new Date("01/01/2016"), // Minimal selectable date
    maxDate: new Date(),  // Maximal selectable date
    barTitleIfEmpty: 'Click to select a date',
    placeholder: 'Seleccionar fecha', // HTML input placeholder attribute (default: '')
    addClass: 'form-control', // Optional, value to pass on to [ngClass] on the input field
    addStyle: {background: 'ghostwhite', border: 'none', borderBottom: '1px solid #bb1f70', width: '100%'}, // Optional, value to pass to [ngStyle] on the input field
    fieldId: 'my-date-picker', // ID to assign to the input field. Defaults to datepicker-<counter>
    useEmptyBarTitle: false, // Defaults to true. If set to false then barTitleIfEmpty will be disregarded and a date will always be shown
  };
  
  private readonly totalToShow: number = 20;
  page: number = 1;
  icon: any='plus';
  field: string;
  isOpen: boolean;
  fields: Array<any>;
  sales: SaleReport[];
  getSalesSubs: Subscription;
  allSales: SaleReport[];

  //variables de filtros
  sucursalesAsignadas: string[];
  sucursales: PuntoVenta[] = [];
  vendedores: Vendedor[] = [];
  supervisor: Supervisor[] = [];
  sucursalesString: string;
  sucursalesArray: number[] = [];

  //listas para filtros
  listaDeEstados: Array<ObjetoFiltro> = new Array<ObjetoFiltro>();
  listaSucursales: Array<ObjetoFiltro> = new Array<ObjetoFiltro>();
  listaProductos: Array<ObjetoFiltro> = new Array<ObjetoFiltro>();
  listaVendedores: Array<ObjetoFiltro> = new Array<ObjetoFiltro>();
  listaSupervisores: Array<ObjetoFiltro> = new Array<ObjetoFiltro>();

  sucursal: string;


  //ngModels de inputs para filtros
  $nroDoc: number;
  $nombreCliente: string;
  $apellidoCliente: string;
  $vendedorElegido: ObjetoFiltro[];
  $productoElegido: ObjetoFiltro[];
  $sucuElegida: ObjetoFiltro[];
  $estado: ObjetoFiltro[];
  $supervisorElegido: ObjetoFiltro[];


  mostrarFiltroSucursal: boolean = false;
  sinFecha = false;


  currentOptionsConfig = CURRENT_OPTIONS_CONFIG;
  //value: string[] = [];


  addOptions: any = [];
  addOptionsConfig = ADD_OPTIONS_CONFIG;
  addOptionsValue: string[] = [];


  @Input() user: UserModel;

  mostrarListaSucursal: boolean;
  productos: Array<Producto>;


  removeOptions: any = this.listaSucursales;
  removeOptionsConfig = REMOVE_OPTIONS_CONFIG;
  removeOptionsValue: string[];
  esSupervisor: boolean;
  esAdministrador: boolean = false;
  mostrarFiltros: boolean = true;
  cargando: boolean = false;
  verFiltros: boolean = true;
  cambio: boolean = true;

  mostrarFecha: boolean = false;
  mostrarVenta: boolean = false;
  mostrarCert: boolean = false;
  mostrarDni: boolean = false;
  mostrarCliente: boolean = false;
  mostrarPrd: boolean = false;
  mostrarPlan: boolean = false;
  mostrarEstado: boolean = false;
  mostrarVend: boolean = false;
  mostrarSuper: boolean = false;
  mostrarDniVend: boolean = false;
  mostrarSucu: boolean = false;

  constructor(private sortPipe: SortPipe,
              private datePipe: DatePipe,
              private excelService: ExcelService,
              private dashboardService: DashboardService,
              private newSaleService: NewSaleService,
              private catalogueService: CatalogueService) {
  }

  ngOnInit() {
    //iniciando ngModels
    this.$productoElegido = [];
    this.$sucuElegida = [];

    //datepicker
    this.fechaDesde = null;
    this.fechaHasta = null;

    //booleans
    this.esSupervisor = false;
    this.cargando = false;

    //muestra los inputs para filtros segun Roles
    if( this.user.roles == null && this.user.roles == undefined && this.user.roles[0] == null ){
      this.user.roles[0] = 'Vendedor';
    }else{
      this.user.roles[0] == 'Supervisor' ? this.mostrarFiltroSucursal = true : this.mostrarFiltroSucursal = false;
    }

    this.dashboardService.getProducts(this.user.cuit);
    //this.sucursales = [];
    this.vendedores = [];
    this.sucursalesAsignadas = [];

    if(this.user.suc !== null && this.user.suc !== undefined){
      this.sucursalesString = this.user.suc;
      this.sucursalesArray = this.sucursalesString.split(',').map(function (i) {
        return parseInt(i, 10);
      });
    }

    this.mostrarListaSucursal = false;
    this.isOpen = false;
    this.fields = [
      {key: 'fechaActualizacion', text: 'fecha'},
      {key: 'idVenta', text: 'Nº venta'},
      {key: 'numeroCertificado', text: 'nº certificado'},
      {key: 'dni', text: 'DNI cliente'},
      {key: 'nombreCliente', text: 'nombre apellido'},
      {key: 'descripcionProducto', text: 'producto'},
      {key: 'idPlanElegido', text: 'plan'},
      {key: 'estado', text: 'estado'},
      {key: 'nombreVendedor', text: 'nombre vendedor'},
      {key: 'dniVendedor', text: 'dni vendedor'},
      {key: 'nombreSupervisor', text: 'nombre supervisor'},
      //{key: 'supervisorDni', text: 'dni supervisor'},
      {key: 'nombreSucursal', text: 'sucursal'}

    ];
    this.field = this.fields[1].key;
    this.sales = [];
    this.sinFecha = false;
    this.enviarFiltro();

  }

  enviarFiltro() {
    this.sucursal = this.newSaleService.branch;
    let jsonParaFiltrar = this.cargarJsonParaFiltro();
    this.cargando = false;
    if ( this.sinFecha ) {
      jsonParaFiltrar.fechaDesde = null;
      jsonParaFiltrar.fechaHasta = null;
      this.sinFecha = !this.sinFecha;
    }
    //console.log('Filtrar por: ' + JSON.stringify(jsonParaFiltrar));

    this.dashboardService.getSales(this.user.userid.toString(), jsonParaFiltrar)
      .subscribe((data: SaleReport[]) => {
        this.cargando = true;
        if (data && data.length) {
          this.allSales = data;

          this.allSales.forEach(sale => {
            /* if(sale.numeroCertificado === null || sale.numeroCertificado === 0){
              sale.estado = "Error";
            } */
            sale.numeroCertificado = this.decode(sale.numeroCertificado? sale.numeroCertificado : "-");
            sale.estado = this.decode(sale.estado);
            sale.descripcionProducto = this.decode(sale.descripcionProducto);
            sale.idPlanElegido = this.decode(sale.idPlanElegido? sale.idPlanElegido : "-");
            sale.nombreCliente = this.decode(sale.nombreCliente);
            sale.fechaActualizacion = this.datePipe
              .transform(sale.fechaActualizacion.toString());
          });
          //ordena lista en cada llamado segun la fecha. claves: ordenamiento - ordenar - exportar ordenado
          this.orderBy(this.allSales, false);
          const len = this.allSales.length;

          // if (len > this.totalToShow) {
          //   this.sales = this.allSales.slice(0, this.totalToShow);
          // } else {
          this.sales = this.allSales.slice();
          // }<
        } else {
          this.allSales = [];
        }
      });
    this.cargando = false;
    //this.resetFiltros();
  }

  ngOnDestroy() {
    if (this.getSalesSubs) {
      this.getSalesSubs.unsubscribe();
    }
  }

  toggleView() {
    this.cargarSucursalesParaFiltro();
    this.cargarProductosParaFiltro();
    this.cargarEstadosParaFiltro();
    this.cargarVendedoresParaFiltro();
    this.cargarSupervisoresParaFiltro();

    this.resetFiltros();

    this.enviarFiltro();
    this.isOpen = !this.isOpen;
    this.icon = this.isOpen ? 'minus' : 'plus';
    this.$nroDoc = null;
  }

  //ordena reportes por fecha al inicio y al exportar. claves: ordenamiento - ordenar - exportar ordenado
  orderBy(data: SaleReport[], isAsc: boolean = false) {
    this.sortPipe.transform(data, this.field, isAsc);
  }

  private decode(text: string) {
    try {
      return decodeURIComponent(escape(text).toLocaleUpperCase());
    }catch (e) {
      return text;
    }
  }


  exportData() {
    let strings = [];
    strings = this.sortPipe.transform(this.allSales, this.field, false).map(
      (v: SaleReport, i) =>
        [
          v.fechaActualizacion? v.fechaActualizacion.toString().toUpperCase() : "-",
          v.idVenta? v.idVenta.toString().toUpperCase() : "-",
          v.numeroCertificado? v.numeroCertificado.toString().toUpperCase() : "-",
          v.dni? v.dni.toString().toUpperCase() : "-",
          v.nombreCliente? v.nombreCliente.toString().toUpperCase() : "-",
          v.descripcionProducto? v.descripcionProducto.toString().toUpperCase() : "-",
          v.idPlanElegido? v.idPlanElegido.toString().toUpperCase() : "-",
          v.estado? v.estado.toString().toUpperCase() : "-",
          v.nombreVendedor? v.nombreVendedor.toString().toUpperCase() : "-",
          v.dniVendedor? v.dniVendedor.toString().toUpperCase() : "-",
          v.nombreSupervisor? v.nombreSupervisor.toString().toUpperCase() : "-",
          v.nombreSucursal? v.nombreSucursal.toString().toUpperCase() : "-",
        ]
    );

    this.excelService.exportAsExcelFile(
      [['Fecha', 'Nº Venta', 'Nº Certificado', 'DNI Cliente', 'Nombre apellido', 'Producto', 'Plan', 'Estado', 'Nombre vendedor', 'DNI vendedor', 'Nombre supervisor', 'Sucursal']]
        .concat(
          strings
        )
      , 'Últimas ventas');
  }

  cargarSucursalesParaFiltro() {
    //mostrar filtros para supervisor y administrador
    if (this.user.roles[0] == 'Supervisor') {
      //this.filtrarSucursalesSegunSupervisor();
      this.esSupervisor = true;

    }
    this.user.administrador? this.esAdministrador = true : this.esAdministrador = false;
    this.user.supervisor? this.esSupervisor = true : this.esSupervisor = false;
    
    this.sucursales = this.catalogueService.branchList.body;
    this.sucursales.forEach((item) => {
      this.listaSucursales.push(
        new ObjetoFiltro(
          this.decode(item.descripcion_punto_venta),
          item.id_punto_venta.toString(),
          item.id_punto_venta
        ));
    });
  }

  cargarEstadosParaFiltro() {
    this.listaDeEstados = listaDeEstados;
  }

  cargarProductosParaFiltro() {
    this.productos = this.dashboardService.productList;

    if (this.productos.length) {
      let pre = "";
      this.productos.forEach((item: Producto) => {
        item.id_negocio == 1 ? pre = "Seguros" : "";
        item.id_negocio == 2 ? pre = "Asistencias" : "";
        item.id_negocio == 3 ? pre = "Garantias" : "";
        this.listaProductos.push(
          new ObjetoFiltro(this.decode(item.descripcion_mkt), item.id_producto.toString(), item.id_producto)
        );
      });
    } else {
      this.productos = [];
    }
  }

  cargarVendedoresParaFiltro() {
    this.dashboardService.getVendedores().subscribe((response: Vendedor[]) => {
      this.vendedores = [];
      this.vendedores = response;
      //console.log(JSON.stringify(this.vendedores));
      this.vendedores.forEach((item) => {
        this.listaVendedores.push(
          new ObjetoFiltro(item.nombreApellido, item.id.toString(), item.id)
        );
      });
    });
  }

  cargarSupervisoresParaFiltro() {
    this.dashboardService.getSupervisor().subscribe((response: Supervisor[]) => {
      this.supervisor = response;
      //console.log(JSON.stringify(this.supervisor));
      this.supervisor.forEach((item) => {
        this.listaSupervisores.push(
          new ObjetoFiltro(this.decode(item.nombreApellido), item.id.toString(), item.id)
        );
      });
    });
  }

  filtrarSucursalesSegunSupervisor() {
    this.sucursales = this.catalogueService.branchList.body;
    this.sucursalesAsignadas = this.user.suc.split(',');
    let i = 0;
    this.sucursales.filter((item) => {
      let nroSuc = this.sucursalesAsignadas[i];
      let itemId = item.id_punto_venta.toString();
      if (nroSuc == itemId) {
        this.listaSucursales.push(
          new ObjetoFiltro(
            this.decode(this.sucursales[i].descripcion_punto_venta),
            this.sucursales[i].id_punto_venta.toString(),
            this.sucursales[i].id_punto_venta
          ));
      }
      i++;
    });
  }

  /*  removeSelectedOption() {
      this.listaSucursales = differenceWith(this.listaSucursales, this.removeOptionsValue, (oldValue: any, selectedValue: any) => {
        return oldValue[this.listaSucursalesConfig.valueField] === selectedValue;
      });
      this.refreshRemoveAndAddOptions();
    }*/

  /*  addSelectedOptions() {
      if (this.addOptionsValue && this.addOptionsValue.length > 0) {
        const optionsToAdd = intersectionWith(this.addOptions, this.addOptionsValue, (option: any, value: any) => {
          return option[this.addOptionsConfig.valueField] === value;
        });
        if (optionsToAdd && optionsToAdd.length > 0) {
          optionsToAdd.forEach((option: any) => {
            this.listaSucursales.push(option);
          });
        }
        this.refreshRemoveAndAddOptions();
      }
    }*/

  /*
    refreshRemoveAndAddOptions() {
      this.removeOptions = this.listaSucursales;

      this.addOptions = differenceWith(ExampleValues_Frameworks, this.removeOptions, (allValue: any, removedValue: any) => {
        return allValue[this.listaSucursalesConfig.valueField] === removedValue[this.removeOptionsConfig.valueField];
      });
    }*/


  private cargarJsonParaFiltro() {
    let jsonParaFiltrar: JsonParaFiltrar = new JsonParaFiltrar();

    if (this.$nroDoc) {
      jsonParaFiltrar.dni = this.$nroDoc;
    }
    if (this.$nombreCliente) {
      jsonParaFiltrar.nombre = this.$nombreCliente;
    }
    if (this.$apellidoCliente) {
      jsonParaFiltrar.apellido = this.$apellidoCliente;
    }
    if (this.$vendedorElegido) {
      jsonParaFiltrar.vendedores = [];
      this.$vendedorElegido.forEach((item: any) => {
        jsonParaFiltrar.vendedores.push(item);
      });
    }
    if (this.$supervisorElegido) {
      jsonParaFiltrar.supervisores = [];
      this.$supervisorElegido.forEach((item: any) => {
        jsonParaFiltrar.supervisores.push(item);
      });
    }
    if (this.$sucuElegida.length) {
      jsonParaFiltrar.sucursales = [];
      this.$sucuElegida.forEach((item: any) => {
        jsonParaFiltrar.sucursales.push(item);
      });
    }
    if (this.$productoElegido.length) {
      jsonParaFiltrar.productos = [];
      this.$productoElegido.forEach((item: any) => {
        jsonParaFiltrar.productos.push(item);
      });
    }
    if (this.$estado) {
      jsonParaFiltrar.estados = [];
      this.$estado.forEach((item: any) => {
        jsonParaFiltrar.estados.push(item);
      });

    }
    if (this.fechaDesde) {
      let dia = this.fechaDesde.getDate();
      let mes = this.fechaDesde.getMonth() + 1;
      let anio = this.fechaDesde.getFullYear();
      let fecha = dia + '/' + mes + '/' + anio;

      jsonParaFiltrar.fechaDesde = fecha;
    }
    if (this.fechaHasta) {
      let dia = this.fechaHasta.getDate();
      let mes = this.fechaHasta.getMonth() + 1;
      let anio = this.fechaHasta.getFullYear();
      let fecha = dia + '/' + mes + '/' + anio;

      jsonParaFiltrar.fechaHasta = fecha;
    }

    return jsonParaFiltrar;
  }

  private resetFiltros() {
    this.$nroDoc = null;
    this.$apellidoCliente = '';
    this.$nombreCliente = '';

    this.$vendedorElegido = [];
    this.$supervisorElegido = [];
    this.$productoElegido = [];
    this.$sucuElegida = [];
    this.$estado = [];

    this.fechaDesde = null;
    this.fechaHasta = null;
  }

  limpiarFiltros() {
    this.cambio = !this.cambio;
    this.cargarSucursalesParaFiltro();
    this.cargarProductosParaFiltro();
    this.cargarEstadosParaFiltro();
    this.cargarVendedoresParaFiltro();
    this.cargarSupervisoresParaFiltro();

    this.resetFiltros();

    this.sinFecha = true;
    this.enviarFiltro();
  }

  //ordenamiento segun las columnas de la tabla de reportes. claves: ordenamiento - ordenar - exportar ordenado
  ordenar(thSeleccionado: string, elItem:any, estado: boolean){
    //console.log("Item seleccionado");
    //console.log(elItem.id);
    let titulos:any = document.getElementsByClassName('items');
    for (let it of titulos){
      //it.classList.add('fa');
      it.classList.remove('fa-caret-up');
      it.classList.remove('fa-caret-down');
      if ( estado ){
        elItem.classList.add('fa-caret-down');

      }else {
        elItem.classList.add('fa-caret-up');
      }

    }

    //estado = !estado;

    //si selecciona columna de fecha, quitar guiones, ordenar y luego volver a agregar guiones.
/*    let allSalesConFechaSinGuion;
    if(thSeleccionado == 'fechaActualizacion'){
      allSalesConFechaSinGuion = this.allSales.forEach( it => {
        //let un = it.fechaActualizacion.replace(/ - /gi, "");
        let un = it.fechaActualizacion.split('-').join('');
        return un;
      });
      console.log(allSalesConFechaSinGuion);
    }*/
    var strings = this.sortPipe.transform(this.allSales, thSeleccionado, estado).map(
      (v: SaleReport, i) =>
        [
          v.fechaActualizacion? v.fechaActualizacion.toString() : "-",
          v.idVenta? v.idVenta.toString() : "-",
          v.numeroCertificado? v.numeroCertificado.toString() : "-",
          v.dni? v.dni.toString() : "-",
          v.nombreCliente? v.nombreCliente.toString() : "-",
          v.descripcionProducto? v.descripcionProducto.toString() : "-",
          v.idPlanElegido? v.idPlanElegido.toString() : "-",
          v.estado? v.estado.toString() : "-",
          v.nombreVendedor? v.nombreVendedor.toString() : "-",
          v.dniVendedor? v.dniVendedor.toString() : "-",
          v.nombreSupervisor? v.nombreSupervisor.toString() : "-",
          v.nombreSucursal? v.nombreSucursal.toString() : "-",
        ]
    );



  }

}
