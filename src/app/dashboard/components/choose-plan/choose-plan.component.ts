import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

import {Risk} from '../../models/utils/utils.model';
import {NewSaleService} from '../../services/new-sale.service';
import {Plan, Rango} from '../../models/products/plan.model';

@Component({
  selector: 'app-choose-plan',
  templateUrl: './choose-plan.component.html',
  styleUrls: ['./choose-plan.component.scss']
})
export class ChoosePlanComponent implements OnInit {
  isModalActive: boolean;
  selectedRisk: Risk;
  plansTable: Array<any>;
  tablePlansByRisks: any;
  tableHead: any; //gerr: 20/05 agregado para las cabeceras

  @Input() plans: Array<Plan> = [];

  @Output() next: EventEmitter<any> = new EventEmitter<any>();

  constructor(private newSaleService: NewSaleService) {
  }

  ngOnInit() {
    this.isModalActive = false;
    this.selectedRisk = new Risk();
    this.tablePlansByRisks = this.plans;
    this.tableHead = this.generatePlansTable(this.plans);
    /* se cambio this.generatePlansTable(this.plans) por this.plans */

  }

  selectRisk(risk: Risk) {
    this.selectedRisk = risk;
    this.isModalActive = true;
  }

  closeModal(event: boolean) {
    this.isModalActive = event;
  }

  selectPlan(item: any) {
    this.newSaleService.plan = this.decode(item.plan);
    this.newSaleService.saleRequest.venta.detalleSolicitudVenta.plan = this.decode(item.plan);
    //this.newSaleService.plan = this.decode(item.plan).charAt(0);
    this.newSaleService.precioPrdElegido = item.precio;
    this.newSaleService.saleRequest.venta.detalleSolicitudVenta.precioPrdElegido = item.precio;
    this.newSaleService.saleRequest.venta.detalleSolicitudVenta.codigo_plan_vt = item.plan_vt;
    this.next.emit();
  }

  /*  private generatePlansTable(plans) {
      const head = [];

      plans.forEach(({codigo_plan }: any) => {
        head.push(codigo_plan);
      });

      const cabecera: any[] = Array.from(new Set(head));
      return cabecera;
    } */

  private generatePlansTable(plans: Array<Plan>) {
    const table = {
      head: [],
      body: {
        planes: {}
      },
      plans: {}
    };

    const head = [];

    plans.forEach((v: Plan) => {
      if (v.lstRangos.length == 1) {
        head.push({plan: v.codigo_plan, plan_vt: v.codigo_plan_vt, precio: v.lstRangos[0].precio});
      } else {
        v.lstRangos.forEach((r: Rango) => {
          head.push({plan: v.codigo_plan + ' Edad: ' + r.desc_etario, plan_vt: v.codigo_plan_vt, precio: r.precio});
        });
      }
    });

    table.head = Array.from(new Set(head));

    plans.forEach((v: Plan) => {
      if (v.lstRangos.length === 1) {
        v.lstRangos[0].lstRiesgos.forEach(riesgo => {
          if (!table.body.planes[riesgo.desc_riesgo]) {
            table.body.planes[riesgo.desc_riesgo] = {};
          }
          table.body.planes[riesgo.desc_riesgo][v.codigo_plan] = riesgo.suma_riesgo;
        });

      } else {
        v.lstRangos.forEach(rng => {
          rng.lstRiesgos.forEach(riesgo => {
            if (!table.body.planes[riesgo.desc_riesgo]) {
              table.body.planes[riesgo.desc_riesgo] = {};
            }
            table.body.planes[riesgo.desc_riesgo][v.codigo_plan + ' Edad: ' + rng.desc_etario] = riesgo.suma_riesgo;
          });
        });

      }
    });
    return table;
  }

  private decode(texto: string) {
    return decodeURIComponent(escape(texto));
  }
}
