import {Component, Input, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {State} from "../../dashboard.enums";
import {MotivoCancelacion, RespuestaCancelacion, Cancelacion} from "../../models/sales/sales.model";
import {tap} from "rxjs/operators";
import {DashboardService} from "../../services/dashboard.service";

@Component({
  selector: 'app-cancelar-venta',
  templateUrl: './cancelar-venta.component.html',
  styleUrls: ['./cancelar-venta.component.scss']
})
export class CancelarVentaComponent implements OnInit {
  private readonly BASE_URL = environment.BASE_URL;

  //lista de motivos de cancelacion que se cargan con servicio desde el back
  listadoItem: MotivoCancelacion[];

  //booleano para mostrar el formulario de cancelacion
  isOpen: boolean = false;
  //muestra spin de cargando
  isLoading: boolean = false;

  //objeto para cargar el body con los datos del formulario de cancelacion.
  cancelacion: Cancelacion = new Cancelacion();

  //url para listar motivos de cancelacion

  //url para cancelacion de la venta
  urlCancelacion = `${this.BASE_URL}/ventas/cancelar`;

  //objeto motivos que se carga por servicio
  motivos: any[] = [];

  icon: any = 'plus';

  //formulario
  formGroup: FormGroup;
  private respuestaCancelacion: RespuestaCancelacion;

  //booleanos para manejar resspuestas de la cancelacion
  isCancel: boolean = false;
  isNotCancel: boolean = false;
  msjError = 'Verifique que los datos sean correctos o que la fecha se la actual.';

  constructor(
    private dashboardService: DashboardService,
    private http: HttpClient,
    private formBuilder: FormBuilder) {
  }

  ngOnInit() {

    this.dashboardService.getMotivosCancelacion().subscribe( data => {
      this.motivos = data;
    });

    this.formGroup = this.formBuilder.group({
      idVenta: [State.empty, [
        Validators.required,
        Validators.min(100),
        Validators.max(1000000)
      ]],
      motivo: [State.empty, Validators.required],
      document: [State.empty, [
        Validators.required,
        Validators.min(100000),
        Validators.max(99999999)
      ]]
    });
  }

  get idVenta() {
    return this.formGroup.get('idVenta');
  }

  get motivo() {
    return this.formGroup.get('motivo');
  }

  get document() {
    return this.formGroup.get('document');
  }

  toggleView() {
    this.isOpen = !this.isOpen;
    this.icon = this.isOpen ? 'minus' : 'plus';
    this.isNotCancel = false;
    this.isCancel = false;
    this.formGroup.reset();

  }

  cerrar() {
    this.isNotCancel = false;
    this.isCancel = false;

  }

  anularVenta() {
    this.isLoading = true;
    this.cancelacion.idMotivoCancelacion = this.formGroup.get('motivo').value;
    this.cancelacion.idSolicitud = this.formGroup.get('idVenta').value;
    this.cancelacion.dni = this.formGroup.get('document').value;

    this.dashboardService.anularVentaDelDia(this.cancelacion).subscribe( res => {
      this.respuestaCancelacion = res;
      this.isLoading = false;
      this.formGroup.reset();
      this.respuestaCancelacion.estado != 0 ? this.isNotCancel = true : this.isCancel = true;
      this.msjError = this.respuestaCancelacion.detalle;
    });
  }
}
