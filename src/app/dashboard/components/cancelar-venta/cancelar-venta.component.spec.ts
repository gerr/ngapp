import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelarVentaComponent } from './cancelar-venta.component';

describe('CancelarVentaComponent', () => {
  let component: CancelarVentaComponent;
  let fixture: ComponentFixture<CancelarVentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelarVentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelarVentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
