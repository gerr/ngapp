import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Producto } from '../../../models/products/producto.model';
import { NewSaleService } from '../../../services/new-sale.service';
import { TechnologyForm } from '../../../models/products/products.model';

@Component({
  selector: 'app-technology-template',
  templateUrl: './technology-template.component.html',
  styleUrls: ['./technology-template.component.scss']
})
export class TechnologyTemplateComponent implements OnInit {
  model: TechnologyForm;
  isDisabled: boolean;

  @Input() product: Producto = null;

  @Output() continue: EventEmitter<any> = new EventEmitter<any>();

  constructor(private newSaleService: NewSaleService) {}

  ngOnInit() {
    this.model = new TechnologyForm();
    this.isDisabled = true;
  }

  handleSubmit(model: TechnologyForm) {
    const detail = Object.assign({}, this.newSaleService.saleDetail, model);
    this.newSaleService.saleDetail = detail;
    this.continue.emit();
  }
}
