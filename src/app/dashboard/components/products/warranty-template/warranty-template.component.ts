import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { NewSaleService } from '../../../services/new-sale.service';
import { WarrantyForm } from '../../../models/products/products.model';
import { WarrantyProduct } from '../../../models/services/services.model';

@Component({
  selector: 'app-warranty-template',
  templateUrl: './warranty-template.component.html',
  styleUrls: ['./warranty-template.component.scss']
})
export class WarrantyTemplateComponent implements OnInit {
  model: WarrantyForm;
  isDisabled: boolean;

  @Input() product: WarrantyProduct = null;

  @Output() continue: EventEmitter<any> = new EventEmitter<any>();

  constructor(private newSaleService: NewSaleService) {}

  ngOnInit() {
    this.isDisabled = true;
    this.model = new WarrantyForm();
    this.model.subLinea = this.product.Sublinea;
  }

  handleSubmit(model: WarrantyForm) {
    const current = this.newSaleService.saleDetail;
    this.newSaleService.saleDetail = Object.assign({
      subLinea: this.product.Sublinea
    }, current, model);
    this.continue.emit();
  }
}
