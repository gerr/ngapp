import { HealthTemplateComponent } from './health-template/health-template.component';
import { HomeTemplateComponent } from './home-template/home-template.component';
import { TechnologyTemplateComponent } from './technology-template/technology-template.component';
import { PurchaseTemplateComponent } from './purchase-template/purchase-template.component';
import { WarrantyTemplateComponent } from './warranty-template/warranty-template.component';

import { ProductTemplate } from '../../models/product-template.model';

export const PRODUCTS_TEMPLATES = [
  HealthTemplateComponent,
  HomeTemplateComponent,
  TechnologyTemplateComponent,
  PurchaseTemplateComponent,
  WarrantyTemplateComponent
];

export const PRODUCTS_NAME = [
  'HealthTemplateComponent',
  'HomeTemplateComponent',
  'TechnologyTemplateComponent',
  'PurchaseTemplateComponent',
  'WarrantyTemplateComponent'
];

export const getProductTemplates = () => {
  return PRODUCTS_TEMPLATES.map((component, index) => {
    return new ProductTemplate(component, PRODUCTS_NAME[index]);
  });
};
