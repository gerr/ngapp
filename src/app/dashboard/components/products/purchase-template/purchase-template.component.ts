import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Producto } from '../../../models/products/producto.model';
import { NewSaleService } from '../../../services/new-sale.service';
import { PurchaseForm } from '../../../models/products/products.model';

@Component({
  selector: 'app-purchase-template',
  templateUrl: './purchase-template.component.html',
  styleUrls: ['./purchase-template.component.scss']
})
export class PurchaseTemplateComponent implements OnInit {
  model: PurchaseForm;

  @Input() product: Producto = null;

  @Output() continue: EventEmitter<any> = new EventEmitter<any>();

  constructor(private newSaleService: NewSaleService) {}

  ngOnInit() {
    this.model = new PurchaseForm();
  }

  handleSubmit(model: PurchaseForm) {
    model.tiempoAsegurar = model.tiempoAsegurar.replace('/', '-');
    const detail = Object.assign(this.newSaleService.saleDetail, model);
    this.newSaleService.saleDetail = detail;
    this.continue.emit();
  }
}
