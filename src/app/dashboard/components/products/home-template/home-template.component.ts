import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Client } from '../../..//models/sales/sales.model';
import { Producto } from '../../../models/products/producto.model';
import { HomeForm } from '../../../models/products/products.model';
import { NewSaleService } from '../../../services/new-sale.service';
import { CatalogueService } from '../../../../shared/services/catalogue.service';
import { CatalogService } from '../../../services/catalog.service';
import { ProvinceCatalogResponse } from '../../../models/services/services-response.model';

@Component({
  selector: 'app-home-template',
  templateUrl: './home-template.component.html',
  styleUrls: ['./home-template.component.scss']
})
export class HomeTemplateComponent implements OnInit {
  esDomicilioAsegurado: boolean;
  client: Client;
  city: string;
  street: string;
  department: string;
  question: string;
  hasAddress: boolean;
  hasResponded: boolean;
  isValid: boolean;
  showAddressForm: boolean;
  model: HomeForm;
  form: any;
  provinceList: Array<any>;
  localityList: Array<any>;
  saleDetail: any;

  @Input() product: Producto = null;

  @Output() continue: EventEmitter<any> = new EventEmitter<any>();

  constructor(private catalogService: CatalogService,
              private newSaleService: NewSaleService,
              private catalogueService: CatalogueService) {}

  ngOnInit() {
    this.model = new HomeForm();
    this.saleDetail = {};
    this.isValid = false;
    this.hasAddress = false;
    this.hasResponded = false;
    this.esDomicilioAsegurado = false;
    this.showAddressForm = false;
    this.question = this.getQuestion();
    this.client = this.newSaleService.client;

    this.catalogService.getProvinceCatalog()
      .subscribe(({ body }: ProvinceCatalogResponse ) => {
        this.provinceList = body;
      });

    /* gerr: 29/04 
    this.catalogueService.getLocalidades().subscribe(({ body }: any) => {
      this.localityList = body.localidades;
    }); */
  }

  handleAnwser(anwser: boolean) {
    this.hasResponded = true;
    this.esDomicilioAsegurado = anwser;

    if (anwser) {
     // this.showFormatedAddress(this.client);
      this.handleNext();
    } else {
      this.showAddressForm = true;
    }

    
  }

  handleRemove() {
    this.hasAddress = false;
    this.model = new HomeForm();
    this.showAddressForm = true;
    this.esDomicilioAsegurado = false;
  }

  handleNext() {
    if (this.showAddressForm) {
      this.showAddressForm = false;
      //this.showFormatedAddress(this.form);
      this.continue.emit(true);
    } else {
      this.updateSaleDetail();
      this.continue.emit(true);
    }
  }

  handleSubmit(form: any) {
    //alert("entrando a handlesubmit");
    this.form = form;
    this.isValid = !!this.form;

    this.handleNext();
  }

  private showFormatedAddress(model: any) {
    let complement: string;
    const {
      calle, numero, piso, departamento, torre,
      codigoPostal, idLocalidad, idProvincia
    } = model;

    this.saleDetail = {
      calle, numero, piso, departamento, torre, codigoPostal, idLocalidad, idProvincia
    };

    this.street = `${calle} ${numero}`;

    if (piso) {
      complement = `Piso ${piso}`;
      if (departamento) {
        complement += `, Depto. ${departamento}`;
      }
      this.department = complement;
    }

    this.city = `${this.getProvince(idProvincia)}`;
        //this.city = `${this.getLocality(idLocalidad, idProvincia)} (${codigoPostal}), ${this.getProvince(idProvincia)}`;
    this.hasAddress = true;
  }

  private getQuestion(): string {
    let message = 'El domicilio que vas a asegurar es el mismo que fue declarado';
    const { id_negocio, descripcion_mkt } = this.product;

    if (+id_negocio === 2 && descripcion_mkt.toLocaleLowerCase().includes('asist')) {
      message = 'El domicilio para asistir es el mismo que fue declarado';
    }

    return message;
  }

  private updateSaleDetail() {
    this.newSaleService.saleDetail =
      Object.assign({
        esDomicilioAsegurado: this.esDomicilioAsegurado
      }, this.newSaleService.saleDetail, this.saleDetail);
  }

  private getProvince(idProvince: string) {
    const result = this.provinceList.find(({ id }: any) => id === idProvince);
    return result.descripcion;
  }

  private getLocality(idLocality: string, idProvince: string) {
    return "";
  }
}
