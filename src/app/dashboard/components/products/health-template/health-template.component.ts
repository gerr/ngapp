import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

import {Client} from '../../../models/sales/sales.model';
import {Role} from '../../../dashboard.enums';
import {Producto} from '../../../models/products/producto.model';
import {NewSaleService} from '../../../services/new-sale.service';
import {Additional, Beneficiary} from '../../../models/utils/utils.model';
import {DashboardService} from '../../../services/dashboard.service';

@Component({
  selector: 'app-health-template',
  templateUrl: './health-template.component.html',
  styleUrls: ['./health-template.component.scss']
})
export class HealthTemplateComponent implements OnInit {
  private id: number;
  model: any;
  client: Client;
  esTitular: boolean;
  mainQuestion: string;
  canAddTitular: boolean;
  showTitularForm: boolean;
  additionals: Additional[];
  canAddAdditional: boolean;
  canAddBeneficiary: boolean;
  showAdditionalForm: boolean;


  showHomeComponent: boolean = false;
  showTechnologyComponent: boolean = false;

  beneficiaries: Beneficiary[];
  showBeneficiaryForm: boolean;
  addAnotherAdditional: boolean;
  beneficiarioOAdicionales: Array<any>;
  sumPercentage: number = 0;
  @Input() product: Producto = null;

  @Output() continue: EventEmitter<any> = new EventEmitter<any>();

  constructor(private newSaleService: NewSaleService, private dashboardService: DashboardService) {
  }

  ngOnInit() {
    this.id = 100;
    this.model = {};
    this.esTitular = true;
    this.additionals = [];
    this.beneficiaries = [];
    this.canAddTitular = false;
    this.canAddAdditional = false;
    this.canAddBeneficiary = false;
    this.showTitularForm = false;
    this.showAdditionalForm = false;
    this.showBeneficiaryForm = false;
    this.addAnotherAdditional = false;
    this.beneficiarioOAdicionales = [];
    this.client = this.newSaleService.client;
    this.mainQuestion = `<span class="bold">${this.getFullName(this.client)}</span> es el titular del seguro`;

    /*    if(this.newSaleService.warranty){
          this.canAddTitular = true;
          this.canAddAdditional = false;
          this.canAddBeneficiary = false;
        }*/
    if (this.product.conjunto_producto == undefined) {
      this.canAddTitular = false;
      this.canAddAdditional = false;
      this.canAddBeneficiary = false;
      this.product.conjunto_producto.adicional = -1;
      this.continuar(true);
      this.continue.emit();
    }

    if (this.product.conjunto_producto.titular == 1) {
      this.canAddTitular = true;
      this.canAddAdditional = false;
      this.canAddBeneficiary = false;
    } else if (this.product.conjunto_producto.complementario == "DIRECCION") {
      this.showHomeComponent = true;
    } else if (this.product.conjunto_producto.complementario == "TECNOLOGIA") {
      this.showTechnologyComponent = true;
    } else {
      if (this.product.conjunto_producto.adicional == 1) {
        this.canAddTitular = false;
        this.canAddAdditional = true;
        this.canAddBeneficiary = false;
      } else {
        if (this.product.conjunto_producto.beneficiario == 1) {
          this.canAddTitular = false;
          this.canAddAdditional = false;
          this.canAddBeneficiary = true;
        } else {
          this.canAddTitular = false;
          this.canAddAdditional = false;
          this.canAddBeneficiary = false;
          this.continue.emit();
        }
      }
    }
  }

  continuar(anwser: boolean) {


    if (this.product.conjunto_producto.adicional == 1) {
      this.canAddTitular = false;
      this.canAddAdditional = true;
      this.canAddBeneficiary = false;
    } else {
      if (this.product.conjunto_producto.beneficiario == 1) {
        this.canAddTitular = false;
        this.canAddAdditional = false;
        this.canAddBeneficiary = true;
      } else {
        this.canAddTitular = false;
        this.canAddAdditional = false;
        this.canAddBeneficiary = false;
        this.continue.emit();
      }
    }

  }

  isTitular(anwser: boolean) {
    /*    if(this.newSaleService.warranty){
          console.log("Producto warranty elegido");
          console.log(JSON.stringify(this.newSaleService.warranty));

          this.continue.emit();
        }*/
    if (anwser) {
      this.additionals.push({
        type: 'main',
        name: this.getFullName(this.client)
      });
      this.esTitular = true;

      if (this.product.conjunto_producto.complementario == "DIRECCION") {
        this.showHomeComponent = true;
      } else if (this.product.conjunto_producto.complementario == "TECNOLOGIA") {
        this.showTechnologyComponent = true;
      } else if (this.product.conjunto_producto.adicional == 1) {

        this.canAddAdditional = true;
      } else if (this.product.conjunto_producto.beneficiario == 1) {
        this.canAddBeneficiary = true;
      } else
        this.continue.emit();

    } else {
      this.showTitularForm = true;
    }
    this.canAddTitular = false;
    this.canAddTitular = false;
    //this.showAdditionalForm = false;


  }

  saveBeneficiary(model: any) {
    const id = this.generateId();
    this.showBeneficiaryForm = false;
    this.sumPercentage += parseInt(model.porcentaje);
    if (this.sumPercentage <= 100) {
      model.fechaNacimiento = this.dashboardService.formatDate2(this.model.fechaNacimiento);
      this.canAddBeneficiary = true;
      this.beneficiaries.push({id, name: this.getFullName(model), percentage: model.porcentaje});
      this.addBeneficiaryOrAdditional(model, id, Role.BENEFICIARY);
    } else {

      this.updateSaleDetail();
    }
    this.model = {};
  }

  saveTitular(model: any) {
    //const id = this.generateId();
    this.showTitularForm = false;
    /*     this.addBeneficiaryOrAdditional(model, id, Role.TITULAR);
        this.additionals.push({ id, type: 'main', name:  this.getFullName(model), id_relacion_titular: 'TITULAR', persona: model }); */
    const clientePagador = this.newSaleService.saleRequest.venta.cliente;
    this.newSaleService.saleRequest.venta.cliente = model;
    this.newSaleService.saleRequest.venta.clientePagador = clientePagador;

    this.model = {};
    this.esTitular = true;

    if (this.product.conjunto_producto.complementario == "DIRECCION") {
      this.showHomeComponent = true;
    } else if (this.product.conjunto_producto.complementario == "TECNOLOGIA") {
      this.showTechnologyComponent = true;
    } else if (this.product.conjunto_producto.adicional == 1) {
      this.canAddAdditional = true;
    } else if (this.product.conjunto_producto.beneficiario == 1) {
      this.canAddBeneficiary = true;
    } else {
      this.continue.emit();
    }


  }

  saveAdditional(model: any) {
    const id = this.generateId();
    this.addBeneficiaryOrAdditional(model, id, Role.ADDITIONAL);
    const additional = new Additional(id, 'other', this.getFullName(model));
    this.showAdditionalForm = false;
    this.canAddAdditional = true;
    this.additionals.push(additional);
    this.model = {};
    if (this.product.conjunto_producto.beneficiario == 1) {
      this.canAddBeneficiary = true;
    }
  }

  addAdditional(anwser: boolean) {
    this.canAddAdditional = false;
    this.showAdditionalForm = anwser;
    if (!anwser && this.product.conjunto_producto.beneficiario == 0) {
      this.continue.emit();
    }
    this.addAnotherAdditional = !anwser;
    if (anwser == false && this.product.conjunto_producto.beneficiario == 1) {
      this.canAddBeneficiary = true;
    } else {
      this.canAddBeneficiary = false;
    }
    //this.showTitularForm = false;
  }

  cancelAddition() {
    this.showAdditionalForm = false;
    this.addAnotherAdditional = true;
    this.model = {};

    if (this.beneficiaries.length) {
      if (this.sumPercentage < 100)
        this.canAddBeneficiary = true;
      else
        this.updateSaleDetail();
    } else {
      this.showBeneficiaryForm = true;
    }
  }


  removeAditional(additional: Additional) {
    if (additional.type === 'main') {
      this.esTitular = false;
      this.additionals = [];
      this.beneficiaries = [];
      this.canAddTitular = false;
      this.showAdditionalForm = false;
      this.showBeneficiaryForm = false;
      this.canAddAdditional = false;
      this.canAddBeneficiary = false;
      this.addAnotherAdditional = false;
      this.beneficiarioOAdicionales = [];
      this.model = {};
      this.id = 100;
    } else {
      this.additionals = this.additionals.filter(x => x !== additional);
      this.removeBeneficiaryOrAdditional(additional.id);
    }
  }

  addBeneficiary(anwser: boolean) {
    this.canAddBeneficiary = false;

    if (anwser) {
      this.showBeneficiaryForm = true;
    } else {
      this.updateSaleDetail();
    }
  }


  removeBeneficiary(beneficiary: Beneficiary) {
    this.beneficiaries = this.beneficiaries.filter(x => x !== beneficiary);
    this.removeBeneficiaryOrAdditional(beneficiary.id);
  }

  resetFlow() {
    this.showBeneficiaryForm = false;
    this.canAddBeneficiary = false;

    if (!this.showAdditionalForm) {
      this.model = {};
      this.showAdditionalForm = true;
    }
  }

  private getFullName({nombre: name, apellido: lastName}: any) {
    return `${name} ${lastName}`;
  }

  private addBeneficiaryOrAdditional(model: any, id: number, type: number) {
    this.beneficiarioOAdicionales.push({
      id,
      type,
      model
    });
  }

  private removeBeneficiaryOrAdditional(id: number) {
    this.beneficiarioOAdicionales = this.beneficiarioOAdicionales.filter(item => {
      return item.id !== id;
    });
  }

  private generateId(): number {
    return this.id++;
  }

  private updateSaleDetail() {
    const current = this.newSaleService.saleDetail;
    const detail = {beneficiarioOAdicionales: [], esTitular: this.esTitular};

    this.beneficiarioOAdicionales.map(item => {
      const percentage = item.type === Role.BENEFICIARY ? item.model.porcentaje : null;

      if (item.type === Role.BENEFICIARY) {
        delete item.model.porcentaje;
      }

      detail.beneficiarioOAdicionales.push({
        porcentajeCobro: percentage,
        idRol: item.type,
        datosGeneralesBeneficiarioOAdicional: item.model
      });
    });

    this.newSaleService.saleDetail = Object.assign({}, current, detail);
    this.continue.emit();
  }
}
