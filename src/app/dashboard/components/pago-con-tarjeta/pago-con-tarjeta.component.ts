import { Component, ElementRef, Inject, Input, OnInit, ViewChild, NgZone } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ConfirmationCode } from "../../models/confirmation-code.model";
import { DashboardService } from "../../services/dashboard.service";
import { OAuthService } from "../../../shared/services/oauth.service";
import { NewSaleService } from "../../services/new-sale.service";
import { NotificationService } from "../../../shared/services/notification.service";
import { cardIssuer, State } from "../../dashboard.enums";
import { CatalogueService } from "../../../shared/services/catalogue.service";
import { catchError, tap } from "rxjs/operators";
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../../environments/environment";
import { RespuestaVenta } from "../../models/ventas-realizadas/respuesta-venta.model";
import { DatosTarjeta, ErrorSPS, RespuestaTokenDecidir } from "../../models/sales/sales.model";
// import { get } from 'scriptjs';
import { error } from "util";
import { now } from "moment";
import { PayData } from "../../../shared/models/user.model";

var respuestaApi;

@Component ( {
  selector: 'app-pago-con-tarjeta',
  templateUrl: './pago-con-tarjeta.component.html',
  styleUrls: [ './pago-con-tarjeta.component.scss' ]
} )
export class PagoConTarjetaComponent implements OnInit {
  URL_PSP = environment.URL_PSP;
  API_KEY = environment.API_KEY_SPS;
  datosTarjeta: DatosTarjeta = new DatosTarjeta ();
  listaErroresTarjeta: ErrorSPS[] = [];
  
  disabled = {
    tipoDocumento: false,
    nroDocumento: false,
    apellido: false,
    nombre: false,
    fechaNacimiento: false,
    cuil: false,
    idGenero: false
  };
  
  isLoading: boolean;
  
  typeCard = 'CARD';
  typeValC = 'VALC';
  typeVenc = 'VENC';
  typeText = 'L';
  
  clientName: string;
  phones: Array<string>;
  
  formularioPago: FormGroup;
  uploadForm: FormGroup;
  cardIssuer: cardIssuer;
  cardType = '';
  isValidCardIssuer = false;
  
  isLuhnCheck = false;
  isLuhnMsjOn = false; //false por defecto
  
  filename = '';
  
  validationMethod = true; // true -> sms / false -> dni pic
  validationMessage = '';
  validationCode = '';
  confirmationCode: ConfirmationCode;
  isConsentGiven = false;
  isConfirmationCodeError = false;
  confirmationCodeErrorMsg = '';
  imageUploadErrorMsg = '';
  isNotPUA: boolean;
  grayMaster: boolean = false;
  grayVisa: boolean = false;
  grayAmex: boolean = false;
  
  @ViewChild ( 'cardNum2' ) cardnum2: ElementRef;
  @ViewChild ( 'cardNum3' ) cardnum3: ElementRef;
  @ViewChild ( 'cardNum4' ) cardnum4: ElementRef;
  @Input () isActive: Boolean = false;
  @Input () precio: any;
  error: boolean;
  codigoSegu: string;
  
  numDocumentoIsValid: boolean = false;
  nombreCompletoTitularIsValid: boolean = false;
  @Input () model: any;
  numDocumentoInvalid: boolean = false;
  nombreCompletoInvalid: boolean = false;
  confirmarVenta: boolean = false;
  fechaVencimientoInvalid: boolean = false;
  codigoSegInvalid: boolean = false;
  isConfirmado: boolean = false;
  
  val1 = false;
  val2 = false;
  val3 = false;
  val4 = false;
  @Input () descripcion: string;
  @Input () plan: string;
  isMsjError: boolean = false;
  msjError: string = '';
  planElegido: string;
  mes: string;
  anio: string;
  payData: PayData;
  
  constructor ( private formBuilder: FormBuilder,
                private dashboardService: DashboardService,
                private oauthService: OAuthService,
                private newSaleService: NewSaleService,
                private http: HttpClient,
                private notificationService: NotificationService,
                private catalogueService: CatalogueService,
                private zone: NgZone ) {
    
  }
  
  crearFormulario () {
    this.formularioPago = this.formBuilder.group ( {
      codigoSeg: [ State.empty, [
        Validators.required,
        Validators.min ( 100 ),
        Validators.max ( 999 ) ]
      ],
      //tipoDocumento: [State.empty, [Validators.maxLength(4)]],
      numDocumento: [ State.empty, [
        Validators.required,
        Validators.min ( 1000000 ),
        Validators.max ( 99999999 ) ]
      ],
      nombreCompletoTitular: [ State.empty, [ Validators.required, Validators.minLength ( 1 ), Validators.maxLength ( 40 ) ] ],
      cardNum1: [ State.empty, [ Validators.required, Validators.minLength ( 4 ) ] ],
      cardNum2: [ State.empty, [ Validators.required, Validators.minLength ( 4 ) ] ],
      cardNum3: [ State.empty, [ Validators.required, Validators.minLength ( 4 ) ] ],
      cardNum4: [ State.empty, [ Validators.required, Validators.minLength ( 3 ) ] ],
      fechaVencimiento: [ State.empty, [ Validators.required, Validators.minLength ( 4 ) ] ]
    } );
  }
  
  
  get numDocumento () {
    return this.formularioPago.get ( 'numDocumento' );
  }
  
  get codigoSeg () {
    return this.formularioPago.get ( 'codigoSeg' );
  }
  
  get nombreCompletoTitular () {
    return this.formularioPago.get ( 'nombreCompletoTitular' );
  }
  
  get fechaVencimiento () {
    return this.formularioPago.get ( 'fechaVencimiento' );
  }
  
  ngOnInit () {
    
    this.crearFormulario ();
    this.planElegido = this.newSaleService.selectedWarrantyPlan;
    this.isLoading = false;
    this.isNotPUA = true;
    const { nombre, apellido } = this.newSaleService.client;
    this.clientName = `${ nombre } ${ apellido }`;
    
    
    //this.isLuhnCheck = true;
    this.isValidCardIssuer = false;
    
    //this.formularioPago.get('tipoDocumento').enable();
    //this.formularioPago.get('tipoDocumento').clearValidators();
    this.formularioPago.get ( 'numDocumento' ).enable ();
    this.formularioPago.get ( 'numDocumento' ).clearValidators ();
    this.formularioPago.get ( 'nombreCompletoTitular' ).enable ();
    this.formularioPago.get ( 'nombreCompletoTitular' ).clearValidators ();
    this.formularioPago.get ( 'cardNum1' ).enable ();
    this.formularioPago.get ( 'cardNum1' ).clearValidators ();
    this.formularioPago.get ( 'cardNum2' ).enable ();
    this.formularioPago.get ( 'cardNum2' ).clearValidators ();
    this.formularioPago.get ( 'cardNum3' ).enable ();
    this.formularioPago.get ( 'cardNum3' ).clearValidators ();
    this.formularioPago.get ( 'cardNum4' ).enable ();
    this.formularioPago.get ( 'cardNum4' ).clearValidators ();
    this.formularioPago.get ( 'fechaVencimiento' ).enable ();
    this.formularioPago.get ( 'fechaVencimiento' ).clearValidators ();
    this.formularioPago.get ( 'codigoSeg' ).enable ();
    this.formularioPago.get ( 'codigoSeg' ).clearValidators ();
    
    if ( this.oauthService.isLoginAuto )
      this.cargarTarjetaSiEsLoginAutomatico ();
    
  }
  
  
  onKeyUpInput ( inputNumber: number ) {
    const num: string = this.formularioPago.get ( 'cardNum' + inputNumber ).value;
    
    if ( num.length === 4 ) {
      switch ( inputNumber ) {
        case 1: {
          this.cardnum2.nativeElement.focus ();
          break;
        }
        case 2: {
          this.cardnum3.nativeElement.focus ();
          break;
        }
        case 3: {
          this.cardnum4.nativeElement.focus ();
          break;
        }
        default: {
          break;
        }
      }
    }
    
    if ( inputNumber === 1 ) {
      if ( num.length >= 2 ) {
        this.checkCreditCardIssuer ( num );
      } else {
        if ( num.length < 2 ) {
          this.cardType = '';
        }
      }
    }
    
    switch ( this.cardType ) {
      case cardIssuer.visa: {
        this.isValidCardIssuer = true;
        break;
      }
      case cardIssuer.master: {
        this.isValidCardIssuer = true;
        break;
      }
      case cardIssuer.amex: {
        this.isValidCardIssuer = true;
        break;
      }
      default: {
        this.cardType = 'no compatible';
        this.isValidCardIssuer = false;
        break;
      }
    }
  }
  
  checkCreditCardIssuer ( cardNumber: string ) {
    const firstDigit = cardNumber[0];
    const secDigit = cardNumber[1];
    this.cardType = '';
    
    if ( firstDigit === '4' ) {  // VISA
      this.cardType = cardIssuer.visa; // cardIssuer.visa;
    } else {
      if ( ( firstDigit === '3' ) && ( ( +secDigit >= 4 ) && ( +secDigit <= 7 ) ) ) { // AMEX
        this.cardType = cardIssuer.amex; // cardIssuer.amex;
      } else {
        if ( ( firstDigit === '5' ) && ( ( +secDigit >= 1 ) && ( +secDigit <= 5 ) ) ) {  // MASTERCARD
          this.cardType = cardIssuer.master; // cardIssuer.master;
        } else {
          if ( ( firstDigit === '2' ) && ( ( +secDigit >= 2 ) && ( +secDigit <= 7 ) ) ) {  // MASTERCARD 2
            this.cardType = cardIssuer.master; // cardIssuer.master;
          }
        }
      }
    }
  }
  
  checkLuhn () {
    let ccNumber = this.formularioPago.get ( 'cardNum1' ).value;
    ccNumber += this.formularioPago.get ( 'cardNum2' ).value;
    ccNumber += this.formularioPago.get ( 'cardNum3' ).value;
    ccNumber += this.formularioPago.get ( 'cardNum4' ).value;
    if ( ccNumber.startsWith ( '0000' ) || ccNumber.startsWith ( '1111' ) || ccNumber.startsWith ( '2222' ) || ccNumber.startsWith ( '3333' ) || ccNumber.startsWith ( '4444' ) || ccNumber.startsWith ( '5555' ) || ccNumber.startsWith ( '6666' ) || ccNumber.startsWith ( '7777' ) || ccNumber.startsWith ( '8888' ) || ccNumber.startsWith ( '9999' ) ) {
      this.isLuhnCheck = false;
      this.isValidCardIssuer = false;
      this.isLuhnMsjOn = true;
      return;
    }
    
    if ( ccNumber.length >= 12 ) {
      const max = ccNumber.length - 1;
      let sum = 0;
      let alternate = false;
      let number;
      for ( let i = max; i >= 0; i-- ) {
        number = ccNumber.substring ( i, i + 1 );
        if ( alternate ) {
          number *= 2;
          if ( +number > 9 ) {
            number -= 9;
          }
        }
        sum += +number;
        alternate = !alternate;
      }
      this.isLuhnCheck = ( sum % 10 === 0 );
      this.isLuhnMsjOn = !this.isLuhnCheck;
      if ( this.isLuhnMsjOn ) {
        this.isValidCardIssuer = false;
        this.isLuhnCheck = false;
      } else {
        this.isValidCardIssuer = true;
      }
      
    } else {
      this.isLuhnCheck = false;
      this.isValidCardIssuer = false;
      
    }
    if(this.oauthService.isLoginAuto && this.isLuhnCheck){
      this.isConfirmado = true;
    }
  }
  
  
  validaciones ( key: string, elem: any ) {

    //asdf
    let {
      fechaVencimiento
    } = this.formularioPago.value;
    
    if ( key === "numDocumento" ) {
      elem.toString ().length >= 6 && elem.toString ().length < 9 ? this.numDocumentoInvalid = false : this.numDocumentoInvalid = true;
      this.val1 = !this.numDocumentoInvalid;
    }
    
    if ( key === "nombreCompletoTitular" ) {
      elem.toString ().length > 3 && elem.toString ().length < 25 ? this.nombreCompletoInvalid = false : this.nombreCompletoInvalid = true;
      this.val2 = !this.nombreCompletoInvalid;
    }
    
    if ( key === "codigoSeg" ) {
      elem.toString ().length < 3 || elem.toString ().length > 4 ? this.codigoSegInvalid = true : this.codigoSegInvalid = false;
      this.val3 = !this.codigoSegInvalid;
    }
    
    if ( key === "fechaVencimiento" ) {
      let mesActual = new Date ().getMonth () + 1;
      let anioActual = new Date ().getFullYear ().toString ().substring ( 2, 4 );
      let anioActualInt = parseInt ( anioActual );
      
      let mes = parseInt ( elem.substring ( 0, 2 ) );
      let anio = parseInt ( elem.substring ( 3, 5 ) );
      
      /**
       * validacion de fecha
       * consulta si anio y mes son mayor o igual que la fecha actual y si no pasa mas de 6 años a la fecha actual
       */
      if ( anio < anioActualInt || anio == anioActualInt && mes < mesActual || anio > anioActualInt + 6 ) {
        this.fechaVencimientoInvalid = true;
        return;
      }
      
      let mesIsTrue = this.inRange ( mes, 1, 12 );
      let anioIsTrue = this.inRange ( anio, anioActualInt, 26 );
      if ( mesIsTrue && anioIsTrue ) {
        this.fechaVencimientoInvalid = false;
        this.val4 = !this.fechaVencimientoInvalid;
      } else {
        this.fechaVencimientoInvalid = true;
      }
    }
    
    /**
     * si validaciones anteriores dan ok
     * se activa boton de confirmar venta
     */
    if ( this.val1 && this.val2 && this.val3 && this.val4 && this.isLuhnCheck ) {
      this.isConfirmado = true;
      this.mes = fechaVencimiento.substring ( 0, 2 );
      this.anio = fechaVencimiento.substring ( 3, 5 );
    } else {
      this.isConfirmado = false;
    }
  
    if(this.oauthService.isLoginAuto){
      this.isConfirmado = true;
      return
    }
    
  }
  
  // return true if in range, otherwise false
  inRange ( x, min, max ) {
    return ( ( x - min ) * ( x - max ) <= 0 );
  }
  
  cerrarMsjError () {
    this.isMsjError = false;
    this.listaErroresTarjeta = [];
    
  }
  
  
  /*  checkNombre() {
      let {nombreCompletoTitular} = this.formularioPago.value;
      if (nombreCompletoTitular.length > 3) {
        this.formularioPago.get('cardNum1').enable();
        this.formularioPago.get('cardNum1').clearValidators();
        this.formularioPago.get('cardNum2').enable();
        this.formularioPago.get('cardNum2').clearValidators();
        this.formularioPago.get('cardNum3').enable();
        this.formularioPago.get('cardNum3').clearValidators();
        this.formularioPago.get('cardNum4').enable();
        this.formularioPago.get('cardNum4').clearValidators();
      } else {
        this.formularioPago.get('cardNum1').disable();
        this.formularioPago.get('cardNum1').clearValidators();
        this.formularioPago.get('cardNum2').disable();
        this.formularioPago.get('cardNum2').clearValidators();
        this.formularioPago.get('cardNum3').disable();
        this.formularioPago.get('cardNum3').clearValidators();
        this.formularioPago.get('cardNum4').disable();
        this.formularioPago.get('cardNum4').clearValidators();
      }
    }*/
  
  confirmar () {
    let {
      fechaVencimiento
    } = this.formularioPago.value;
    
    if ( this.codigoSegInvalid && this.nombreCompletoInvalid && this.numDocumentoInvalid && fechaVencimiento.length < 4 ) {
      this.isLoading = false;
      return;
    } else if ( fechaVencimiento === '' ) {
      this.fechaVencimientoInvalid = true;
      return;
    }
    this.getTokenPago ();
  }
  
  getTokenPago () {
    const publicApiKey = this.API_KEY;
    const urlSandbox = this.URL_PSP;
    // @ts-ignore
    const decidir = new Decidir ( urlSandbox );
    decidir.setPublishableKey ( publicApiKey );
    decidir.setTimeout ( 5000 );//timeout de 5 segundos
    var form = document.querySelector ( '#formulario' );
    console.log("form", form);
    window["thisSelf"] = this;
    let miThis = window["thisSelf"];
    addEvent ( form, 'submit', sendForm );
    
    //funcion de invocacion con sdk
    function addEvent ( el, eventName, handler ) {
      if ( el.addEventListener ) {
        el.addEventListener ( eventName, handler );
      } else {
        el.attachEvent ( 'on' + eventName, function () {
          handler.call ( el );
        } );
      }
    }
    
    function sendForm ( event ) {
      event.preventDefault ();
      decidir.createToken ( form, sdkResponseHandler );
      return false;
    }
    
    //funcion para manejar la respuesta
    function sdkResponseHandler ( status, response ) {
      if ( status != 200 && status != 201 ) {
        if ( response.error.length ) {
          miThis.listaErroresTarjeta = response.error;
        }
        miThis.isMsjError = true;
      } else {
        miThis.newSaleService.saleRequest.venta.respuestaApi = response;
        
        if(!environment.production){
            console.log("************************************************* environment.production = ", environment.production);
            console.log("sdkResponseHandler", JSON.stringify(response));
            console.log("************************************************************************** ");
        }
        
        miThis.isLoading = true;
        miThis.dashboardService.hiddenActivado = true;
        miThis.newSaleService.saleRequest.venta.detalleSolicitudVenta.precio = miThis.precio;
        miThis.newSaleService.saleRequest.venta.isWarranty = true;
        
        miThis.newSaleService.saleRequest.venta.idVendedor = '' + miThis.newSaleService.user.userid;
        miThis.newSaleService.saleRequest.venta.dni = miThis.newSaleService.user.dni;
        if ( miThis.newSaleService.saleRequest.venta.clientePagador === null ) {
          miThis.newSaleService.saleRequest.venta.clientePagador = miThis.newSaleService.saleRequest.venta.cliente;
        }
        const newSaleRequest = miThis.newSaleService.saleRequest;
        miThis.dashboardService.setConfirmSale ( newSaleRequest, true ).subscribe ( ( response: RespuestaVenta ) => {
            miThis.newSaleService.resetSale ();
    
            response.producto = `${miThis.precio}/mes, ${miThis.descripcion}, Plazo - ${miThis.planElegido} años`;
            
            // response.plan = 'plazo ' + miThis.planElegido + ' años';
            miThis.notificationService.showSaleSuccess ( response );
            miThis.dashboardService.respuestaVenta = response;
            miThis.dashboardService.reset ();
            miThis.isLoading = false;
            miThis.dashboardService.setSaleSuccess ( true );
          }, ( error: any ) => {
            miThis.newSaleService.resetSale ();
            //miThis.notificationService.showChangePasswordError();
            miThis.notificationService.mostrarErrorServidor ();
            miThis.dashboardService.reset ();
            miThis.isLoading = false;
            miThis.dashboardService.setSaleSuccess ( true );
            
          }
          /**
           * fin del subcribe
           * manejo de error
           */
        ), ( error: HttpErrorResponse ) => {
          //console.log(error.status);
          //console.log(error.message);
          //console.log(error.statusText);
          miThis.newSaleService.resetSale ();
          miThis.notificationService.mostrarErrorServidor ();
          miThis.dashboardService.reset ();
          miThis.isLoading = false;
          miThis.dashboardService.setSaleSuccess ( true );
        };
        
        //Aca no se puede utilizar this porque rompe, y no se puede hacer return. Por eso cree una variable local
        //para poder guardar la respuesta.
      }
      
    }
    
  }
  
  cargarTarjetaSiEsLoginAutomatico () {
    
    this.payData = this.oauthService.userAuto.pay_data;
    
    this.formularioPago.get ( 'nombreCompletoTitular' ).patchValue ( this.payData.card_holder_name );
    this.formularioPago.get ( 'numDocumento' ).patchValue ( this.payData.number );
    this.formularioPago.get ( 'codigoSeg' ).patchValue ( this.payData.security_code );
    let fecha = this.payData.card_expiration_month.toString () + this.payData.card_expiration_year.toString ();
    this.formularioPago.get ( 'fechaVencimiento' ).patchValue ( fecha );
    let tamagno = this.payData.card_number.length;
    this.formularioPago.get ( 'cardNum1' ).patchValue ( this.payData.card_number.substring ( 0, 4 ) );
    this.formularioPago.get ( 'cardNum2' ).patchValue ( this.payData.card_number.substring ( 4, 8 ) );
    this.formularioPago.get ( 'cardNum3' ).patchValue ( this.payData.card_number.substring ( 8, 12 ) );
    this.formularioPago.get ( 'cardNum4' ).patchValue ( this.payData.card_number.substring ( 12, tamagno ) );
    
    //this.isConfirmado = true;
    this.mes = this.formularioPago.get ("fechaVencimiento").value.substring(0,2);
    this.anio = this.formularioPago.get ("fechaVencimiento").value.substring(2,5);
    this.checkLuhn();
  }
}
