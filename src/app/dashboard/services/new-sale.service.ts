import { Injectable } from '@angular/core';

import { State } from '../dashboard.enums';
import { Producto } from '../models/products/producto.model';
import { WarrantyProduct, WarrantyProductPrice, Product } from '../models/services/services.model';
import { SaleRequest, BaseSale, Client, RespuestaTokenDecidir, EncuestaSale } from '../models/sales/sales.model';
import { UserModel } from "../../shared/models/user.model";
import { CatalogResponse } from "../models/services/services-response.model";

@Injectable ()
export class NewSaleService {
  public idBranch: string;
  public branchName: string;
  public newClient: Client;
  public idVendedor: string;
  public businessLine: string;
  public selectedProduct: Producto;
  public newSaleRequest: SaleRequest;
  public selectedWarranty: WarrantyProduct;
  public saleRequestDetail: any;
  public servicioSinParametros: Boolean = false;
  public precioPrdElegido: string;
  public productoSeleccionado: any;
  public selectedWarrantyPlan: string;
  public respuestaApi: RespuestaTokenDecidir;
  public user: UserModel;
  public provincias: any;
  public localidades: Array<any>;

  constructor () {
    this.createNewSale ();
  }

  private createNewSale () {
    this.newSaleRequest = new SaleRequest ();
    this.newSaleRequest.venta = new BaseSale ();
    this.saleDetail = {};
  }

  set branch ( idBranch: string ) {
    this.idBranch = idBranch;
    this.updateSaleRequest ( 'idSucursal', this.idBranch );
  }

  get branch (): string {
    return this.idBranch;
  }

  set userId ( userId: string ) {
    this.idVendedor = userId;
    this.updateSaleRequest ( 'idVendedor', this.idVendedor );
  }

  get saleRequest (): SaleRequest {
    return Object.assign ( this.newSaleRequest );
  }

  set client ( client: Client ) {
    this.newClient = client;
    this.updateSaleRequest ( 'cliente', this.newClient );
  }

  get client (): Client {
    return Object.assign ( this.newClient );
  }

  set category ( category: string ) {
    this.businessLine = category;
  }

  get category (): string {
    return this.businessLine;
  }

  set product ( product: Producto ) {
    this.selectedProduct = product;
  }

  get product (): Producto {
    return this.selectedProduct;
  }

  set warranty ( warranty: WarrantyProduct ) {
    this.selectedWarranty = warranty;
  }

  get warranty (): WarrantyProduct {
    return this.selectedWarranty;
  }

  set plan ( plan ) {
    if ( plan ) {
      this.saleDetail.idPlanElegido = plan;
    }
  }

  set warrantyPlan ( plan: WarrantyProductPrice ) {
    if ( plan ) {
      this.saleDetail.plazo = decodeURIComponent ( escape ( plan.Plazo ) );
      this.saleDetail.importe = plan.Importe;
    }
  }

  set saleDetail ( saleDetail: any ) {
    this.saleRequestDetail = saleDetail;
    this.updateSaleRequest ( 'detalleSolicitudVenta', this.saleRequestDetail );
  }

  get saleDetail () {
    return this.saleRequestDetail;
  }

  updateSaleRequest ( property: string, value: any, key: string = 'venta' ) {
    this.newSaleRequest[key][property] = value;
  }

  resetSale () {
    this.createNewSale ();
    this.product = null;
    this.warranty = null;
    this.plan = State.empty;
    this.warrantyPlan = null;
    this.businessLine = State.empty;
    this.updateSaleRequest ( 'idSucursal', this.branch );
    this.updateSaleRequest ( 'idVendedor', this.idVendedor );
  }
}
