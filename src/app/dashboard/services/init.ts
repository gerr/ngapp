import { CatalogService } from './catalog.service';
import { NewSaleService } from './new-sale.service';
import { DashboardService } from './dashboard.service';

export const SERVICES = [
  CatalogService,
  NewSaleService,
  DashboardService
];
