import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { of, Observable, throwError as observableThrowError, Subscription, throwError } from 'rxjs';

import {HeaderOption} from '../models/utils/utils.model';
import {environment} from '../../../environments/environment';
import {
  CatalogResponse,
  LineCatalogResponse,
  BrandCatalogResponse,
  SubLineCatalogResponse,
  ProductCatalogResponse,
  ProvinceCatalogResponse,
} from '../models/services/services-response.model';
import {Rango, Rangos, RespuestaRango, Code} from "../models/services/services.model";
import { NewSaleService } from "./new-sale.service";

@Injectable()
export class CatalogService {
  private readonly SUCCESS: number = 200;
  private readonly BASE_URL = environment.BASE_URL;

  private readonly CONTENT_TYPE: HeaderOption = {key: 'Content-Type', value: 'application/json'};

  lineCatalogResponse: LineCatalogResponse;
  brandCatalogResponse: BrandCatalogResponse;
  productCatalogResponse: ProductCatalogResponse;
  subLineCatalogResponse: SubLineCatalogResponse;

  genderCatalogResponse: CatalogResponse;
  civilStatusCatalogResponse: CatalogResponse;
  workRelationCatalogResponse: CatalogResponse;
  personalRelationCatalogResponse: CatalogResponse;
  provinceCatalogResponse: ProvinceCatalogResponse;

  constructor(private http: HttpClient, private newSaleService: NewSaleService) {
  }

  getCivilStatusCatalog(): Observable<CatalogResponse> {
    return this.request('catalogo-estados-civiles', 'civilStatus');
  }

  getWorkRelationsCatalog(): Observable<CatalogResponse> {
    return this.request('catalogo-relaciones-laborales', 'workRelation');
  }

  getGenderCatalog(): Observable<CatalogResponse> {
    return this.request('catalogo-sexos', 'gender');
  }

  getProvinceCatalog(): Observable<ProvinceCatalogResponse> {
    return this.request('catalogo-provincias', 'province');
  }

  getPersonalRelationCatalog(): Observable<CatalogResponse> {
    return this.request('catalogo-parentescos', 'personalRelation');
  }

  getSubLineCatalog(): Observable<SubLineCatalogResponse> {
    //return;
    if (this.subLineCatalogResponse) {
      return of(this.subLineCatalogResponse);
    }

    const headers = this.getHeaders([this.CONTENT_TYPE]);
    //return this.http.get(`${this.API}/catalogo-sublineas`, { headers })
    return this.http.get(`${this.BASE_URL}/catalogo-sublineas`)
      .pipe(
        tap((response: SubLineCatalogResponse) => {
          if (response) {
            if (response.code.code === this.SUCCESS) {
              this.subLineCatalogResponse = response;
            }
          } else {
            this.subLineCatalogResponse = undefined;
          }
        })
      );
  }

  getRangeBySubLineCatalog(cuitSocio: string, precio: number, subLine: string) {
    const fullUrl = `${this.BASE_URL}/catalogo-rango/cuit/${cuitSocio}/precio/${precio}/sublinea/${subLine}`;
    return this.http.get(fullUrl).pipe(
      map((response: any) => {
        if(!environment.production){
          console.log("************************************************* environment.production = ", environment.production);
          console.log("respuesta getRangeBySubLineCatalog(): urlPath => ", fullUrl);
          console.log("************************************************************************** ");
        }
        return response;
      }), catchError( error => {
        if(!environment.production){
            console.log("************************************************* environment.production = ", environment.production);
            console.log("Error al obtener rangos en getRangeBySubLineCatalog(): urlPath => ", fullUrl);
            console.log("************************************************************************** ");
        }
        return throwError('Error al obtener rangos ' + error);
      })
    );
    //return this.mockGetRango();
  }

  getBrandCatalog(): Observable<BrandCatalogResponse> {
    if (this.brandCatalogResponse) {
      return of(this.brandCatalogResponse);
    }

    const headers = this.getHeaders([this.CONTENT_TYPE]);
    return this.http.get(`${this.BASE_URL}/catalogo-marcas/`, {headers})
      .pipe(
        tap((response: BrandCatalogResponse) => {
          if (response.code.code === this.SUCCESS) {
            this.brandCatalogResponse = response;
          }
        })
      );
  }

  getLineCatalog(): Observable<LineCatalogResponse> {
    if (this.lineCatalogResponse) {
      return of(this.lineCatalogResponse);
    }

    const headers = this.getHeaders([this.CONTENT_TYPE]);
    return this.http.get(`${this.BASE_URL}/catalogo-lineas/`, {headers})
      .pipe(
        tap((response: LineCatalogResponse) => {
          if (response.code.code === this.SUCCESS) {
            this.lineCatalogResponse = response;
          }
        })
      );
  }

  getProductCatalog(): Observable<ProductCatalogResponse> {
    if (this.productCatalogResponse) {
      return of(this.productCatalogResponse);
    }

    const headers = this.getHeaders([this.CONTENT_TYPE]);
    return this.http.get(`${this.BASE_URL}/catalogo-productos/`, {headers})
      .pipe(
        tap((response: ProductCatalogResponse) => {
          if (response.code.code === this.SUCCESS) {
            this.productCatalogResponse = response;
          }
        })
      );
  }

  private request(service: string, key: string, method: string = 'get') {
    const attribute = `${key}CatalogResponse`;

    if (this[attribute]) {
      return of(this[attribute]);
    }

    const headers = this.getHeaders([this.CONTENT_TYPE]);

    return this.http[method](`${this.BASE_URL}/${service}`, {headers})
      .pipe(
        tap((response: CatalogResponse) => {
          if (response.code.code === this.SUCCESS) {
            this[attribute] = response;
            if(attribute == 'provinceCatalogResponse'){
              this.newSaleService.provincias = response.body;
            }
          }

        })
      );
  }

  private getHeaders(options?: HeaderOption[]): HttpHeaders {
    let headers = new HttpHeaders({
      user: 'user',
      password: 'password'
    });

    if (options && options.length) {
      options.map(({key, value}: any) => {
        headers = headers.append(key, value);
      });
    }

    return headers;
  }
  
  private mockGetRango () {
    let respuestaRango = new RespuestaRango();
    let rangos:Array<Rango> = [];
    rangos.push( new Rango("580", "des") );
    return  of({ code: {code: 200, descripcion: "descr"}, body: {rangos: rangos} });
  }
}
