import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {from, of, Subject, Observable} from 'rxjs';
import {catchError, filter, tap} from 'rxjs/operators';
import {map} from 'rxjs/operators';
import {
  Cancelacion,
  Client,
  MotivoCancelacion,
  RespuestaCancelacion,
  Sucursal,
  SucursalGuardar
} from '../models/sales/sales.model';
import {environment} from '../../../environments/environment';
import {GetClientResponse, GetProductsResponse} from '../models/services/services-response.model';
import {JsonParaFiltrar, Product, Range, RespuestaSucursales} from '../models/services/services.model';
import {SaleRequest} from '../models/sales/sales.model';
import {StateSale, State} from '../dashboard.enums';
import {NewSaleService} from './new-sale.service';
import {OAuthService} from '../../shared/services/oauth.service';
import {RespuestaVenta} from '../models/ventas-realizadas/respuesta-venta.model';
import {PayData, UserModel} from '../../shared/models/user.model';
import {Producto} from '../models/products/producto.model';
import {SortPipe} from '../../shared/pipes/sort.pipe';
import {Form} from '@angular/forms';
import {RespuestaGenerica} from '../models/respuesta-generica';
import {SolicitudAdhesion} from '../../shared/models/SolicitudAdhesion.model';
import {error} from 'util';
import {forEach} from '@angular/router/src/utils/collection';

@Injectable()
export class DashboardService {
  private readonly BASE_URL = environment.BASE_URL;
  //private readonly CLIENTE_CARRE = environment.CLIENTE_CARRE;
  public respuestaVenta: RespuestaVenta;
  productsSubs: any;
  hiddenActivado = false;
  public isSaleCancel = false;
  // id: multi-clientes
  clientData: Array<Client> = [];
  user: UserModel;
  productList: Array<any>;
  contentType: Object = {
    key: 'Content-Type',
    value: 'application/json'
  };

  public isSaleSuccess = new Subject<boolean>();
  private respuesRxjs: Observable<any>;

  constructor(private sortPipe: SortPipe, private http: HttpClient, private newSaleService: NewSaleService, private userService: OAuthService) {
    //carga de datos del usuario al servicio de newSale
    this.newSaleService.user = this.userService.userData;
    if (!environment.production || !environment.testing) {
      console.log('************************************************* environment.production = ', environment.production);
      console.log('************************************************* environment.production = ', environment.testing);
      console.log('hardcodeado tiene_solicitud_adhesion: ');
      //this.newSaleService.user.tieneSolicitud = true;
      //this.newSaleService.user.tiene_sps = true;
      console.log('************************************************************************** ');
    }

  }


  searchClient(documentType: string, documentNumber: string, idGenero?: string) {
    const tipoDocumento = 'tipo-documento/' + documentType;
    const numeroDocumento = 'numero-documento/' + documentNumber;
    const genero = 'genero/' + idGenero;
    const cuitSocio = 'cuit-socio/' + this.userService.userData.cuit;
    let fullUrl: string;
    fullUrl = `${this.BASE_URL}/clientes/${tipoDocumento}/${numeroDocumento}/${genero}/${cuitSocio}`;
    // id: multiples-clientes
    let headers = new HttpHeaders();
    headers = headers.append('user', 'user');
    headers = headers.append('password', 'password');

    return this.http.get(fullUrl)
      .pipe(
        tap(({status, cliente, clientes}: GetClientResponse) => {
          if (cliente) {
            cliente.fechaNacimiento = this.formatDate(cliente.fechaNacimiento);
          } else if (clientes) {
            clientes.forEach(elem => {
              if (elem != null && status != null && status.code === 200) {
                if (elem.fechaNacimiento) {
                  elem.fechaNacimiento = this.formatDate(elem.fechaNacimiento);
                }
              }
            });
          }
        }),
        map(res => {
          if (res.status.code != 200) {
            res.cliente = new Client();
            res.cliente.nroDocumento = documentNumber;
            res.cliente.tipoDocumento = documentType;
            res.cliente.idGenero = idGenero;
          }
          return res;
        })
      );
  }

  formatDate(date: string) {
    return date.includes('-') ? date.split('-').reverse().join('/') : date;
  }

  public formatDate2(date: string) {
    return date.includes('/') ? date.split('/').reverse().join('-') : date;
  }

  getProducts(cuitSocio: string) {
    const fullUrl = `${ this.BASE_URL }/productos/cuit-socio/${ cuitSocio }`;
    return this.http.get(fullUrl).pipe(
      tap((response: GetProductsResponse) => {
        let res = response.body.productos;
        this.productList = this.sortPipe.transform(res, 'descripcion_mkt', true);
      })
    );
  }

  getMotivosCancelacion() {
    const urlMotivos = `${this.BASE_URL}/motivo_anulacion`;
    return this.http.get(urlMotivos).pipe(
      tap((response: any[]) => {
        return response;
      })
    );
  }

  anularVentaDelDia(cancelacionBody: Cancelacion) {
    const urlCancelacion = `${this.BASE_URL}/ventas/cancelar`;
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    return this.http.post(urlCancelacion, cancelacionBody, {headers}).pipe(
      map((response: RespuestaCancelacion) => {
        return response;
      })
    );
  }


  getSales(userId: string, filtroItems?: JsonParaFiltrar, length?: string) {

    const serviceUrl = 'ventas/obtener-reporte/vendedor';
    /*    if (!length) {
          length = '200';
        }*/
    let fullUrl = `${this.BASE_URL}/${serviceUrl}`;
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    //let filtro = JSON.stringify(filtroItems);
    return this.http.post(fullUrl, filtroItems, {headers});
  }


  SetNewPassword(oldPass: string, newPass: string, email: string, userId: string) {
    const serviceUrl = '/usuarios/change-password';
    const fullUrl = this.BASE_URL + serviceUrl;

    const body = {
      oldPassword: oldPass,
      newPassword: newPass,
      email: email,
      userId: userId
    };
    let headers = new HttpHeaders();
    headers = headers.append('user', 'user');
    headers = headers.append('password', 'password');
    headers = headers.append('Content-Type', 'application/json');

    return this.http.post(fullUrl, body, {headers, observe: 'response'});
  }

  getConfirmationMethod(company: string) {
    const serviceUrl = '/params/nombre_parametro/validaConfirmacionVenta/id_cliente/';
    const fullUrl = `${this.BASE_URL}/${serviceUrl}/${company}/`;

    let headers = new HttpHeaders();
    headers = headers.append('user', 'user');
    headers = headers.append('password', 'password');
    headers = headers.append('Content-Type', 'application/json');

    return this.http.get(fullUrl, {headers});
  }

  saveCanceledSale(saleRequest: SaleRequest, reason: string) {
    saleRequest.venta.motivoCancelacion = reason;
    const exists = saleRequest.venta.detalleSolicitudVenta
      && Object.keys(saleRequest.venta.detalleSolicitudVenta).length;

    if (!exists) {
      saleRequest.venta.detalleSolicitudVenta = {
        idProductoElegido: State.empty,
        descripcionProductoElegido: 'No seleccionado'
      };
    }

    return this.setConfirmSale(saleRequest, false);
  }

  setConfirmSale(saleRequest: SaleRequest, completed?: boolean) {
    const fullUrl = `${this.BASE_URL}/ventas/guardar`;
    const headers = this.getHeaders([this.contentType]);
    saleRequest.venta.idEstado = completed ? StateSale.PENDING : StateSale.CANCELED;
    saleRequest.venta.motivoCancelacion =
      saleRequest.venta.motivoCancelacion ? saleRequest.venta.motivoCancelacion : State.empty;
    const body = saleRequest;

    const fecha = this.formatDate2(saleRequest.venta.cliente.fechaNacimiento);
    saleRequest.venta.cliente.fechaNacimiento = fecha;
    saleRequest.venta.cuit = this.userService.userData.cuit;

    if (!environment.production) {
      console.log('************************************************* environment.production = ', environment.production);
      console.log('VENTA COMPLETA: =================================================================');
      console.log(JSON.stringify(body));
      console.log('************************************************************************** ');
    }

    return this.http.post(fullUrl, body, {headers});
  }


  getWarrantyProduct(cuitSocio: string, idPrd: string) {
    const fullUrl = `${this.BASE_URL}/productos/garantias/cuit/${cuitSocio}/idProducto/${idPrd}`;
    return this.http.get(fullUrl);
  }


  reset() {
    // id: multi-clientes
    this.clientData = [];
    return of(true);
  }

  getClientData() {
    return of(this.clientData);
  }

  getProductList() {
    return of(this.productList);
  }

  getSaleSuccess(): Observable<any> {
    return this.isSaleSuccess;
  }

  setSaleSuccess(isSuccess: boolean) {
    this.isSaleSuccess.next(isSuccess);
  }

  private getHeaders(options?: Array<any>) {
    let headers = new HttpHeaders({user: 'user', password: 'password'});

    if (options && options.length) {
      options.map(({key, value}: any) => {
        headers = headers.append(key, value);
      });
    }

    return headers;
  }

  getSupervisor() {
    const serviceUrl = 'usuarios/supervisor/todos';
    let fullUrl = `${this.BASE_URL}/${serviceUrl}`;
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    return this.http.get(fullUrl);
  }


  getVendedores() {
    const serviceUrl = 'usuarios/vendedor/todos';
    let fullUrl = `${this.BASE_URL}/${serviceUrl}`;
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    return this.http.get(fullUrl);
  }

  getPopUpUrl() {
    let cuit = this.userService.userData.cuit;
    let url: string = `${this.BASE_URL}/params/nombre_parametro/POPUP/id_cliente/${cuit}`;
    return this.http.get(url)
      .pipe(
        tap((response: any) => {
          //console.log(response.valor);
          return response.valor;
        }));
  }

  setSucursal(sucursalGuardar: SucursalGuardar) {
    const url = `${this.BASE_URL}/sucursal/guardar`;
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    return this.http.post(url, sucursalGuardar, {headers}).pipe(
      tap((response: any) => {
        return response;
      })
    );
  }

  getSucursales() {
    const url = `${this.BASE_URL}/sucursal/sucursales/${this.userService.userData.cuit}`;
    //console.log("url", url);
    return this.http.get(url).pipe(
      tap((response: Sucursal[]) => {
        return response;
      })
    );
  }

  postFile(sucursales: Sucursal[]) {
    const url = `${this.BASE_URL}/sucursal/carga`;
    let headers = new HttpHeaders();
    // console.log("JSON.stringify(sucursales)");
    //console.log(JSON.stringify(sucursales));
    headers = headers.append('Content-Type', 'application/json');

    return this.http
      .post(url, sucursales, {headers})
      .pipe(
        map((data: Sucursal[]) => {
          //console.log("Respuesta de carga masiva");
          //console.log(JSON.stringify(data));
          return data;
        })
        /*        catchError((error: any, result?: T) => {
                  console.log(error);
                  return false;
                })*/
      );
  }

  /**
   * mensajeria enviar SMS sms
   * @param telefono
   * @constructor
   */
  SendMessage(telefono: any) {
    let documento = this.newSaleService.client.nroDocumento;
    const fullUrl = `${this.BASE_URL}/ventas/valida_sms/${documento}/${telefono}/isSms`;
    const headers = this.getHeaders([this.contentType]);
    return this.http.get(fullUrl, {headers});

    //Mock
    //return this.httpGetMockSendMessage();
  }

  httpGetMockSendMessage() {
    if (!environment.production) {
      console.log('************************************************* environment.production = ', environment.production);
      console.log('httpGetMockSendMessage: True');
      console.log('************************************************************************** ');
    }
    let respuestaMock = new RespuestaGenerica();
    respuestaMock.code.code = 200;
    respuestaMock.code.description = 'OK';
    respuestaMock.body = 'OK';
    respuestaMock.respuesta = 'OK';
    respuestaMock.mensaje = 'OK';
    return of(respuestaMock);
  }


  getConfirmationCode(codigo: number) {
    let documento = this.newSaleService.client.nroDocumento;
    //const fullUrl = `${this.BASE_URL}/ventas/obtener-numero-confirmacion/${codigo}/${documento}/isSms`;
    const fullUrl = `${this.BASE_URL}/ventas/validar-numero-confirmacion/${codigo}/${documento}`;
    const headers = this.getHeaders([this.contentType]);
    return this.http.get(fullUrl, {headers});

    //Mock
    //return this.httpGetMockGetConfirmationCode(codigo);


  }


  private httpGetMockGetConfirmationCode(codigo) {
    if (!environment.production) {
      console.log('************************************************* environment.production = ', environment.production);
      console.log('httpGetMockGetConfirmationCode: True');
      console.log('************************************************************************** ');
    }
    let respuestaMock = new RespuestaGenerica();
    if (codigo == 3211) {
      respuestaMock.code.code = 200;
      respuestaMock.code.description = 'OK';
      let bodi = {
        estado: 0,
        mensaje: 'msj'
      };
      respuestaMock.body = JSON.stringify(bodi);
      respuestaMock.respuesta = 'OK';
      respuestaMock.mensaje = 'OK';
    } else {
      respuestaMock.code.code = 200;
      respuestaMock.code.description = 'OK';
      let bodi = {
        estado: -1,
        mensaje: 'msj'
      };
      respuestaMock.body = JSON.stringify(bodi);
      respuestaMock.respuesta = 'OK';
      respuestaMock.mensaje = 'OK';
    }
    return from([respuestaMock]);
  }

  getSolicitudAdhesion2(idVenta) {
    let url = `${this.BASE_URL}/reporte/get_formulario/${idVenta}`;
    return this.http.get(url, {responseType: 'blob'}).pipe(
      map((response: any) => {
        return response;
      })
    );

  }

  getSolicitudAdhesion(data: SolicitudAdhesion) {
    let url = `${this.BASE_URL}/reporte/get_formulario`;
    return this.http.post(url, data, {responseType: 'blob'}).pipe(
      map((res: any) => {
        return res;
      })
    );


  }

}
