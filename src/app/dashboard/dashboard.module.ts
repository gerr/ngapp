import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgxCleaveDirectiveModule } from 'ngx-cleave-directive';

import { COMPONENTS } from './init';
import { PIPES } from './pipes/init';
import { DIRECTIVES } from './directives/_directives';
import { SharedModule } from '../shared/shared.module';
import { PRODUCTS_TEMPLATES } from './components/products/init';
import { DashboardRoutingModule } from './dashboard-routing.module';

import { SERVICES } from './services/init';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import {NgxPaginationModule} from 'ngx-pagination';
import {NgSelectizeModule} from 'ng-selectize';
import {NgDatepickerModule} from 'ng2-datepicker';
import { PagoConTarjetaComponent } from './components/pago-con-tarjeta/pago-con-tarjeta.component';
import { CancelarVentaComponent } from './components/cancelar-venta/cancelar-venta.component';
import { PopUpComponent } from './modals/pop-up/pop-up.component';
import {AbmSucursalesComponent} from "./modals/abm-sucursales/abm-sucursales.component";
import {Angular2CsvModule} from "angular2-csv";
import { DashboardReportesComponent } from './modals/dashboard-reportes/dashboard-reportes.component';
library.add(fas);

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    NgxCleaveDirectiveModule,
    FormsModule,
    SharedModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgSelectizeModule,
    NgDatepickerModule,
    Angular2CsvModule,
    BrowserAnimationsModule
  ],
  declarations: [...COMPONENTS, ...DIRECTIVES, ...PIPES, PagoConTarjetaComponent, CancelarVentaComponent, PopUpComponent, AbmSucursalesComponent, DashboardReportesComponent],
  entryComponents: [...PRODUCTS_TEMPLATES],
  providers: [ ...SERVICES ],
})
export class DashboardModule {}
