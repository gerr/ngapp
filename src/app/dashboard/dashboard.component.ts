import { Component, OnInit } from '@angular/core';

import { Title } from '@angular/platform-browser';
import { OAuthService } from '../shared/services/oauth.service';
import { UserModel } from '../shared/models/user.model';
import { DashboardService } from './services/dashboard.service';
import { animate, state, style, transition, trigger } from "@angular/animations";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  user: UserModel;
  isActive: boolean;
  title: string;
  public state: string = 'inactive';
  constructor(private titleService: Title,
              private oAuthService: OAuthService,
              private dashBoardService: DashboardService) {}

  ngOnInit() {
    this.title = 'Ventas - Portal Socios';
    this.oAuthService.getUserData().subscribe((user: UserModel) => {
      this.user = user;
      //si tiene login automatico saltar chooseBranch
      this.oAuthService.isLoginAuto ? this.isActive = false : this.isActive = true;
      this.titleService.setTitle(this.title);
    });
    this.dashBoardService.productsSubs = this.dashBoardService.getProducts(this.user.cuit).subscribe();
    //this.dashBoardService.productsSubs = this.dashBoardService.productList;
  }

  closeModal(status: boolean) {
    this.isActive = !status;
  }
}
