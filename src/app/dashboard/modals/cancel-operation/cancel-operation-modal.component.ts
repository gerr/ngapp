import { Component, OnInit, Input, Output, EventEmitter, ViewChild, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Reason } from '../../models/utils/utils.model';
import { NewSaleService } from '../../services/new-sale.service';
import { DashboardService } from '../../services/dashboard.service';
import {OAuthService} from '../../../shared/services/oauth.service';

@Component({
  selector: 'app-cancel-operation-modal',
  templateUrl: './cancel-operation-modal.component.html',
  styleUrls: ['./cancel-operation-modal.component.scss']
})
export class CancelOperationModalComponent implements OnInit, OnDestroy {
  title: string;
  question: string;
  reasons: Reason[];
  isLoading: boolean;
  cancelOpSubscription: Subscription;
  phones: Array<string>;

  @Input() isActive: Boolean = false;

  @Output() closeModal: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() resetProcess: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('f') form: any;

  constructor(private newSaleService: NewSaleService,
              private dashboardService: DashboardService,
              private userService: OAuthService) {}

  ngOnInit() {
    this.isLoading = false;
    this.title = 'Cancelar operación';
    this.question = 'Seleccione el motivo por el cual desea cancelar la venta:';
    this.reasons = [
      { id: 1, description: 'Temas económicos.'},
      { id: 2, description: 'No necesita la cobertura.'},
      { id: 3, description: 'No quiero brindar los datos de medio de pago.'},
      { id: 4, description: 'Ya tiene un producto similar con la competencia.'},
      { id: 5, description: 'No brinda motivo.'}
    ];
  }

  ngOnDestroy() {
    if (this.cancelOpSubscription) {
      this.cancelOpSubscription.unsubscribe();
    }
  }

  handleCloseModal() {
    this.form.resetForm();
    this.closeModal.emit(false);
  }

  onSubmit() {

    const { nombre, apellido, celular1, celular2, celular3, telefonoFijo1, telefonoFijo2, telefonoFijo3 } = this.newSaleService.client;
    this.phones = [];
    if (celular1) {
      const celularLimpio = this.limpiarCelular(celular1);
      this.newSaleService.client.celular1 = celularLimpio;
      this.phones.push(celularLimpio);
    }

    if (celular2) {
      const celularLimpio = this.limpiarCelular(celular2);
      this.newSaleService.client.celular2 = celularLimpio;
      this.phones.push(celularLimpio);
    }

    if (celular3) {
      const celularLimpio = this.limpiarCelular(celular3);
      this.newSaleService.client.celular3 = celularLimpio;
      this.phones.push(celularLimpio);
    }

    if (celular3) {
      const celularLimpio = this.limpiarCelular(celular3);
      this.newSaleService.client.celular3 = celularLimpio;
      this.phones.push(celularLimpio);
    }

    if (telefonoFijo1) {
      const celularLimpio = this.limpiarCelular(telefonoFijo1);
      this.newSaleService.client.telefonoFijo1 = celularLimpio;
      this.phones.push(telefonoFijo1);
    }

    if (telefonoFijo2) {
      const celularLimpio = this.limpiarCelular(telefonoFijo2);
      this.newSaleService.client.telefonoFijo2 = celularLimpio;
      this.phones.push(telefonoFijo2);
    }

    if (telefonoFijo3) {
      const celularLimpio = this.limpiarCelular(telefonoFijo3);
      this.newSaleService.client.telefonoFijo3 = celularLimpio;
      this.phones.push(telefonoFijo3);
    }
    if (this.form.valid) {
      this.cancelOperation();
    }
  }

  limpiarCelular(celular:string){

    let celularLimpio = celular.replace("-", "");
    let celularLimpio2 = celularLimpio.replace("-", "");
    let celularLimpio3 = celularLimpio2.replace("-", "");

    let celularLimpio4 = celularLimpio3.replace(".", "");
    let celularLimpio5 = celularLimpio4.replace(".", "");
    let celularLimpio6 = celularLimpio5.replace(".", "");

    let celularLimpio7 = celularLimpio6.replace(" ", "");
    let celularLimpio8 = celularLimpio7.replace(" ", "");
    let celularLimpio9 = celularLimpio8.replace(" ", "");


    let tam = celularLimpio9.length;
    let celu = celularLimpio9.substr(tam - 8, tam );

    return celu;
   }

  private cancelOperation() {
    const { anwser: id } = this.form.value;
    const { description }: Reason = this.reasons.filter(item => item.id === id)[0];
    //this.newSaleService.saleRequest.venta.idVendedor =

    this.newSaleService.saleRequest.venta.idVendedor = '' + this.userService.userData.userid;

    //console.log("datos de cancelacion: " + this.newSaleService.saleRequest.venta.idVendedor);
    const saleService = this.newSaleService.saleRequest;
    //console.log(saleService);
    this.isLoading = true;

    this.cancelOpSubscription = this.dashboardService
      .saveCanceledSale(saleService, id).subscribe((response: any) => {
        this.newSaleService.resetSale();
        this.form.resetForm();
        this.resetProcess.emit(true);
        this.handleCloseModal();
        this.isLoading = false;
        this.dashboardService.isSaleCancel = true;
      });
  }
}
