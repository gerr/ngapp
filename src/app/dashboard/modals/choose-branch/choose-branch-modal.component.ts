import {Component, OnInit, Input, Output, EventEmitter, AfterViewInit, ChangeDetectorRef} from '@angular/core';

import {UserModel} from '../../../shared/models/user.model';
import {NewSaleService} from '../../services/new-sale.service';
import {CatalogueService} from '../../../shared/services/catalogue.service';
import {OAuthService} from '../../../shared/services/oauth.service';
import {BranchesResponse, PuntoVenta} from '../../../shared/models/catalogue-models/branches-response.model';
import {DashboardService} from '../../services/dashboard.service';
import {State} from '../../dashboard.enums';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {timeout} from 'rxjs/operators';
import {SortPipe} from '../../../shared/pipes/sort.pipe';


@Component({
  selector: 'app-choose-branch-modal',
  templateUrl: './choose-branch-modal.component.html',
  styleUrls: ['./choose-branch-modal.component.scss'],
  animations: [
    trigger('animacion', [
      state('inactive', style({
        opacity: '0'
      })),
      state('active', style({
        opacity: '1'
      })),
      transition('inactive => active', animate('600ms ease-in')),
      transition('active => inactive', animate('600ms ease-out')),
    ])
  ]
})
export class ChooseBranchModalComponent implements OnInit, AfterViewInit {
  title: string;
  detail: string;
  branchList: Array<PuntoVenta> = [];
  model: any = {branch: ''};
  isActivePopUp: boolean = false;
  @Input() isActive: boolean;
  @Input() user: UserModel = null;
  @Output() next: EventEmitter<any> = new EventEmitter<any>();

  localstorage = localStorage;
  urlPopUp: string;

  @Output() closeModal: EventEmitter<boolean> = new EventEmitter<boolean>();
  estado = '';

  constructor(private catalogueService: CatalogueService,
              private dashboardService: DashboardService,
              private newSaleService: NewSaleService,
              private userService: OAuthService,
              private sortPipe: SortPipe,
              private cdRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.estado = 'inactive';
    this.branchList = [];
    this.newSaleService.saleRequest.venta.clientePagador = null;
    //si tiene login automatico salta el paso este.
    if (this.userService.isLoginAuto) {
      this.onSubmit(1);
      // id:multi-clientes
      this.dashboardService.clientData.push(this.userService.userAuto.client);
      this.newSaleService.client = this.userService.userAuto.client;
    } else {
      this.title = `¡Bienvenido ${this.user.firstName}!`;
      this.detail = `
      Para empezar a vender es necesario que selecciones el código
      de la sucursal donde te encuentras: * `;
      this.catalogueService.getBranches(this.user.cuit).subscribe(response => {
        this.branchList = response.body;
        this.sortPipe.transform(this.branchList, 'descripcion_punto_venta', true);
      });
    }

  }


  mostrarPopUp($event) {
    this.isActivePopUp = $event;
  }

  onSubmit({branch}: any) {
    //this.animar();
    const momento = new Date().getDate().toString();
    const dia = this.localstorage.getItem('now');
    if (dia == momento) {
      this.mostrarPopUp(false);
    } else {
      this.dashboardService.getPopUpUrl().subscribe(response => {
        this.urlPopUp = response.valor;
        if (this.urlPopUp) {
          this.localstorage.setItem('now', momento);
          this.mostrarPopUp(true);
        }
      });
    }

    this.newSaleService.branch = branch;
    this.animar();
    setTimeout(() => {
      this.closeModal.emit(true);
    }, 700);
  }

  animar() {
    this.estado = this.estado === 'active' ? 'inactive' : 'active';
  }

  ngAfterViewInit() {
    this.animar();
    this.cdRef.detectChanges();
  }
}
