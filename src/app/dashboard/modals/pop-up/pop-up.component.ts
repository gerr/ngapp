import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {tap} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {OAuthService} from "../../../shared/services/oauth.service";
import {el} from "@angular/platform-browser/testing/src/browser_util";
import {DashboardService} from "../../services/dashboard.service";

@Component({
  selector: 'app-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.scss']
})
export class PopUpComponent implements OnInit {
  isActive: boolean = false;
  @Input() urlPopUp: string = '';
  @Output() next: EventEmitter<any> = new EventEmitter<any>();
  constructor(
    private http: HttpClient,
    private userService: OAuthService) {

  }

  ngOnInit() {

  }



  mostrarPopUp( estado: boolean ) {
    this.next.emit(estado);
  }
}
