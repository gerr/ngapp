import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Risk } from '../../models/utils/utils.model';

@Component({
  selector: 'app-risk-detail-modal',
  templateUrl: './risk-detail-modal.component.html',
  styleUrls: ['./risk-detail-modal.component.scss']
})
export class RiskDetailModalComponent implements OnInit {
  @Input() risk: Risk;
  @Input() isActive: Boolean = false;

  @Output() closeModal: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {}

  ngOnInit() {}

  handleCloseModal() {
    this.closeModal.emit(false);
  }
}
