import {Component, OnInit, ViewChild, ElementRef, Input} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {ExportCsv, Sucursal, SucursalGuardar} from '../../models/sales/sales.model';
import {SortPipe} from '../../../shared/pipes/sort.pipe';
import {ExcelService} from '../../../shared/services/excel.service';
import {
  CURRENT_OPTIONS_CONFIG,
  CURRENT_OPTIONS_CONFIG_SUCURSALES,
  ObjetoFiltro
} from '../../models/services/services.model';
import {Supervisor} from '../../models/products/producto.model';
import {DashboardService} from '../../services/dashboard.service';
import {State} from '../../dashboard.enums';
import {OAuthService} from '../../../shared/services/oauth.service';

@Component({
  selector: 'app-amb-sucursales',
  templateUrl: './abm-sucursales.component.html',
  styleUrls: ['./abm-sucursales.component.scss']
})

export class AbmSucursalesComponent implements OnInit {

  options = {
    fieldSeparator: ';',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: false,
    headers: ['Id', 'Descripcion', 'DNI Supervisor', 'Estado', 'Detalle'],
    showTitle: false,
    title: 'sucursales',
    filename: 'sucursales',
    useBom: false,
    removeNewLines: true,
    keys: ['id', 'descripcion', 'dniSupervisor', 'estado', 'detalle']
  };

  template = {
    fieldSeparator: ';',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: false,
    headers: ['Id', 'Descripcion', 'DNI Supervisor'],
    showTitle: false,
    title: 'template',
    filename: 'template',
    useBom: false,
    removeNewLines: true,
    keys: ['id', 'descripcion', 'dniSupervisor']
  };


  descripcionActual: any;
  fileToUpload: File = null;
  dniSupervisorActual: any;
  $dniSupervisor: ObjetoFiltro[] = [];
  sucursalesConError: SucursalGuardar[] = [];
  sucursalesCorrectas: SucursalGuardar[] = [];
  nombreSupervisorActual: any;
  $supervisor: any;

  page: number = 1;
  isOpen: boolean = false;
  icon: any = 'plus';
  sucursales: Array<Sucursal> = [];
  id: boolean = false;
  descripcion: boolean = false;
  dniSuper: boolean = false;
  nombreSuper: boolean = false;
  estado: boolean = true;
  field: string;
  fields: Array<any>;
  sucursalRespuesta: Sucursal = new Sucursal();

  $supervisores: Array<ObjetoFiltro> = new Array<ObjetoFiltro>();
  supervisores: Array<Supervisor> = [];

  mostrarAgregarSucu: boolean = false;
  currentOptionsConfig = CURRENT_OPTIONS_CONFIG_SUCURSALES;
  sucursalEditar: Sucursal = new Sucursal();
  sucursalGuardar: SucursalGuardar = new SucursalGuardar();
  sinEditar: boolean = false;
  formulario: FormGroup;
  editar: boolean = false;
  mostrarImportarArchivo: boolean = false;
  cargaExitosa: boolean = false;
  habilitarBotonSubirArchivo: boolean = false;
  habilitarBotonGuardar: boolean = false;
  puntoVentaActual: number;
  cuit: string = '';
  cambiarEstado: boolean = false;
  archivo: any;
  sucursalesParaGuardar: Array<Sucursal> = new Array<Sucursal>();
  msjSucursalesCargadas: boolean = false;
  cantSucusExitosas: number = 0;
  cantSucusErroneas: number = 0;
  exportCvsData: ExportCsv[] = [];

  constructor(private fb: FormBuilder, private sortPipe: SortPipe, private excelService: ExcelService, private dashboardService: DashboardService, private userService: OAuthService) {
  }

  ngOnInit(): void {
    this.cuit = this.userService.userData.cuit;
    this.createForm();
    this.fields = [
      {key: 'id', text: 'id'},
      {key: 'descripcion', text: 'Descripcion'},
      {key: 'dniSuper', text: 'DNI Supervisor'},
      {key: 'nombreSuper', text: 'Nombre Supervisor'}
    ];
    this.field = this.fields[1].key;

    this.getSucursales();

  }

  getSucursales() {
    this.dashboardService.getSucursales().subscribe((response: Sucursal[]) => {
      /*      response.forEach((item) => {
              this.sucursales.push(item);
            });*/
      this.sucursales = response;
      this.sucursales.push(this.sucursalRespuesta);
      //console.log("this.sucursales");
      //console.log(this.sucursales);
      this.getListaSupervisores();
    });
  }

  getListaSupervisores() {
    this.sucursales.forEach((item) => {
      if (item.idUsuario != null) {
        this.$supervisores.push(
          new ObjetoFiltro(item.nombreApellido + ' - ' + item.numeroDocumento, '' + item.idUsuario, item.idUsuario)
        );
      }
    });


    this.dashboardService.getSupervisor().subscribe((response: Supervisor[]) => {
      //console.log("entro en getSupervisores");
      this.supervisores = [];
      this.supervisores = response;
      response.forEach((item) => {
        this.$supervisores.push(
          new ObjetoFiltro(item.nombreApellido + ' - ' + item.nroDocumento, '' + item.id, item.id)
        );
      });
      this.$supervisores = this.removeDuplicates(this.$supervisores, 'code');
    });

  }

  toggleView() {
    this.isOpen = !this.isOpen;
    this.icon = this.isOpen ? 'minus' : 'plus';
  }

  //ordena reportes por fecha al inicio y al exportar. claves: ordenamiento - ordenar - exportar ordenado
  orderBy(data: Sucursal[], isAsc: boolean = false) {
    this.sortPipe.transform(data, this.field, isAsc);
  }

  private decode(text: string) {
    try {
      return decodeURIComponent(escape(text).toLocaleUpperCase());
    } catch (e) {
      return text;
    }
  }

  ordenar(thSeleccionado: string, elItem: any, estado: boolean) {
    //console.log("Item seleccionado");
    //console.log(elItem.id);
    let titulos: any = document.getElementsByClassName('items');
    for (let it of titulos) {
      //it.classList.add('fa');
      it.classList.remove('fa-caret-up');
      it.classList.remove('fa-caret-down');
      if (estado) {
        elItem.classList.add('fa-caret-down');

      } else {
        elItem.classList.add('fa-caret-up');
      }

    }
    //estado = !estado;
    const strings = this.sortPipe.transform(this.sucursales, thSeleccionado, estado).map(
      (v: Sucursal, i) =>
        [
          v.numeroPuntoVenta ? v.numeroPuntoVenta.toString() : '-',
          v.descripcion ? v.descripcion.toString() : '-',
          v.numeroDocumento ? v.numeroDocumento.toString() : '-',
          v.nombreApellido ? v.nombreApellido.toString() : '-',
          v.nombreApellido ? v.estado.toString() : '-',
        ]
    );
  }

  exportData() {
    let strings = [];
    strings = this.sortPipe.transform(this.sucursales, this.field, false).map(
      (v: Sucursal, i) => {
        return {
          sucursalId: v.numeroPuntoVenta ? v.numeroPuntoVenta.toString().toUpperCase() : '-',
          descripcion: v.descripcion ? v.descripcion.toString().toUpperCase() : '-',
          dniSupervisor: v.numeroDocumento ? v.numeroDocumento.toString().toUpperCase() : '-',
          nombre: v.nombreApellido ? v.nombreApellido.toString().toUpperCase() : '-'
        };
      }
    );

    this.excelService.exportAsExcelFile(
      strings
      , 'Sucursales');
  }


  importarArchivo() {
    this.mostrarImportarArchivo = !this.mostrarImportarArchivo;

  }

  editarSucursal(id: number) {
    this.editar = true;
    this.habilitarBotonGuardar = false;
    this.mostrarAgregarSucu = !this.mostrarAgregarSucu;
    this.sucursalEditar = this.sucursales.find(item => {
      return item.numeroPuntoVenta == id;
    });
    this.descripcionActual = this.sucursalEditar.descripcion.trim();
    this.dniSupervisorActual = this.sucursalEditar.numeroDocumento;
    this.nombreSupervisorActual = this.sucursalEditar.nombreApellido;
    this.puntoVentaActual = this.sucursalEditar.numeroPuntoVenta;
    this.$supervisor = this.sucursalEditar.idUsuario;
  }

  guardarCambios(value?: string) {
    if (value == 'editar') {
      //cambiar estado de la sucursal.
      this.sucursalGuardar.id = this.puntoVentaActual;
      this.sucursalGuardar.descripcion = this.descripcionActual;
      this.sucursalGuardar.idSupervisor = this.$supervisor;
      this.sucursalGuardar.cuit = this.cuit;
      this.sucursalGuardar.activo = this.sucursalEditar.estado;
    } else if (value == 'cambiar') {
      //cambiar estado de la sucursal.
      this.sucursalGuardar.id = this.sucursalEditar.numeroPuntoVenta;
      this.sucursalGuardar.descripcion = this.sucursalEditar.descripcion;
      this.sucursalGuardar.idSupervisor = this.sucursalEditar.idUsuario;
      this.sucursalGuardar.cuit = this.cuit;
      this.sucursalGuardar.activo = !this.sucursalEditar.estado;
    } else {
      //guardar o editar sucursal
      let idSupervisorActual = '';
      this.$supervisor ? idSupervisorActual = this.$supervisor.toString() : idSupervisorActual = undefined;
      this.sucursalGuardar.id = this.puntoVentaActual;
      this.sucursalGuardar.descripcion = this.sucuDescripcion.value;
      this.sucursalGuardar.idSupervisor = parseInt(idSupervisorActual);
      this.sucursalGuardar.cuit = this.userService.userData.cuit;
      this.sucursalGuardar.activo = true;
    }

    //console.log(JSON.stringify(this.sucursalGuardar));
    this.dashboardService.setSucursal(this.sucursalGuardar).subscribe((data: any) => {
        this.sucursalRespuesta = data;
        //console.log("sucursalRespuesta: -----------------------------------");
        //console.log(this.sucursalRespuesta);
        this.mostrarAgregarSucu = false;
        this.limpiar();
        this.sucursales = new Array<Sucursal>();
        this.getSucursales();
      }
    );

  }

  cancelar() {
    this.mostrarAgregarSucu = !this.mostrarAgregarSucu;
    this.limpiar();
  }

  limpiar() {
    this.sucursalEditar = null;
    this.descripcionActual = '';
    this.nombreSupervisorActual = '';
    this.dniSupervisorActual = null;
    this.sucursalEditar = null;
    this.$supervisor = null;
    this.editar = false;
    this.sucursalGuardar = new SucursalGuardar();
    this.sucuPuntoVenta.reset();
    this.sucuDescripcion.reset();
  }

  createForm() {
    this.formulario = this.fb.group({
      sucuDescripcion: new FormControl(''),
      puntoVenta: new FormControl(''),
      sucuNombreSuper: new FormControl('')
    });
    //this.selectedDoc=223550000;
  }

  get sucuDescripcion() {
    return this.formulario.get('sucuDescripcion');
  }

  get sucuPuntoVenta() {
    return this.formulario.get('puntoVenta');
  }

  get sucuNombreSuper() {
    return this.formulario.get('sucuNombreSuper');
  }


  cerrarImportarArchivo() {
    this.habilitarBotonSubirArchivo = false;
    this.mostrarImportarArchivo = !this.mostrarImportarArchivo;
  }

  subirArchivo() {

    this.dashboardService.postFile(this.sucursalesParaGuardar).subscribe((data: SucursalGuardar[]) => {
      this.cargaExitosa = !this.cargaExitosa;
      this.sucursalesConError = [];
      this.sucursalesCorrectas = [];
      this.exportCvsData = [];
      data.forEach((item: Sucursal) => {

        if (item.codRespuesta == '-1') {
          this.sucursalesConError.push(item);
          this.cantSucusErroneas = 0;
          this.cantSucusErroneas = this.sucursalesConError.length;
        }
        if (item.codRespuesta == '0') {
          this.sucursalesCorrectas.push(item);
          this.cantSucusExitosas = 0;
          this.cantSucusExitosas = this.sucursalesCorrectas.length;
        }

        let id = item.id ? item.id : 'null';
        let descripcion = item.descripcion ? item.descripcion : 'null';
        let numeroDocumento = item.numeroDocumento ? item.numeroDocumento : 'null';
        let estado = item.codRespuesta == '0' ? 'Ok' : 'Error';
        let detalle = item.mensaje;
        let exportCsv = new ExportCsv('' + id, descripcion, '' + numeroDocumento, estado, detalle);
        if (item.id != null) {
          this.exportCvsData.push(exportCsv);
        }

      });

      //console.log("sucursales con error");
      //console.log(JSON.stringify(this.cantSucusErroneas));

      //console.log("sucursales correctas");
      //console.log(JSON.stringify(this.cantSucusExitosas));

      //console.log("array export data");
      //console.log(JSON.stringify(this.exportCvsData));

      this.msjSucursalesCargadas = true;
      this.habilitarBotonSubirArchivo = false;
      this.mostrarImportarArchivo = false;
      //this.getSucursales();

    }, error => {
      //console.log("error");
      //console.log(error);
    });
  }

  obtenerCabecerasArchivo(archivo: any) {
    let headers = archivo[0].split(';');
    let headerArray = [];

    for (let j = 0; j < 3; j++) {
      headerArray.push(headers[j]);
    }
    //console.log("headerArray");
    //console.log(headerArray);
    return headerArray;
  }

  obtenerDatosDeArchivo(csv: any, headerLength: any) {
    this.sucursalesParaGuardar = [];
    for (let i = 1; i < csv.length; i++) {
      //las cabeceras son 6, pero solo se toman las 3 primeras
      //for (let i = 1; i < 3; i++) {
      if (csv[i] != null) {
        let data = csv[i].split(';', headerLength);
        if (data.length == headerLength) {
          //si la fila esta vacia no se guarda.
          if (data[0] != '' || data[1] != '' || data[2] != '') {
            let sucursal: Sucursal = new Sucursal();
            sucursal.numeroPuntoVenta = data[0].trim();
            sucursal.descripcion = data[1].trim();
            sucursal.numeroDocumento = data[2].trim();
            sucursal.cuitSocio = this.userService.userData.cuit;
            this.sucursalesParaGuardar.push(sucursal);
          }
        }
      }
    }
  }

  isCSVFile(file: any) {

    return file.name.endsWith('.csv');

  }

  changeArchivo($event: any): void {
    var text = [];
    var files = $event.target.files;
    files ? this.habilitarBotonSubirArchivo = true : this.habilitarBotonSubirArchivo = false;

    if (this.isCSVFile(files[0])) {
      var input = $event.target;
      var reader = new FileReader();
      reader.readAsText(input.files[0]);

      reader.onload = (data) => {
        let csvData = reader.result;
        let csvRecordsArray = csvData.split(/\r\n|\n/);
        let headersRow = this.obtenerCabecerasArchivo(csvRecordsArray);
        this.obtenerDatosDeArchivo(csvRecordsArray, headersRow.length);
      };

      reader.onerror = function () {
        alert('Unable to read' + input.files[0]);
      };
    } else {
      alert('El archivo debe ser con extension .csv');
      this.fileReset();
      files = null;
    }
  }

  verificar(value?: any, tipo?: string) {
    if (tipo == 'puntoVenta') {
      value == '' || value == null ? this.puntoVentaActual = null : this.puntoVentaActual = value.toString();
    }

    if (tipo == 'descripcion') {
      value == '' || value == null ? this.descripcionActual = null : this.descripcionActual = value;
    }

    if (this.puntoVentaActual && this.descripcionActual) {
      this.habilitarBotonGuardar = true;
    } else {
      this.habilitarBotonGuardar = false;
    }

  }

  alerta(event?: any, tipo?: string) {
    //console.log(event);
    //console.log(tipo);
    this.verificar(event, tipo);
  }

  mostrarAvisoDeCambio(id: number) {
    //console.log("estado", id);
    this.cambiarEstado = true;
    this.sucursalEditar = this.sucursales.find((item) => {
      if (item.numeroPuntoVenta == id) {
        // item.estado = !item.estado;
        return item.numeroPuntoVenta == id;
      }

    });
  }

  cambiarEstadoSucursal() {
    this.cambiarEstado = false;
    //this.sucursalEditar.estado = !this.sucursalEditar.estado;
    this.guardarCambios('cambiar');
  }

  fileReset() {

    this.sucursalesParaGuardar = [];
    this.msjSucursalesCargadas = false;
    this.exportCvsData = [];
  }

  descargarArchivo() {
    this.cantSucusErroneas = 0;
    this.cantSucusExitosas = 0;

    this.fileReset();
  }

  eliminarDuplicados(arrayIn) {
    var arrayOut = [];
    arrayIn.forEach(item => {
      try {
        if (JSON.stringify(arrayOut[arrayOut.length - 1].zone) !== JSON.stringify(item.zone)) {
          arrayOut.push(item);
        }
      } catch (err) {
        arrayOut.push(item);
      }
    });
    return arrayOut;
  }

  removeDuplicates(originalArray, prop) {
    var newArray = [];
    var lookupObject = {};

    for (var i in originalArray) {
      lookupObject[originalArray[i][prop]] = originalArray[i];
    }

    for (i in lookupObject) {
      newArray.push(lookupObject[i]);
    }
    return newArray;
  }

  cerrarModalCargaArchivo() {
    this.mostrarImportarArchivo = false;
    this.exportCvsData = [];
    this.habilitarBotonSubirArchivo = false;
    this.fileReset();
  }

  descargarTemplate() {

  }

  cerrarMsjDeRespuestaDeCargaMasiva() {
    this.msjSucursalesCargadas = false;
    this.sucursales = [];
    this.getSucursales();
  }
}
