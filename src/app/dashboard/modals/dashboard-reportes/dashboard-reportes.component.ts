import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-dashboard-reportes',
  templateUrl: './dashboard-reportes.component.html',
  styleUrls: ['./dashboard-reportes.component.scss']
})
export class DashboardReportesComponent implements OnInit {
  isOpen: boolean = false;
  icon: any = 'plus';
  isActive: boolean = false;
  @Input() urlReportes: string = '';
  @Output() next: EventEmitter<any> = new EventEmitter<any>();
  constructor(private sanitizer: DomSanitizer) {
  
  }

  ngOnInit() {
    console.log(this.urlReportes);
  }
  
  toggleView() {
    this.isOpen = !this.isOpen;
    this.icon = this.isOpen ? 'minus' : 'plus';
  }
  
  getUrl(){
      return this.sanitizer.bypassSecurityTrustResourceUrl(this.urlReportes);
  }
  
}
