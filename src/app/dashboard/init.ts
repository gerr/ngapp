// Parent View
import { DashboardComponent } from './dashboard.component';

// Children - features
import { NewSaleComponent } from './views/new-sale/new-sale.component';
import { ChangePasswordComponent } from './views/change-password/change-password.component';

// New Sale Flow - steps
import { STEPS } from './components/steps/init';

// Products Templates
import { PRODUCTS_TEMPLATES } from './components/products/init';

// Forms
import { FORMS } from './forms/_forms';

// Simple components
import { LatestSalesComponent } from './components/latest-sales/latest-sales.component';
import { ChoosePlanComponent } from './components/choose-plan/choose-plan.component';

// Modals
import { CancelOperationModalComponent } from './modals/cancel-operation/cancel-operation-modal.component';
import { RiskDetailModalComponent } from './modals/risk-detail/risk-detail-modal.component';
import { ChooseBranchModalComponent } from './modals/choose-branch/choose-branch-modal.component';

export const COMPONENTS = [
  ...STEPS,
  ...PRODUCTS_TEMPLATES,
  ...FORMS,
  DashboardComponent,
  ChangePasswordComponent,
  NewSaleComponent,
  LatestSalesComponent,
  CancelOperationModalComponent,
  RiskDetailModalComponent,
  ChooseBranchModalComponent,
  ChoosePlanComponent
];

export const CHILDREN = [
  {
    path: '',
    component: NewSaleComponent
  },
  {
    path: 'perfil',
    component: ChangePasswordComponent
  }
];
