import { Component, OnInit } from '@angular/core';

import { OAuthService } from './shared/services/oauth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  isTokenValid = false;

  constructor(private authservice: OAuthService) {}

  ngOnInit() {
    // @ts-ignore
    this.authservice.getDataFromCookie().subscribe((data) => {
      console.log(data);
      this.isTokenValid = true;
    });
  }
}
