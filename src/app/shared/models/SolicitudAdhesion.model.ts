export class SolicitudAdhesion {
  nombreApellido: string;
  tipoDocNum: string;
  telefono: string;
  domicilio: string;
  provincia: string;
  codPostal: string;
  localidad: string;
  email: string;
  plan: string;
  fechaNacimiento?: string;
  fechaSolicitud?: string;
  productoElegido: number;
  cuit: string;
}
