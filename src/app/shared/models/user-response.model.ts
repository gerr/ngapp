import { Status } from './status.model';
import { UserModel } from './user.model';

export class UserModelResponse {
  status: Status;
  body: UserModel;
}
