/* export class UserModel {
  companyId: number;
  emailAddress: string;
  firstName: string;
  greeting: string;
  jobTitle: string;
  languageId: string;
  lastName: string;
  middleName: string;
  reminderQueryAnswer: string;
  reminderQueryQuestion: string;
  screenName: string;
  userId: number;
  company_name: string;
  userCuit: number;
} */

import {Client} from '../../dashboard/models/sales/sales.model';

export class UserModel {
  cuit?: string;
  dni?: string;
  exp?: number;
  firstName?: string;
  fullname?: string;
  greeting?: string;
  iat?: number;
  lastName?: string;
  mail?: string;
  roles?: string[];
  suc?: string;
  userid?: number;
  vendedor?: boolean;
  supervisor?: boolean;
  administrador?: boolean;
  mensajeria?: boolean;
  url_dashboard?: string;
  tiene_sps?: boolean;
  tieneSolicitud?: boolean;

}

export class PayData {
  card_number: string;
  card_expiration_month: number;
  card_expiration_year: number;
  security_code: number;
  card_holder_name: string;
  type: string;
  number: string;
}

export class UserAuto {
  user_data ? = new UserModel();
  client ? = new Client();
  client_data ? = new ClientData();
  pay_data ? = new PayData();
}

export class ClientData {
  nombre?: string;
  apellido?: string;
  celular1?: string;
  celular2?: string;
  celular3?: string;
  telefono_fijo1: string;
  telefono_fijo2?: string;
  telefono_fijo3?: string;
  id_estado_civil?: string;
  id_actividad?: string;
  id_genero?: string;
  fecha_nacimiento?: string;  // dd-mm-yyyy
  id_nacionalidad?: string;
  id_relacionLaboral?: string;
  tipo_documento?: string;
  nro_documento?: string;
  domicilio: {
    domicilio_codigo_postal?: string,
    id_localidad?: string,
    numero?: string,
    calle?: string,
    piso?: string,
    departamento?: string,
    id_provincia?: number,
    torre?: 7
  };
  email?: Array<string>;
}

