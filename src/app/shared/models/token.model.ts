export class TokenModel {
  mail: string;
  scope: String;
  expires_in: number;
  token_type: String;
  access_token: String;
  refresh_token: String;
}
