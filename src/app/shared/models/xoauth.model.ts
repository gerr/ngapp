export class XOauth {
  access_token: string;
  refresh_token: string;
  mail: string;
  javaClass: string;
  scope: string;
  token_type: string;
  expires_in: number;
}
