import { Status } from '../status.model';

// gerr
/* export class BranchesResponse {
  code: Status;
  body = {
    Negocios_por_socio_lista: [{
      id_negocio: Number
    }]
  };
} */

export class PuntoVenta {
  id_punto_venta: number;
  descripcion_punto_venta: string;
}

export class BranchesResponse {
  code: Status;
  body: Array<PuntoVenta>;
}
