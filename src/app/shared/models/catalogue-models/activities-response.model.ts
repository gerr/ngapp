import { Status } from '../status.model';
import { CatalogueObjectActividad } from './catalogue-object-actividad';
import { CatalogueObject } from './catalogue-object.model';


export class ActivitiesResponse {
  code: Status;
  body: Array<CatalogueObject>
  
}
