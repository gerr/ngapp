import { Status } from '../status.model';
import { CatalogResponse } from '../../../dashboard/models/services/services-response.model';
import { Catalog } from '../../../dashboard/models/services/services.model';

export class LocalidadesResponse extends CatalogResponse {

  body: Localidad[];
  code: Status;
  /* body = {
    localidades: [
      {
        idProvincia: String,
        localidades: [
          {
            idLocalidad: String,
            localidad: String
          }
        ]
      }
    ]
  }; */
}

export class Localidad extends Catalog{
      idLocalidad: String;
      localidad: String;
      idProvincia: String;

      id: string;
      descripcion: string;
}


