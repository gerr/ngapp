import {Injectable} from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest, HttpHandler,
  HttpSentEvent, HttpHeaderResponse,
  HttpProgressEvent,
  HttpResponse,
  HttpUserEvent,
  HttpErrorResponse
} from '@angular/common/http';
import {finalize, catchError, switchMap} from 'rxjs/operators';

import {OAuthService} from '../services/oauth.service';
import {throwError as observableThrowError, Observable, BehaviorSubject, of} from 'rxjs';
import {TokenModel} from '../models/token.model';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {
  private isRefreshingToken = false;
  private tokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  storage = sessionStorage;
  localstorage = localStorage;
  TOKEN_OBJECT: string = 'tokenObjectCardif';
  LOGOUT = environment.LOGOUT;
  COOKIE_NAME = environment.COOKIE_NAME;
  API_KEY = environment.API_KEY_SPS;
  URL_PSP = environment.URL_PSP;

  constructor(private auth: OAuthService,
              private cookieService: CookieService) {
  }

  addToken(request: HttpRequest<any>): HttpRequest<any> {
    if(request.url == this.URL_PSP){
      return request.clone({
        setHeaders: {
          apikey: `${this.API_KEY}`,
          'Content-Type': "application/json"
        }
      });
    }
    if (request.url.includes('refresh_token')) {
      return request;
    }

    const cok = this.cookieService.get(this.COOKIE_NAME);
    const tok = this.storage.getItem(this.TOKEN_OBJECT);

    if ( cok !== tok) {
      window.location.href = this.LOGOUT;
      //return null;
    }

    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.auth.getToken()}`
      }
    });
  }

  intercept(request: HttpRequest<any>, next: HttpHandler)
    : Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
    return next.handle(this.addToken(request)).pipe(
      catchError(error => {
        if (error instanceof HttpErrorResponse) {
          switch ((<HttpErrorResponse>error).status) {
            case 400:
              return this.handle400Error(error);
            case 401:
              return this.handle401Error(request, next);
            default:
              return observableThrowError(error);
          }
        } else {
          return observableThrowError(error);
        }
      })
    );
  }

  handle400Error(error) {
    if (error && error.status === 400 && error.error && error.error.error === 'invalid_grant') {
      this.auth.logOut();
      return observableThrowError('logout');
    }

    return observableThrowError(error);
  }

  handle401Error(request: HttpRequest<any>, next: HttpHandler) {
    if (!this.isRefreshingToken) {
      this.isRefreshingToken = true;
      this.tokenSubject.next(null);

      return this.auth.refreshToken().pipe(
        switchMap((response: TokenModel) => {
          if (response) {
            this.tokenSubject.next(response);
            return next.handle(this.addToken(request));
          }

          this.auth.logOut();
          return observableThrowError('Logout');
        }),
        finalize(() => {
          this.isRefreshingToken = false;
        })
      );
    } else {
      this.auth.logOut();
      // return this.tokenSubject.pipe(
      //   filter(token => token != null),
      //   take(1),
      //   switchMap(() => {
      //     return next.handle(this.addToken(request));
      //   }));
    }
  }
}
