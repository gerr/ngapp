import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, FormControl} from '@angular/forms';

@Directive({
  selector: '[appMinValue][ngModel]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: MinValueDirective,
    multi: true
  }]
})
export class MinValueDirective implements Validator {
  @Input() appMinValue: number;

  validate(f: FormControl): {[key: string]: any} {
    const value = parseInt(f.value, 0);
    return value < +this.appMinValue ? { 'minValue': true } : null;
  }
}
