import { Directive, ElementRef, HostListener, Input, OnChanges, SimpleChanges } from '@angular/core';

@Directive({ selector: '[appNumbersOnly]' })
export class NumbersOnlyDirective implements OnChanges {

    @Input()'Type': string;

    previousValue = '';
    maxLenght = 0;
    regex = '';
    regexFirst3Digits = '';  // A regular expression only to PA Type
    regexAlt = '';  // A regular expression only to PA Type
    checkedValue = '';
    alreadyFilteredKey = false;

    constructor(private hostElement: ElementRef) {}

    ngOnChanges(changes: SimpleChanges) {
        this.setRules();
        this.validateOnChange();
    }

    @HostListener('change', ['$event']) onChange() {
        this.validateValue(this.hostElement.nativeElement.value);
    }

    @HostListener('input', ['$event'])
    onInput(e) {
      if (!this.alreadyFilteredKey) {
          if (this.checkPressedKey(e)) {  // ifValid
            return;
          } else {
            this.hostElement.nativeElement['value'] = this.checkedValue;
          }
      }
    }

    @HostListener('paste', ['$event']) onPaste(e) {

        // get and validate data from clipboard
        let value = e.clipboardData.getData('text/plain');
        value = value.replace(/\s/g, '');   // remove whitespace
        let valid = (new RegExp(this.regex)).test(value);
        this.previousValue = this.hostElement.nativeElement['value'];

        // if lenght > maxlength trim the value to fit
        if ((this.previousValue.length + value.length) > this.maxLenght) {
          value = value.substr(0, (this.maxLenght - this.previousValue.length));
        }

        // Only for passport 3 numbers or letters and after that only numbers
        if (this.Type === '1') {
          if (this.previousValue.length < 3) {
            for (let x = 0; x < (value.length); x++) {

              // if 3 first digits
              if (this.previousValue.length + (x + 1) > 3) {
                valid = (new RegExp(this.regexAlt)).test(value[x]);
                if (!valid) { break; }
              // if more than 3 digits
              } else {
                valid = (new RegExp(this.regexFirst3Digits)).test(value[x]);
                if (!valid) { break; }
              }
            }
          }
        }

        if (valid) {
            this.hostElement.nativeElement['value'] = this.previousValue + value;
            e.preventDefault();
        } else {
            e.preventDefault();
        }
    }

    @HostListener('keydown', ['$event']) onKeyDown(e: KeyboardEvent) {

      this.alreadyFilteredKey = false;
      this.checkedValue = this.hostElement.nativeElement['value'];  // copy previous value on keydown

      if (this.checkPressedKey(e)) {
          return;
      } else {
          e.preventDefault();
      }
    }

    checkPressedKey(e) {

      let key: string = this.getName(e);

      // If key has no value then get it from e.data (onInput Event)
      if (!key) {
        key = e.data;
      }

      // 'Unidentified' is the returned key value on mobile chrome onKeyDown event
      if (key !== 'Unidentified') {
        // this flag states that the onKeyDown event actually returned a value
        this.alreadyFilteredKey = true;
      }

      const controlOrCommand = (e.ctrlKey === true || e.metaKey === true);

      // allowed keys apart from numeric characters
      const allowedKeys = [
          'Backspace', 'ArrowLeft', 'ArrowRight', 'Escape', 'Tab', 'Delete', 'End', 'Home', 'Enter'
      ];

      // allow some non-numeric characters
      if (allowedKeys.indexOf(key) !== -1 ||
          // Allow: Ctrl+A and Command+A
          (key === 'a' && controlOrCommand) ||
          // Allow: Ctrl+C and Command+C
          (key === 'c' && controlOrCommand) ||
          // Allow: Ctrl+V and Command+V
          (key === 'v' && controlOrCommand) ||
          // Allow: Ctrl+X and Command+X
          (key === 'x' && controlOrCommand)) {
          // let it happen, don't do anything
          return true;
      }

      // Only to PA Type, to allow 3 letters and only numbers after that
      if (this.Type === '1') {
        // After first 3 digits, only numbers allowed
        if (this.checkedValue.length >= 3) {
            this.regex = this.regexAlt;
        } else {
          this.regex = this.regexFirst3Digits;
        }
      }

      // allows specific characters only
      let isValid = (new RegExp(this.regex)).test(key);

      // Max length
      if (this.checkedValue.length >= this.maxLenght) {
          isValid = false;
      }

      return isValid;
    }

    validateValue(value: string): void {
        let valid: boolean = (new RegExp(this.regex)).test(value);
        if (value.length > this.maxLenght) {
          valid = false;
        }
        this.hostElement.nativeElement['value'] = valid ? value : this.previousValue;
    }

    validateOnChange() {  // when input Type changes, validate
      const value = this.hostElement.nativeElement['value'];
      let valid: boolean = (new RegExp(this.regex)).test(value);

      if (value.length > this.maxLenght) {
        valid = false;
      }

      this.hostElement.nativeElement['value'] = valid ? value : '';
    }

    getName(e): string {
        // for old browsers

        if (e.key) {
            return e.key;
        } else {
            if (e.keyCode && String.fromCharCode) {

                switch (e.keyCode) {
                    case   8: return 'Backspace';
                    case   9: return 'Tab';
                    case  13: return 'Enter';
                    case  27: return 'Escape';
                    case  37: return 'ArrowLeft';
                    case  39: return 'ArrowRight';
                    case  46: return 'Delete';
                    case  82: return 'Home';
                    default: return String.fromCharCode(e.keyCode);
                }
            }
        }
    }

    setRules() {
        switch (this.Type) {
            case '1': { // Pasaporte - max 9
                this.maxLenght = 9;
                this.regex = '^[A-Za-z0-9]*$';
                this.regexFirst3Digits = '^[A-Za-z0-9]*$';
                this.regexAlt = '^[0-9]*$';
                break;
            }
            case '5': { // DNI - max 9
              this.maxLenght = 9;
              this.regex = '^[0-9]*$';
              break;
          }
            case '6': { // CUIT - max 11
                this.maxLenght = 11;
                this.regex = '^[0-9]*$';
                break;
            }
            case '8': { // CUIL - max 11
                this.maxLenght = 11;
                this.regex = '^[0-9]*$';
                break;
            }
            case 'CPA': { // CPA - max 4
              this.maxLenght = 4;
              this.regex = '^[0-9]*$';
              break;
            }
            case 'TEL': { // TEL - max 10
              this.maxLenght = 10;
              this.regex = '^[0-9]*$';
              break;
            }
            case 'ALT': { // ALT - max 6
              this.maxLenght = 6;
              this.regex = '^[0-9]*$';
              break;
            }
            case 'CARD': {  // credit card fragment - max 4
              this.maxLenght = 4;
              this.regex = '^[0-9]*$';
              break;
            }
            case 'SECC': {  // credit card security code - max 4
              this.maxLenght = 4;
              this.regex = '^[0-9]*$';
              break;
            }
            case 'VALC': {  // SMS validation code - max 8
              this.maxLenght = 8;
              this.regex = '^[A-Za-z0-9]*$';
              break;
            }
            case 'L': {
              this.maxLenght = 99;
              this.regex = '^[A-Za-z ]*$';
              break;
            }
            case 'VENC': {
              this.maxLenght = 4;
              this.regex = '^[0-9]*$';
              break;
            }
            case 'percentage': {
              this.maxLenght = 3;
              this.regex = '^[0-9]*$';
              break;
            }
            default: { // Default - max 8
                this.maxLenght = 8;
                this.regex = '^[0-9]*$';
                break;
            }
        }
    }
}
