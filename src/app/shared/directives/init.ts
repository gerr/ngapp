import { NumbersOnlyDirective } from './numbers-only.directive';
import { MaxValueDirective } from './max-value.directive';
import { MinValueDirective } from './min-value.directive';

export const DIRECTIVES = [
  NumbersOnlyDirective,
  MaxValueDirective,
  MinValueDirective
];
