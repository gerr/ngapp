import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, FormControl } from '@angular/forms';

@Directive({
  selector: '[appMaxValue][ngModel]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: MaxValueDirective,
    multi: true
  }]
})
export class MaxValueDirective implements Validator {
  @Input() appMaxValue: number;
  @Input() suma: number=0;

  validate(f: FormControl): {[key: string]: any} {
    const value = parseInt(f.value, 0)+this.suma;
    return value > +this.appMaxValue ? { 'maxValue': true } : null;
  }
}
