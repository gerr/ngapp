import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {
  transform(data: any[], key: string, isAsc: boolean = false) {
    if (!data && data.length) {
      return;
    }

    if (key == 'fechaActualizacion') {

      data.sort(function (a, b) {
        return (convertirFecha(a[key]) < convertirFecha(b[key]) ? -1 : 1) * (isAsc ? 1 : -1);
      });

      //@ts-ignore
      function   convertirFecha (fechaString) {
        var fechaSp = fechaString.split("-");
        var anio = new Date().getFullYear();
        if (fechaSp.length == 3) {
          anio = fechaSp[2];
        }
        var mes = fechaSp[1] - 1;
        var dia = fechaSp[0];

        return new Date(anio, mes, dia);
      }
    }else{

      data.sort((a, b) => {
        return (a[key] < b[key] ? -1 : 1) * (isAsc ? 1 : -1);
      });
    }


    return data;
  }

  /*
  Puede recibir fechas en formato: "dd-mm" (supone año actual) o "dd-mm-aaaa"
  (Esta función esta ajustada a las fechas que tienes)
*/

}
