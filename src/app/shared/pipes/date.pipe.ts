import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'date'
})
export class DatePipe implements PipeTransform {
  transform(seconds: string) {
    if (!seconds) {
      return;
    }

    const date = new Date(+seconds);
    const day = this.addZero(date.getDate());
    const month = this.addZero(date.getMonth() + 1);
    const year = date.getFullYear();

    return `${day}-${month}-${year}`;
  }

  addZero(value: number) {
    return value > 9 ? value : `0${value}`;
  }
}
