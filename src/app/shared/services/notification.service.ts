import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { RespuestaVenta } from '../../dashboard/models/ventas-realizadas/respuesta-venta.model';


@Injectable()
export class NotificationService {
  public isChangePassSuccess = new Subject<boolean>();
  public isChangePassError = new Subject<boolean>();
  public isSaleSuccess = new Subject<{status: boolean, idSale?: string, respuesta: RespuestaVenta}>();

  public errorServidor = new Subject<any>();

  public mostrarErrorServidor(){
    this.errorServidor.next(true);
  }

  public showChangePasswordSucces() {
    this.isChangePassSuccess.next(true);
  }

  public showChangePasswordError() {
    this.isChangePassError.next(true);
  }

  public showSaleSuccess(respuestaVenta: RespuestaVenta) {
    this.isSaleSuccess.next({ status: true, idSale: respuestaVenta.idContrato, respuesta: respuestaVenta });
  }

  getPassSucces(): Observable<any> {
    return this.isChangePassSuccess;
  }
  
  getPassError(): Observable<any> {
    return this.isChangePassError;
  }

  getSaleSucces(): Observable<any> {
    return this.isSaleSuccess;
  }

  getErrorServidor(){
    return this.errorServidor;
  }
}
