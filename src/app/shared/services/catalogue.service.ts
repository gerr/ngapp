import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {BranchesResponse} from '../models/catalogue-models/branches-response.model';
import {ActivitiesResponse} from '../models/catalogue-models/activities-response.model';
import {of} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {LocalidadesResponse, Localidad} from '../models/catalogue-models/localidades-response.model';

import {DocumentTypeResponse} from '../models/document-type-response.model';
import {CatalogueObject} from '../models/catalogue-models/catalogue-object.model';
import {NewSaleService} from '../../dashboard/services/new-sale.service';

@Injectable()
export class CatalogueService {
  BASE_URL_API = environment.API;
  SUCCESS: Number = 200;
  contentType: Object = {
    key: 'Content-Type',
    value: 'application/json'
  };

  public branchList: BranchesResponse;
  private documentTypes: DocumentTypeResponse;
  private nationalityList: any;
  private activityList: ActivitiesResponse;
  private locaList: LocalidadesResponse;
  public localidadesActuales: any;

  constructor(private http: HttpClient, private newSaleService: NewSaleService) {
  }

  getBranches(cuit: string) {
    if (this.branchList) {
      return of(this.branchList);
    }
    const serviceUrl = `${this.BASE_URL_API}/sucursal/obtener-sucursal/${cuit}`;
    const headers = this.getHeaders([this.contentType]);

    return this.http.get(serviceUrl, {headers})
      .pipe(
        tap((response: BranchesResponse) => {
          if (response.code.code === this.SUCCESS) {
            this.branchList = response;
          }
        })
      );
  }

  getDocumentType() {
    const serviceUrl = 'documents/document-type';
    const documentTypeCode = '5';
    const fullUrl = `${this.BASE_URL_API}/${serviceUrl}`;
    const headers = this.getHeaders([{key: 'userId', value: 'test'}]);

    return this.http.get(fullUrl, {headers})
      .pipe(
        tap((response: any) => this.documentTypes = response),
        map(res => res.listadoItem )
      );
  }

  getNationalities() {
    if (this.nationalityList) {
      return of(this.nationalityList);
    }

    const serviceUrl = `${this.BASE_URL_API}/catalogo-nacionalidad`;
    const headers = this.getHeaders([this.contentType]);

    return this.http.get(serviceUrl, {headers})
      .pipe(
        tap((response: any) => {
          if (response.code.code === this.SUCCESS) {
            this.nationalityList = response;
            //console.log("nacionalidades: ", JSON.stringify(this.nationalityList))
          }
        })
      );
  }

  getActivities() {
    if (this.activityList) {
      return of(this.activityList);
    }

    const serviceUrl = `${this.BASE_URL_API}/catalogo-actividad`;
    const headers = this.getHeaders([this.contentType]);

    return this.http.get(serviceUrl, {headers})
      .pipe(
        tap((response: ActivitiesResponse) => {
          if (response.code.code === this.SUCCESS) {
            response.body = response.body.sort((a: CatalogueObject, b: CatalogueObject) =>
              a.descripcion < b.descripcion ? -1 : 1);

            this.activityList = response;
          }
        })
      );
  }

  getLocalidades(idProvincia) {
    // if (this.locaList) {
    //   return of(this.locaList);
    // }
    const serviceUrl = `${this.BASE_URL_API}/catalogo-localidad/provincia/${idProvincia}`;
    const headers = this.getHeaders([this.contentType]);
    return this.http.get(serviceUrl, {headers})
      .pipe(
        tap((response: LocalidadesResponse) => {
          if (response.code.code === this.SUCCESS) {
            this.locaList = response;
            this.newSaleService.localidades = this.locaList.body;
          }
        })
      );

  }

  // gerr: 09/05 nuevo metodo para obtener las localidades
  ObtenerLocalidades(idProvincia) {
    const serviceUrl = `${this.BASE_URL_API}/catalogo-localidad/provincia/${idProvincia}`; // gerr: 29/04 mi codigo
    const headers = this.getHeaders([this.contentType]);

    this.http.get(serviceUrl, {headers})
      .subscribe((respuesta: any) => {
        this.localidadesActuales = respuesta.body;

      });
  }

  private getHeaders(options?: Array<any>) {
    let headers = new HttpHeaders({user: 'user', password: 'password'});

    if (options && options.length) {
      options.map(({key, value}: any) => {
        headers = headers.append(key, value);
      });
    }

    return headers;
  }
}
