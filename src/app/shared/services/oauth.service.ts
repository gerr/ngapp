import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';

import {of} from 'rxjs';
import {map, tap} from 'rxjs/operators';

import {TokenModel} from '../models/token.model';
import {XOauth} from '../models/xoauth.model';
import {UserAuto, UserModel} from '../models/user.model';
import {environment} from '../../../environments/environment';
import {ActivatedRoute, Router} from '@angular/router';

@Injectable()
export class OAuthService {
  BASE_URL_API = environment.API;
  BASE_URL_OAUTH = environment.OAUTH;
  COOKIE_NAME = environment.COOKIE_NAME;
  COOKIE_LOGIN_AUTO = environment.COOKIE_LOGIN_AUTO;
  TOKEN_OBJECT: string = 'tokenObjectCardif';
  tokenObject: TokenModel;
  USER_DATA: string = 'userData';
  userData: UserModel = new UserModel();
  userAuto: UserAuto = new UserAuto();
  jwt: string;
  storage = sessionStorage;
  localstorage = localStorage;
  isLoginAuto: boolean = false;

  constructor(private http: HttpClient,
              private cookieService: CookieService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  getDataFromCookie() {
    /**
     * descomentar para usar login automatico hardcodeado.
     */
    //this.mockCookieAuto();

    this.tieneLoginAuto();

    if (this.cookieService.check(this.COOKIE_NAME)) {
      const cookie = this.cookieService.get(this.COOKIE_NAME);
      console.log(this.cookieService.get('document'));
      this.storage.setItem(this.TOKEN_OBJECT, cookie);

      this.jwt = cookie;

    } else {
      this.logOut();
      return of(false);
    }

    if (this.jwt) {
      return this.getUser();
    } else {
      this.logOut();
      return of(false);
    }
  }


  private getUser() {

    const serviceUrl = 'usuarios/obtener-usuario';
    const fullUrl = `${this.BASE_URL_API}/${serviceUrl}`;
    //return this.http.post(fullUrl, null)
    return this.http.get(fullUrl)
      .pipe(
        tap((response: UserModel) => {
          console.log('response --------------------------------------------------------------');
          console.log(JSON.stringify(response));
          this.userData = response;
          this.userData.url_dashboard = response.url_dashboard;
          if (environment.production != true) {
            console.log('************************************************* environment.production = ', environment.production);
            console.log('HARDCODEADO URL DASHBOARD: ', this.userData.url_dashboard);
            console.log('************************************************************************** ');
          }
          return response;
        })
      );
  }

  refreshToken() {
    const auth = btoa('my-trusted-client:aqui_va_el_password');
    const headers = new HttpHeaders({
      Authorization: `Basic ${auth}`
    });
    const refreshUrl = `${this.BASE_URL_OAUTH}?grant_type=refresh_token&refresh_token=${this.tokenObject.expires_in}`;

    return this.http.post(refreshUrl, {
      grant_type: 'refresh_token',
      refresh_token: this.tokenObject.expires_in
    }, {headers})
      .pipe(
        tap((response: TokenModel) => {
          const newCookie = Object.assign(this.tokenObject, response);
          this.storage.setItem(this.TOKEN_OBJECT, JSON.stringify(newCookie));
          this.tokenObject.access_token = response.access_token;
          this.tokenObject.refresh_token = response.refresh_token;
        })
      );
  }

  getToken() {
    return this.jwt;
  }

  isAuthenticated() {
    return !!this.tokenObject;
  }

  logOut() {
    this.jwt = null;
    this.tokenObject = null;
    this.cookieService.deleteAll();
    this.storage.removeItem(this.TOKEN_OBJECT);

    if (environment.production || environment.testing) {
      window.location.replace(environment.LOGOUT);
    }

    return null;
  }

  getUserData() {
    if (this.userData) {
      return of(this.userData);
    }
  }

  private tieneLoginAuto() {
    if (this.cookieService.check(this.COOKIE_LOGIN_AUTO)) {
      const datosToken = atob(this.cookieService.get(this.COOKIE_LOGIN_AUTO));
      let json = JSON.parse(datosToken);
      if (!environment.production) {
        console.log('************************************************* environment.production = ', environment.production);
        console.log('datosToken : ', datosToken);
        console.log('************************************************************************** ');
      }
      const {userData, clientData, payData} = json;
      this.userAuto.user_data = userData;
      this.userAuto.client_data = clientData;
      this.formatClient();
      this.userAuto.pay_data = payData;
      this.isLoginAuto = true;
      this.cookieService.delete(this.COOKIE_LOGIN_AUTO);
    } else {
      this.isLoginAuto = false;
    }
  }

  /**
   * metodo para usar login automatico hardcodeado.
   */
  mockCookieAuto() {
    if (!environment.production) {
      console.log('************************************************* environment.production = ', environment.production);
      console.log('hardcodeo de login automatico ativado.');
      console.log('************************************************************************** ');
    }
    let tokenAuto = 'ewogICAgInVzZXJEYXRhIjogewogICAgICAgICJtYWlsIjogImxhX2Fub25pbWFAbWFpbC5jb20uYXIiLAogICAgICAgICJ1c2VyaWQiOiA1NDU1MTQsCiAgICAgICAgImN1aXQiOiAiMzA2ODE0MjQ3MjEiLAogICAgICAgICJmaXJzdE5hbWUiOiAiUm9nZXIiLAogICAgICAgICJsYXN0TmFtZSI6ICJGZWRlcmVyIiwKICAgICAgICAiZ3JlZXRpbmciOiAiV2VsY29tZSBSb2dlciBGZWRlcmVyISIsCiAgICAgICAgImZ1bGxuYW1lIjogIlJvZ2VyIEZlZGVyZXIiLAogICAgICAgICJkbmkiOiAiMzAzMzMwMzMiLAogICAgICAgICJzdWMiOiAiNTEiLAogICAgICAgICJtZW5zYWplcmlhIjogZmFsc2UsCiAgICAgICAgImFkbWluaXN0cmFkb3IiOiBmYWxzZSwKICAgICAgICAidmVuZGVkb3IiOiB0cnVlLAogICAgICAgICJzdXBlcnZpc29yIjogZmFsc2UKICAgIH0sCgogICAgImNsaWVudERhdGEiOnsKICAgICAgICAibm9tYnJlIjogIk1hcnRpbiIsCiAgICAgICAgImFwZWxsaWRvIjogIkFyaWFzIiwKICAgICAgICAiY2VsdWxhcjEiOiAiMDExLTMxMjMxMjQ1IiwKICAgICAgICAiY2VsdWxhcjIiOiAiMTEtMzY0MjMyNDkiLAogICAgICAgICJjZWx1bGFyMyI6ICIxMS0zNjQyMzI0OCIsCiAgICAgICAgInRlbGVmb25vRmlqbzEiOiAiMDExLTExMTExMTExIiwKICAgICAgICAidGVsZWZvbm9GaWpvMiI6ICIxMS0yMjIyMjIyMiIsCiAgICAgICAgInRlbGVmb25vRmlqbzMiOiAiMTEtMjIyMjIyMjIiLAogICAgICAgICJpZEVzdGFkb0NpdmlsIjogIjMiLAogICAgICAgICJpZEFjdGl2aWRhZCI6ICI2IiwKICAgICAgICAiaWRHZW5lcm8iOiAiMSIsIAogICAgICAgICJmZWNoYU5hY2ltaWVudG8iOiAiMTk5OS0xMi0xMiIsCiAgICAgICAgImlkTmFjaW9uYWxpZGFkIjogIjMyIiwKICAgICAgICAiaWRSZWxhY2lvbkxhYm9yYWwiOiAiMiIsCiAgICAgICAgInRpcG9Eb2N1bWVudG8iOiAiNSIsCiAgICAgICAgIm5yb0RvY3VtZW50byI6ICIzMTU1NTc4NyIsCiAgICAgICAgImRvbWljaWxpbyI6IHsKICAgICAgICAgICAgImRvbWljaWxpb19jb2RpZ29fcG9zdGFsIjogIjE3NTQiLAogICAgICAgICAgICAiaWRMb2NhbGlkYWQiOiAxNjgxLAogICAgICAgICAgICAibnVtZXJvIjogIjciLAogICAgICAgICAgICAiY2FsbGUiOiAiaGFoYWhhIiwKICAgICAgICAgICAgInBpc28iOiAiNyIsCiAgICAgICAgICAgICJkZXBhcnRhbWVudG8iOiAiNyIsCiAgICAgICAgICAgICJpZFByb3ZpbmNpYSI6IDEsCiAgICAgICAgICAgICJ0b3JyZSI6ICI3IgogICAgICAgIH0sCiAgICAgICAgImVtYWlsIjogWwogICAgICAgICAgICAiYXJpYXNtYXJ0aW5AZ21haWwuY29tIgogICAgICAgIF0KICAgIH0sCgogICAgInBheURhdGEiOiB7CiAgICAgICAgImNhcmRfbnVtYmVyIjogIjMyMTEzMjExMzIxMTMyMTEiLAogICAgICAgICJjYXJkX2V4cGlyYXRpb25fbW9udGgiOiAxMiwKICAgICAgICAiY2FyZF9leHBpcmF0aW9uX3llYXIiOiAyMCwKICAgICAgICAic2VjdXJpdHlfY29kZSI6IDMyMSwKICAgICAgICAiY2FyZF9ob2xkZXJfbmFtZSI6ICJub21icmUgdGl0dSIsIAogICAgICAgICJ0eXBlIjogIkROSSIsCiAgICAgICAgIm51bWJlciI6ICIzMDMyMTMyMSIKICAgIH0KICAgIAp9Cgo=';
    //let tokenAuto =
    // 'bWVzc2FnZT0rKyslMEQlMEErKysrJTdCJTBEJTBBKysrKyUyMmNsaWVudERhdGElMjIlM0ElN0IlMEQlMEErKysrKysrKyUyMm5vbWJyZSUyMiUzQSslMjJNYXJ0aW4lMjIlMkMlMEQlMEErKysrKysrKyUyMmFwZWxsaWRvJTIyJTNBKyUyMkFyaWFzJTIyJTJDJTBEJTBBKysrKysrKyslMjJjZWx1bGFyMSUyMiUzQSslMjIwMTEtMzEyMzEyNDUlMjIlMkMlMEQlMEErKysrKysrKyUyMmNlbHVsYXIyJTIyJTNBKyUyMjExLTM2NDIzMjQ5JTIyJTJDJTBEJTBBKysrKysrKyslMjJjZWx1bGFyMyUyMiUzQSslMjIxMS0zNjQyMzI0OCUyMiUyQyUwRCUwQSsrKysrKysrJTIydGVsZWZvbm9GaWpvMSUyMiUzQSslMjIwMTEtMTExMTExMTElMjIlMkMlMEQlMEErKysrKysrKyUyMnRlbGVmb25vRmlqbzIlMjIlM0ErJTIyMTEtMjIyMjIyMjIlMjIlMkMlMEQlMEErKysrKysrKyUyMnRlbGVmb25vRmlqbzMlMjIlM0ErJTIyMTEtMjIyMjIyMjIlMjIlMkMlMEQlMEErKysrKysrKyUyMmlkRXN0YWRvQ2l2aWwlMjIlM0ErJTIyMyUyMiUyQyUwRCUwQSsrKysrKysrJTIyaWRBY3RpdmlkYWQlMjIlM0ErJTIyNiUyMiUyQyUwRCUwQSsrKysrKysrJTIyaWRHZW5lcm8lMjIlM0ErJTIyMSUyMiUyQyslMEQlMEErKysrKysrKyUyMmZlY2hhTmFjaW1pZW50byUyMiUzQSslMjIxOTk5LTEyLTEyJTIyJTJDJTBEJTBBKysrKysrKyslMjJpZE5hY2lvbmFsaWRhZCUyMiUzQSslMjIzMiUyMiUyQyUwRCUwQSsrKysrKysrJTIyaWRSZWxhY2lvbkxhYm9yYWwlMjIlM0ErJTIyMiUyMiUyQyUwRCUwQSsrKysrKysrJTIydGlwb0RvY3VtZW50byUyMiUzQSslMjI1JTIyJTJDJTBEJTBBKysrKysrKyslMjJucm9Eb2N1bWVudG8lMjIlM0ErJTIyMzE1NTU3ODclMjIlMkMlMEQlMEErKysrKysrKyUyMmRvbWljaWxpbyUyMiUzQSslN0IlMEQlMEErKysrKysrKysrKyslMjJkb21pY2lsaW9fY29kaWdvX3Bvc3RhbCUyMiUzQSslMjIxNzU0JTIyJTJDJTBEJTBBKysrKysrKysrKysrJTIyaWRMb2NhbGlkYWQlMjIlM0ErMTY4MSUyQyUwRCUwQSsrKysrKysrKysrKyUyMm51bWVybyUyMiUzQSslMjI3JTIyJTJDJTBEJTBBKysrKysrKysrKysrJTIyY2FsbGUlMjIlM0ErJTIyaGFoYWhhJTIyJTJDJTBEJTBBKysrKysrKysrKysrJTIycGlzbyUyMiUzQSslMjI3JTIyJTJDJTBEJTBBKysrKysrKysrKysrJTIyZGVwYXJ0YW1lbnRvJTIyJTNBKyUyMjclMjIlMkMlMEQlMEErKysrKysrKysrKyslMjJpZFByb3ZpbmNpYSUyMiUzQSsxJTJDJTBEJTBBKysrKysrKysrKysrJTIydG9ycmUlMjIlM0ErJTIyNyUyMiUwRCUwQSsrKysrKysrJTdEJTJDJTBEJTBBKysrKysrKyslMjJlbWFpbCUyMiUzQSslNUIlMEQlMEErKysrKysrKysrKyslMjJhcmlhc21hcnRpbiU0MGdtYWlsLmNvbSUyMiUwRCUwQSsrKysrKysrJTVEJTBEJTBBKysrKyU3RCUyQyUwRCUwQSUwRCUwQSsrKyslMjJwYXlEYXRhJTIyJTNBKyU3QiUwRCUwQSsrKysrKysrJTIyY2FyZF9udW1iZXIlMjIlM0ErJTIyMzIxMTMyMTEzMjExMzIxMSUyMiUyQyUwRCUwQSsrKysrKysrJTIyY2FyZF9leHBpcmF0aW9uX21vbnRoJTIyJTNBKzEyJTJDJTBEJTBBKysrKysrKyslMjJjYXJkX2V4cGlyYXRpb25feWVhciUyMiUzQSsyMCUyQyUwRCUwQSsrKysrKysrJTIyc2VjdXJpdHlfY29kZSUyMiUzQSszMjElMkMlMEQlMEErKysrKysrKyUyMmNhcmRfaG9sZGVyX25hbWUlMjIlM0ErJTIybm9tYnJlK3RpdHUlMjIlMkMrJTBEJTBBKysrKysrKyslMjJ0eXBlJTIyJTNBKyUyMkROSSUyMiUyQyUwRCUwQSsrKysrKysrJTIybnVtYmVyJTIyJTNBKyUyMjMwMzIxMzIxJTIyJTBEJTBBKysrKyU3RCUwRCUwQSsrKyslMEQlMEElN0QlMEQlMEErKyslMEQlMEErKys=';
    this.cookieService.set(this.COOKIE_LOGIN_AUTO, tokenAuto);
  }

  private formatClient() {
    this.userAuto.client.nombre = this.userAuto.client_data.nombre;
    this.userAuto.client.apellido = this.userAuto.client_data.apellido;
    this.userAuto.client.celular1 = this.userAuto.client_data.celular1;
    this.userAuto.client.celular2 = this.userAuto.client_data.celular2;
    this.userAuto.client.celular3 = this.userAuto.client_data.celular3;
    this.userAuto.client.telefonoFijo1 = this.userAuto.client_data.telefono_fijo1;
    this.userAuto.client.telefonoFijo2 = this.userAuto.client_data.telefono_fijo2;
    this.userAuto.client.telefonoFijo3 = this.userAuto.client_data.telefono_fijo3;
    this.userAuto.client.idEstadoCivil = this.userAuto.client_data.id_estado_civil;
    this.userAuto.client.idActividad = this.userAuto.client_data.id_actividad;
    this.userAuto.client.idGenero = this.userAuto.client_data.id_genero;
    this.userAuto.client.fechaNacimiento = this.userAuto.client_data.fecha_nacimiento;
    this.userAuto.client.idNacionalidad = this.userAuto.client_data.id_nacionalidad;
    this.userAuto.client.idRelacionLaboral = this.userAuto.client_data.id_relacionLaboral;
    this.userAuto.client.tipoDocumento = this.userAuto.client_data.tipo_documento;
    this.userAuto.client.nroDocumento = this.userAuto.client_data.nro_documento;
    this.userAuto.client.domicilio = this.userAuto.client_data.domicilio;
    this.userAuto.client.email = this.userAuto.client_data.email;

  }
  // id: mock
  private crearUsuarioHardcodeado() {
    const userHard2 = JSON.parse('{"mail": "don_ramon@fullzero.com.ar","userid": 623514,"cuit": "30687310434","firstName": "Don","lastName": "Ramon","greeting": "Welcome Don Ramon!","fullname": "Don Ramon","iat": 1584547028,"exp": 1584633428,"dni": "","suc": "","roles": [ "Administrador Socio", "Administrador"],"mensajeria": false,"url_dashboard": "https"}');
    this.userData = userHard2;

  }
  // fin mock
}

