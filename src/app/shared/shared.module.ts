import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { COMPONENTS } from './components/init';
import { DIRECTIVES } from './directives/init';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { RefreshTokenInterceptor } from './interceptors/refresh.token.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { SortPipe } from './pipes/sort.pipe';
import { DatePipe } from './pipes/date.pipe';
library.add(fas);

@NgModule({
  imports:  [ CommonModule, RouterModule, FontAwesomeModule, FormsModule ],
  declarations: [ ...COMPONENTS, ...DIRECTIVES, SortPipe, DatePipe],
  exports: [ ...COMPONENTS, CommonModule, ...DIRECTIVES, SortPipe, DatePipe],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: RefreshTokenInterceptor,
    multi: true
  }, SortPipe, DatePipe]
})
export class SharedModule {}
