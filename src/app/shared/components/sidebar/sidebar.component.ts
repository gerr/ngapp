import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserModel } from '../../models/user.model';
import { OAuthService } from '../../services/oauth.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  links: Array<{ url: string, title: string }>;
  isSidebarOpen: boolean;
  accSub1 = false;
  accSub2 = false;

  user: UserModel;

  constructor(private authservice: OAuthService,
              private router: Router) { }

  ngOnInit() {
    this.links = [
      {
        url: 'http://www.bnpparibascardif.com.ar/',
        title: 'BNP Paribas Cardif Argentina'
      },
      {
        url: 'http://www.bnpparibascardif.com.ar/',
        title: 'Condiciones generales'
      },
      {
        url: 'http://qr.afip.gob.ar/?qr=_bDIwALCUrzOC0AWXkKj9Q,,',
        title: 'Data Fiscal Seguros'
      },
      {
        url: 'http://qr.afip.gob.ar/?qr=diQSc-EwKZ_BQCNY1vOU7Q,,',
        title: 'Data Fiscal Servicios'
      }
    ];
    this.isSidebarOpen = false;
    this.authservice.getUserData().subscribe((response: UserModel) => {
      this.user = response;
    });
  }

  openSidebar() {
    this.isSidebarOpen = true;
  }

  logout()
  {
    this.authservice.logOut();
   // window.location.href = 'http://bnp-cardif-uat.globant.com/c/portal/logout';
  }

  redirect(path: string) {
    this.closeSidebar();
    this.router.navigate([`/${path}`]);
  }

  closeSidebar() {
    this.isSidebarOpen = false;
  }

  openAccordion(accordionNumber) {
    if (accordionNumber === 1) {
      this.accSub1 = !this.accSub1;
    }

    if (accordionNumber === 2) {
      this.accSub2 = !this.accSub2;
    }
  }
}
