import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  year: number;
  links: Array<{ url: string, title: string }>;

  cosntructor() {}

  ngOnInit() {
    this.year = new Date().getFullYear();
    this.links = [
      {
        url: 'http://www.bnpparibascardif.com.ar/',
        title: 'BNP Paribas Cardif Argentina'
       }      // {
      //   url: 'http://qr.afip.gob.ar/?qr=_bDIwALCUrzOC0AWXkKj9Q,,',
      //   title: 'Data Fiscal Seguros'
      // },
      // {
      //   url: 'http://www.bnpparibascardif.com.ar/',
      //   title: 'Condiciones generales'
      // },
      // {
      //   url: 'http://qr.afip.gob.ar/?qr=diQSc-EwKZ_BQCNY1vOU7Q,,',
      //   title: 'Data Fiscal Servicios'
      // }
    ];
  }
}
