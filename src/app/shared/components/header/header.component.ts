import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { UserModel } from '../../models/user.model';
import { OAuthService } from '../../services/oauth.service';
import { NotificationService } from '../../services/notification.service';
import { RespuestaVenta } from '../../../dashboard/models/ventas-realizadas/respuesta-venta.model';
import { DashboardService } from '../../../dashboard/services/dashboard.service';
import { NewSaleService } from "../../../dashboard/services/new-sale.service";

@Component ( {
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: [ './header.component.scss' ]
} )
export class HeaderComponent implements OnInit, OnDestroy {
  showMenu = false;
  isOverlayOn = false;
  isChangePassSuccess = false;
  isChangePassError = false;
  isSaleSuccess = false;
  isSaleCancel = false;
  idSale: number;
  notifSubscription: Subscription;
  notifSubscription2: Subscription;
  notifSubscription3: Subscription;
  errorServidorSubscription: Subscription;
  errorServidor: boolean = false;
  user: UserModel;
  isSaleError = false;
  isSaleExcluido = false;
  private notificationDelay = 10000;
  respuestaVenta: RespuestaVenta;
  producto: string;
  idVenta: string;
  idContrato: string;
  estado: string;
  detalle: string;
  plan: string;
  efecto: boolean = true;
  in: string = "bounceInDown";
  out: string = "zoomOut";
  tieneSolicitud = false;
  
  constructor ( private oAuthService: OAuthService, private notificationService: NotificationService,
                public dashboardService: DashboardService, private newSaleService: NewSaleService ) { }
  
  ngOnInit () {
    
    this.user = this.newSaleService.user;
    this.respuestaVenta = this.dashboardService.respuestaVenta;
    
    window.onclick = ( event: any ) => {
      if ( !event.target.matches ( '.toggle-dropdown' ) ) {
        this.showMenu = false;
      }
    };
    
    this.notifSubscription = this.notificationService.getPassSucces ()
      .subscribe ( ( isSuccess ) => {
        
        this.isChangePassSuccess = isSuccess;
      } );
    
    this.notifSubscription2 = this.notificationService.getPassError ()
      .subscribe ( ( isError ) => {
        
        this.isChangePassError = isError;
      } );
    
    this.notifSubscription3 = this.notificationService.getSaleSucces ()
      .subscribe ( ( { status, idSale, respuesta }: any ) => {
        this.respuestaVenta = respuesta;
        this.producto = respuesta.producto;
        this.idVenta = respuesta.idVenta;
        this.idContrato = respuesta.idContrato;
        this.detalle = respuesta.detalle;
        // this.plan = respuesta.plan;
        this.idSale = respuesta.estado;
        if ( this.idSale == -1 ) {
          this.isSaleError = true;
        } else {
          this.idContrato = respuesta.idContrato;
          this.isSaleSuccess = status;
        }
      } );
    
    this.errorServidorSubscription = this.notificationService.getErrorServidor ().subscribe ( ( mensajeDeError: any ) => {
      this.errorServidor = mensajeDeError;
    } );
  }
  
  putOverlay () {
    this.isOverlayOn = true;
    this.isChangePassSuccess = false;
    this.isChangePassError = false;
  }
  
  removeOverlay () {
    this.isOverlayOn = false;
  }
  
  ngOnDestroy () {
    if ( this.notifSubscription ) {
      this.notifSubscription.unsubscribe ();
    }
    
    if ( this.notifSubscription2 ) {
      this.notifSubscription2.unsubscribe ();
    }
    
    if ( this.notifSubscription3 ) {
      this.notifSubscription3.unsubscribe ();
    }
  }
  
  logout () {
    this.oAuthService.logOut ();
  }
  
  cerrarMensajeError () {
    this.isSaleError = false;
    this.isSaleExcluido = false;
    
  }
  
  cerrarModalCancelar () {
    this.dashboardService.isSaleCancel = !this.dashboardService.isSaleCancel;
    this.efecto = this.dashboardService.isSaleCancel;
  }
  
  cerrarModalSucces () {
    this.isSaleSuccess = !this.isSaleSuccess;
    this.efecto = this.isSaleSuccess;
  }
  
  cerrarModalErrorServidor () {
    this.errorServidor = !this.errorServidor;
    this.efecto = this.errorServidor;
  }
  
  cerrarModalIsSaleError () {
    this.isSaleError = !this.isSaleError;
    this.efecto = this.isSaleSuccess;
  }

}
