import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-next-button',
  templateUrl: './next-button.component.html',
  styleUrls: ['./next-button.component.scss']
})
export class NextButtonComponent {
  @Input() title: string = 'Siguiente';
  @Input() isDisabled: boolean = false;

  @Output() nextAction: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}
}
