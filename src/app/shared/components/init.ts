import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SocialNetworksComponent } from './social-networks/social-networks.component';
import { YesNoQuestionComponent } from './yes-no-question/yes-no-question.component';
import { NextButtonComponent } from './buttons/next-button/next-button.component';

export const COMPONENTS = [
  HeaderComponent,
  FooterComponent,
  SpinnerComponent,
  SidebarComponent,
  SocialNetworksComponent,
  YesNoQuestionComponent,
  NextButtonComponent
];
