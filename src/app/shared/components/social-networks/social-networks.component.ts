import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-social-networks',
  templateUrl: './social-networks.component.html',
  styleUrls: ['./social-networks.component.scss']
})
export class SocialNetworksComponent implements OnInit {
  items: Array<{url: string, name: string}>;

  construcotr() {}

  ngOnInit() {
    this.items = [
      {
        url: 'https://twitter.com/BNPPCardifARG',
        name: 'twitter'
      },
      {
        url: 'https://www.linkedin.com/company/bnp-paribas-cardif/',
        name: 'linkedin'
      },
      {
        url: 'https://www.instagram.com/bnppcardifarg/',
        name: 'pinterest'
      },
      {
        url: 'https://www.youtube.com/channel/UCJZeXaDnCtfWMDFHaD_YPiA',
        name: 'youtube'
      },
      {
        url: 'https://www.instagram.com/bnppcardifarg/',
        name: 'instagram'
      }
    ];
  }
}
