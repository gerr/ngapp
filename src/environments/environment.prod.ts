export const environment = {
  production: true,
  testing: false,
  BASE_URL: 'https://beta-portalsocios.cardif.com.ar/cardif/api/v1',
  API: 'https://beta-portalsocios.cardif.com.ar/cardif/api/v1',
  OAUTH: 'https://beta-portalsocios.cardif.com.ar/oauth/token',
  LOGOUT: 'https://beta-portalsocios.cardif.com.ar/c/portal/logout',
  COOKIE_NAME: 'cardif_jwt',
  COOKIE_LOGIN_AUTO: 'client_data',
  URL_PSP: 'https://live.decidir.com/api/v2',
  API_KEY_SPS: '7f53ef0d158f4d6e8ca88539f49da670'
  //key de produccion
};
