// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  testing:false,

  BASE_URL: 'http://localhost:8080/cardif/api/v1',
  API: 'http://localhost:8080/cardif/api/v1',
  OAUTH: 'http://localhost:8080/oauth/token',
  LOGOUT: 'https://portalsocios-test.cardif.com.ar/web/guest/home',
  COOKIE_NAME: 'cardif_jwt',
  COOKIE_LOGIN_AUTO: 'client_data',
  URL_PSP: 'https://developers.decidir.com/api/v2',
  API_KEY_SPS: 'e9cdb99fff374b5f91da4480c8dca741'
};
