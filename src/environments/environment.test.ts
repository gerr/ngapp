export const environment = {
  production: false,
  testing:true,
  BASE_URL: 'https://portalsocios-test.cardif.com.ar/cardif/api/v1',
  API: 'https://portalsocios-test.cardif.com.ar/cardif/api/v1',
  OAUTH: 'https://portalsocios-test.cardif.com.ar/oauth/token',
  LOGOUT: 'https://portalsocios-test.cardif.com.ar/c/portal/logout',
  COOKIE_NAME: 'cardif_jwt',
  COOKIE_LOGIN_AUTO: 'client_data',
 URL_PSP: 'https://developers.decidir.com/api/v2',
  API_KEY_SPS: 'e9cdb99fff374b5f91da4480c8dca741'
};
