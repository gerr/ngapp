export const environment = {
  production: false,
  tessting:false,
  BASE_URL: 'http://bnp-cardif-uat.globant.com:8094/cardif/api/v1',
  API: 'http://bnp-cardif-uat.globant.com:8094/cardif/api/v1',
  OAUTH: 'http://bnp-cardif-uat.globant.com:8094/oauth/token',
  LOGOUT: 'https://portalsocios-test.cardif.com.ar/c/portal/logout',
  COOKIE_NAME: 'cardif_jwt',
 URL_PSP: 'https://developers.decidir.com/api/v2',
  API_KEY_SPS: 'e9cdb99fff374b5f91da4480c8dca741'
};
